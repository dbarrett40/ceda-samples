#!/bin/bash

PATH_TO_FOLDER_CONTAINING_THIS_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SOURCE=$PATH_TO_FOLDER_CONTAINING_THIS_SCRIPT

BUILD=$SOURCE/../build/ceda-samples

function build_linux_config {
    # x86 or x64
    SYSTEM_PROCESSOR=$1

    # ON or OFF
    SHARED_LIBS=$2

    # Debug or RelWithDebInfo
    CONFIG=$3  

    FOLDER=linux-$SYSTEM_PROCESSOR
    if [ "$SHARED_LIBS" != 'ON' ]; then
        FOLDER=$FOLDER-static
    fi
    if [ "$CONFIG" == 'Debug' ]; then
        FOLDER=$FOLDER-debug
    fi

    echo "SOURCE=$SOURCE"
    echo "BUILD=$BUILD"
    echo "FOLDER=$FOLDER"

    rm -rf $BUILD/$FOLDER

    mkdir -p $BUILD/$FOLDER
    cd $BUILD/$FOLDER

    echo "-----------------------------------------------------------------------------"
    echo "Running cmake configure ..."

    cmake -G "Ninja" \
            -DBUILD_SHARED_LIBS=$SHARED_LIBS \
            -DCMAKE_INSTALL_PREFIX:PATH="install" \
            -DCMAKE_CXX_STANDARD=17 \
            -DCMAKE_CXX_COMPILER=g++ \
            -DCMAKE_C_COMPILER=gcc \
            -DCMAKE_BUILD_TYPE=$CONFIG \
            -DCMAKE_MAKE_PROGRAM=ninja \
            $SOURCE || exit

    echo "-----------------------------------------------------------------------------"
    echo "Running cmake build and install ..."
    cmake --build . --target install || exit

    echo "-----------------------------------------------------------------------------"
    echo "Running ctest..."
    ctest
}

function build_android_config {
    ABI=$1
    SHARED_LIBS=$2
    CONFIG=$3  
    
    SYSTEM_PROCESSOR=$ABI
    if [ "$ABI" == 'arm64-v8a' ]; then
        SYSTEM_PROCESSOR=aarch64
    fi

    PLATFORM=android-$SYSTEM_PROCESSOR

    # The Android NDK must be installed.  This provides the cross compiler for targeting Android with ABIs such as arm64-v8a
    NDK=~/Android/Sdk/ndk-bundle

    # Boost must be built using the NDK.  See the script buildboostandroid_linux
    BOOST=~/cedanet/boost-android/install/$ABI

    FOLDER=$PLATFORM
    if [ "$SHARED_LIBS" != 'ON' ]; then
        FOLDER=$FOLDER-static
    fi
    if [ "$CONFIG" == 'Debug' ]; then
        FOLDER=$FOLDER-debug
    fi

    echo "SOURCE=$SOURCE"
    echo "BUILD=$BUILD"
    echo "FOLDER=$FOLDER"

    rm -rf $BUILD/$FOLDER

    mkdir -p $BUILD/$FOLDER
    cd $BUILD/$FOLDER

    echo "-----------------------------------------------------------------------------"
    echo "Running cmake configure ..."

    cmake -G "Ninja" \
            -DCEDA_CUSTOM_BUILD_ANDROID=ON \
            -DBUILD_SHARED_LIBS=$SHARED_LIBS \
            -DCMAKE_INSTALL_PREFIX:PATH="install" \
            -DANDROID_ABI=$ABI \
            -DANDROID_NDK=$NDK \
            -DCMAKE_BUILD_TYPE=$CONFIG \
            -DCMAKE_MAKE_PROGRAM=ninja \
            -DCMAKE_TOOLCHAIN_FILE="$NDK/build/cmake/android.toolchain.cmake" \
            -DANDROID_NATIVE_API_LEVEL="21" \
            -DANDROID_TOOLCHAIN="clang" \
            -DCMAKE_CXX_FLAGS=-fPIC -frtti -fexceptions -std=c++17 \
            $SOURCE || exit

    echo "-----------------------------------------------------------------------------"
    echo "Running cmake build and install ..."
    cmake --build . --target install || exit
}

################ linux

build_linux_config x64 ON RelWithDebInfo
build_linux_config x64 ON Debug

# static linking doesn't work at the moment because there's a static link dependendency on boost
#build_linux_config x64 OFF RelWithDebInfo
#build_linux_config x64 OFF Debug

################ android

build_android_config arm64-v8a ON RelWithDebInfo
build_android_config arm64-v8a ON Debug

build_android_config armeabi-v7a ON RelWithDebInfo
build_android_config armeabi-v7a ON Debug

build_android_config armeabi-x86 ON RelWithDebInfo
build_android_config armeabi-x86 ON Debug

build_android_config armeabi-x86_64 ON RelWithDebInfo
build_android_config armeabi-x86_64 ON Debug

# static linking doesn't work at the moment because there's a static link dependendency on boost
#build_android_config arm64-v8a OFF RelWithDebInfo
#build_android_config arm64-v8a OFF Debug

# These don't build yet because these are currently unsupported in ceda
#build_android_config armeabi-v7a ON Debug
#build_android_config x86 ON Debug
#build_android_config x86_64 ON Debug

cd $SOURCE
