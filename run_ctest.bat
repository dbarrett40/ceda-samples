@ECHO OFF

SET VCVARS64=%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat
CALL "%VCVARS64%"

SET PATH_TO_THIS_BATCH_FILE=%~dp0
SET SOURCE=%PATH_TO_THIS_BATCH_FILE%

::SET BUILD=%SOURCE%\..\_build\ceda-samples
SET BUILD=%SOURCE%\..\..\build-ceda-samples

SET WINDOWS_COMPILER="cl.exe"
::SET WINDOWS_COMPILER="C:/Program Files/LLVM/bin/clang-cl.exe"
SET CONFIG=Debug

CD %BUILD%\Win64-Debug-dll
CD

cmake -G "Ninja" ^
-DJAVA_HOME="C:/Program Files/Java/jdk-12.0.1" ^
-DCMAKE_INSTALL_PREFIX:PATH="%BUILD%\install\x64-Release" ^
-DCMAKE_TOOLCHAIN_FILE="%VCPKG_ROOT_DIR%/scripts/buildsystems/vcpkg.cmake" ^
-DCMAKE_WINDOWS_EXPORT_ALL_SYMBOLS="TRUE" ^
-DCMAKE_CXX_COMPILER=%WINDOWS_COMPILER% ^
-DCMAKE_C_COMPILER=%WINDOWS_COMPILER% ^
-DCMAKE_BUILD_TYPE=%CONFIG% ^
-DCMAKE_MAKE_PROGRAM=ninja ^
--config %CONFIG% ^
"%SOURCE%"

ninja

ctest

::CD Test
::ctest

::CD Java/JarExample
::ctest


::cmake --build . --target RUN_TESTS
::cmake --build . --target test1

PAUSE
