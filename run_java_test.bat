set JAVA=C:/Program Files/Java/jdk-11.0.8/bin/java.exe

set CEDABIN=C:/Program Files/Ceda/windows-x64-debug/bin
set CEDAJAR=%CEDABIN%/CedaJni.jar

set BUILD=C:/cedanet/build-ceda-samples/windows-x64-debug
set SAMPLESBIN=%BUILD%/bin

set TESTJAR=%BUILD%/MyCompany/java/tests/Example/MyTestJar.jar
set PIZZAJAR=%BUILD%/MyCompany/PizzaJni.jar

:: The JVM uses the java.library.path property in order to locate native libraries.
:: There a multiple .dll files in CEDABIN
:: It would seem appropriate to use -Djava.library.path="%CEDABIN%;%SAMPLESBIN%"
:: However it has been found that setting java.library.path doesn't work
:: because, for example when loading cxJava.dll we get the error: Can't find dependent libraries.
:: Instead it is necessary to set the PATH environment variable so it includes CEDABIN
set PATH=%CEDABIN%;%SAMPLESBIN%

"%JAVA%" -ea -cp "%TESTJAR%;%PIZZAJAR%;%CEDAJAR%" MyTest
del pizzaclient
del pizzaserver
