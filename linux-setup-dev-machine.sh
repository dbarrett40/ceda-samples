#!/bin/bash

sudo apt-get update
sudo apt-get upgrade

sudo apt-get -y install build-essential    
sudo apt-get -y install tree
sudo apt-get -y install git
sudo apt-get -y install ninja-build
sudo apt-get -y install default-jdk

sudo apt-get -y install python
sudo apt-get -y install libpython2.7-dev
sudo apt-get -y install python-pip
python -m pip install --upgrade pip
python -m pip install --user --upgrade setuptools wheel
python -m pip install --user --upgrade twine

# Need later version of cmake than what is obtained when just install cmake using apt-get (version 3.10.2)
if [ -d ~/cmake ]; then
    echo folder ~/cmake already exists
else
    mkdir -p ~/cmake
    cd ~/cmake
    wget https://github.com/Kitware/CMake/releases/download/v3.15.2/cmake-3.15.2-Linux-x86_64.sh
    sh cmake-3.15.2-Linux-x86_64.sh
fi
