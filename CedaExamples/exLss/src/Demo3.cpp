// Demo3.cpp

#include "Ceda/cxLss/ILogStructuredStore.h"
#include "Ceda/cxLss/LssSettings.h"
#include "Ceda/cxLss/LssExceptions.h"
#include "Ceda/cxUtils/AutoCloser.h"
#include "Ceda/cxUtils/FileException.h"
#include "Ceda/cxUtils/Environ.h"
#include "Ceda/cxUtils/FileTools.h"
#include "Ceda/cxUtils/xostream.h"
#include "Ceda/cxUtils/Tracer.h"
#include <assert.h>
#include <iostream>


void OpenThenCloseLssExample()
{
    const char* path = "myfile";

    bool createdNew;

    // These settings affect some of the properties of the LSS such as the segment size and the maxmimum
    // number of segments in the cache.
    // See http://www.cedanet.com.au/ceda/libs/cxLss/LssSettings.php
    ceda::LssSettings settings;
    settings.segmentSize = 4*1024*1024;
    settings.maxNumSegmentsInCache = 64;
    settings.flushTimeMilliSec = 2000;

    // Create or open a Log Structured Store, using the given path and LssSettings.
    // See http://www.cedanet.com.au/ceda/libs/cxLss/CreateOrOpenLSS.php
    // OM_OPEN_ALWAYS means the file will be opened if it already exists, otherwise it is created.
    // See http://www.cedanet.com.au/ceda/libs/cxUtils/EOpenMode.php
    ceda::ILogStructuredStore* lss = ceda::CreateOrOpenLSS(path, nullptr, createdNew, ceda::OM_OPEN_ALWAYS, settings);

    if (createdNew)
    {
        std::cout << "A new store was created";
    }
    else
    {
        std::cout << "An existing store was opened";
    }

    lss->Close();
}

void WriteMySerialElement(ceda::ICloseableOutputStream* os)
{
}

void WriteRootSerialElementExample()
{
    const char* path = "myfile";

    bool createdNew;
    ceda::LssSettings settings;
    ceda::ILogStructuredStore* lss = ceda::CreateOrOpenLSS(path, nullptr, createdNew, ceda::OM_OPEN_ALWAYS, settings);
    if (createdNew)
    {
        // Create a 32 bit Seid space for all Seid allocations
        ceda::SeidHigh seidHigh = lss->CreateSeidSpace();
                
        // Allocate Seid for root object
        ceda::Seid seid = lss->AllocateSeid(seidHigh);
        assert(seid == ceda::ROOT_SEID);

        const char* buffer = "Hello world";

        ceda::ILssTransaction* txn = lss->OpenTransaction();

        // Write root object
        ceda::ICloseableOutputStream* os = txn->WriteSerialElement(seid);
        os->WriteStream(buffer, strlen(buffer)+1);
        os->Close();

        txn->Close();
    }
    else
    {
        ceda::Seid seid = ceda::ROOT_SEID;

        const int BUFSIZE = 100;
        char buffer[BUFSIZE];

        // Read root object
        ceda::ICloseableInputStream* is = lss->ReadSerialElement(seid);
		ceda::ssize_t numBytesRead = is->ReadStream(buffer, BUFSIZE);
        is->Close();

        std::cout << "Read: " << buffer << '\n';
    }

    lss->Close();
}



bool Demo3()
{
    try
    {
        ceda::xstring path = ceda::GetCedaTestPath("exlss_demo3.lss");

        if (ceda::DeleteFile_u8(path.c_str()))
        {
             Tracer() << "Deleting file " << path << ceda::endl;
        }

        for (int i = 0 ; i < 3 ; ++i)
        {
            bool createdNew;

            ceda::close_ptr<ceda::ILogStructuredStore> lss;
            try
            {
                 Tracer() << "\nPath = " << path << ceda::endl;

                // Open or create store, using default settings
                ceda::LssSettings settings;
                lss.reset(ceda::CreateOrOpenLSS(path.c_str(), NULL, createdNew, ceda::OM_OPEN_ALWAYS, settings));
                assert(lss);
            }
            catch(ceda::FileException& e)
            {
                 Tracer() << "Error creating or opening store : " << e << ceda::endl;
                return false;
            }

            const int BUFSIZE = 20;

            ceda::Seid seid;
            if (createdNew)
            {
                 Tracer() << "Creating store for first time" << ceda::endl;

                // Create a 32 bit Seid space for all Seid allocations
                ceda::SeidHigh seidHigh = lss->CreateSeidSpace();
                
                // Allocate Seid for root object
                seid = lss->AllocateSeid(seidHigh);

                assert(seid == ceda::ROOT_SEID);

                ceda::close_ptr<ceda::ILssTransaction> txn(lss->OpenTransaction());

                // Write root object
                ceda::octet_t buffer[BUFSIZE];
                for (int i=0 ; i < BUFSIZE ; ++i) buffer[i] = (ceda::octet_t) i;

                {
                    ceda::close_ptr<ceda::ICloseableOutputStream> os(txn->WriteSerialElement(seid));
                    os->WriteStream(buffer, BUFSIZE);
                }
            }
            else
            {
                Tracer() << "Store already exists" << ceda::endl;
                seid = ceda::ROOT_SEID;

                // Read root object
                ceda::close_ptr<ceda::ICloseableInputStream> is(lss->ReadSerialElement(seid));
                assert(is);
                ceda::octet_t buffer[BUFSIZE];
				ceda::ssize_t numBytesRead = is->ReadStream(buffer, BUFSIZE);
                assert(numBytesRead == BUFSIZE);
                for (int i=0 ; i < BUFSIZE ; ++i) 
                {
                    Tracer() << "buffer[" << i << "] = " << (int) buffer[i] << ceda::endl;
                    assert(buffer[i] == i);
                }
            }
        }

         Tracer() << "\nTest completed" << ceda::endl;
    }
    catch(ceda::ProgrammingErrorException& e)
    {
         Tracer() << "Programming error exception : " << e << ceda::endl;
        return false;
    }
    catch(ceda::FileException& e)
    {
         Tracer() << "File Exception : " << e << ceda::endl;
        return false;
    }
    catch(ceda::CorruptLSSException& e)
    {
         Tracer() << "Corruption : " << e << ceda::endl;
        return false;
    }

    return true;
}

