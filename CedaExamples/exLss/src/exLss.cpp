// exLss.cpp

#include "Ceda/cxUtils/xstring.h"
#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"

bool Demo1();
bool Demo2();
bool Demo3();

void exLss_RunAllTests()
{
    Tracer() << "----------------------------------- exLss examples  -----------------------------------\n";
    ceda::HPTimer timer;
    Demo1();
    Demo2();
    Demo3();
    timer.Done() << "exLss examples completed successfully";
}

CEDA_MAIN_FUNCTION(argc,argv)
{
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exLss_RunAllTests();
    ceda::DestroyTheTraceFile();
    return 0;
}

