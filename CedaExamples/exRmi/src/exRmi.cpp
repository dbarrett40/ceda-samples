// exRmi.cpp

#include "Ceda/cxUtils/xstring.h"
#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"
@import "Ceda/cxObject/TargetRegistry.h"

mRegisterLeafTarget(
    {
        // version         year mon day
        { "Release 0.0",   {2008,8,24}   },      // Release #0
    })

void RmiExample();
void RmiBounce();
void RmiTwoClients();
void RmiTwoClients2();

///////////////////////////////////////////////////////////////////////////////////////////////////
// RunAllExamples()

void exRmi_RunAllExamples()
{
    Tracer() << "----------------------------------- exRmi examples -----------------------------------\n";
    ceda::HPTimer timer;
    
    RmiExample();
    RmiBounce();
    // todo: causes a segmentation fault
    //RmiTwoClients();

    timer.Done() << "exRmi examples completed successfully";
}

CEDA_MAIN_FUNCTION(argc,argv)
{
    CEDA_REGISTER_PROJECT
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exRmi_RunAllExamples();
#ifdef CEDA_CHECK_ASSERTIONS
    ReportLeaks(Tracer());
#endif
    ceda::DestroyTheTraceFile();
    return 0;
}

