// RmiExample.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2011

@import "Ceda/cxRmi/RmiConnection.h"
#include "Ceda/cxUtils/Tracer.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// Ix

$interface rmi Ix
{
    // Asynchronous method
    void PrintInt(
        [in] ceda::int32 x);
        
    // c = a+b
    void Sum(
        [in] ceda::int32 a,
        [in] ceda::int32 b,
        [out] ceda::float32& c);
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// X

/*
warning W1003 : $interface Ix doesn't inherit from IObject
warning W1013 : $interface Ix not reflected
*/
$warning(push; disable: 1003 1013)

$struct X isa Ix :
    mixin 
    [
        ceda::RmiSessionMixin2<Ix,Ix>
    ]
{
    // Implementation of Ix
    void PrintInt(ceda::int32 x)
    {
        Tracer() << "    PrintInt() called with x= " << x << '\n';
    }
    void Sum(ceda::int32 a,ceda::int32 b,ceda::float32& c)
    {
        Tracer() << "    Sum() called with a= " << a << "  b= " << b << '\n';
        c = (ceda::float32) (a+b);
    }
};

$warning(pop)

///////////////////////////////////////////////////////////////////////////////////////////////////

/*
Output is as follows:

    Rmi Example
      Waited for connections
      PrintInt() called for 0,1,2,3,4
      Flush
      Sum(10,20), Sum(1,2), Sum(4,7) issued
        PrintInt() called with x= 0
        PrintInt() called with x= 1
        PrintInt() called with x= 2
        PrintInt() called with x= 3
        PrintInt() called with x= 4
        Sum() called with a= 10  b= 20
        Sum() called with a= 1  b= 2
        Sum() called with a= 4  b= 7
      c1 = 30
      c2 = 3
      c3 = 11
*/

void RmiExample()
{
    Tracer() << "Rmi Example\n";
    ceda::TraceIndenter indent;

    ceda::Iocp* iocp = ceda::CreateIocp();
    for (int count = 0 ; count < 3 ; ++count)
    {
        ceda::SOCKET_PORT port = 3001;
        
        ceda::BlockingSingleSessionCreator<X>* ccClient = new ceda::BlockingSingleSessionCreator<X>();

        ceda::MServer* server = ceda::CreateMServer(iocp, new ceda::SessionCreator<X>(), port);
        ceda::MClient* client = ceda::CreateMClient(iocp, ccClient, "127.0.0.1", port);
        
        X* sClient = ccClient->GetSession();
        Tracer() << "Waited for connections\n";
        
        ceda::ptr<Ix> stub = sClient->GetStub();

        for (int i=0 ; i < 5 ; ++i)
        {
            stub->PrintInt(i);
        }
        Tracer() << "PrintInt() issued for 0,1,2,3,4\n";
        
        Tracer() << "Flush\n";
        sClient->Flush();
        
        ceda::float32 c1 = 0;
        ceda::float32 c2 = 0;
        ceda::float32 c3 = 0;

        stub->Sum(10,20,c1);
        stub->Sum(1,2,c2);
        stub->Sum(4,7,c3);
        Tracer() << "Sum(10,20), Sum(1,2), Sum(4,7) issued\n";

        sClient->MarkAndWait();
        
        Tracer() << "c1 = " << c1 << '\n';
        Tracer() << "c2 = " << c2 << '\n';
        Tracer() << "c3 = " << c3 << '\n';
        
        Close(client);
        Close(server);
    }
    Close(iocp);
    
    Tracer() << "Test completed\n";
}

