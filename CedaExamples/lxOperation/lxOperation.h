@import "Ceda/cxPersistStore/Models.h"

CEDA_DEFINE_PROJECT_API_MACRO

$enum+ class ENotificationLevel
{
    low,
    medium,
    high    
};

$model+ Notification
{
    assignable< ceda::xstring > Message;
    ENotificationLevel Level;
};

