// PizzaDeliveries.h
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2010

@import "lxOperation.h"
@import "DateTime.h"
@import "Shapes.h"

/*
http://www.databaseanswers.org/data_models/ is an excellent resource for examples of
database schemata.  One such example is for pizza deliveries.

It is interesting to compare to an OO version.  It is observed that in the pizza delivery
database schema most relations use a surrogate id for the primary key. For
example, each pizza order is allocated a unique identity.

In CEDA it would appear appropriate to normally associate this with the creation of an 
object with unique identity, and use a cref to reference it.  However it is noteworthy 
that we currently don't allow for an object to be cref'd more than once.  i.e. 
objects must form a tree.   Nor is this a suggestion for changing that approach.  

In other cases, identity is created when there doesn't appear sufficient reason to do
so.  For example, there seems little reason to make an address an object rather than
simply embed it by value where it is needed (such as in the tuple for a customer).

Somehow in CEDA we should allow for both OO and RM perspectives.  E.g. given any model
definition we should allow it to represent a relvar very conveniently.
*/

namespace pizza
{
    $typedef+ int32 TToppingId;
    $typedef+ int32 TEmployeeId;
    $typedef+ int32 TVehicleTypeId;
    $typedef+ int32 TVehicleId;
    $typedef+ int64 TCustomerId;
    $typedef+ int64 TOrderId;
    $typedef+ float64 TCurrency;
    $typedef+ assignable<string8> TEmail;
    $typedef+ assignable<string8> TPhoneNumber;

    $model+ TAddress <<multiline>>
    {
        int32 Number;
        assignable<string8> Street;
        assignable<string8> City;
        int32 ZipPostCode;
        assignable<string8> StateProvinceCounty;
        assignable<string8> Country;
    };

    $model+ TEmployee <<multiline>>
    {
        TAddress Address;
        assignable<string8> FirstName;
        assignable<string8> LastName;
        TPhoneNumber PhoneNumber;
    };

    $model+ TVehicleType
    {
        assignable<string8> Description;
    };

    $model+ TVehicle
    {
        TVehicleTypeId VehicleTypeId;
        assignable<string8> LicensePlateNumber;
    };

    $enum+ class EPaymentMethod
    {
        Cash,
        EftPos,
        CreditCard
    };

    $model+ TCustomer <<multiline>>
    {
        TAddress Address;
        assignable<string8> FirstName;
        assignable<string8> LastName;
        TPhoneNumber PhoneNumber;
        TEmail Email;
        dt::TDateTime DateOfFirstOrder;
        EPaymentMethod PaymentMethod;
    };

    $enum+ class EDeliveryStatus
    {
        Cooking,
        Delivering,
        Completed,
        Returned
    };

    $enum+ class EBaseType
    {
        Thin,
        DeepPan
    };

    $model+ TCircle
    {
        float32 Radius;    
    };

    $model+ TRectangle
    {
        float32 Width;
        float32 Height;
    };

    $variant+ TShape
    {
        default TCircle(200);
        TCircle Circle;
        TRectangle Rectangle;
    };
    
    $model+ TTopping <<multiline>>
    {
        TCurrency Price;
        assignable<string8> Description;
    };

    $model+ TOrderedPizza <<multiline>>
    {
        TShape Shape;
        EBaseType BaseType;
        xvector<TToppingId> ToppingIds;
    };

    $struct+ TPizzaOrder <<multiline>> isa ceda::IPersistable :
        model
        {
            TOrderId Id;
            TCustomerId CustomerId;
            TEmployeeId TakenByEmployeeId;
            TEmployeeId DeliveredByEmployeeId;
            EDeliveryStatus DeliveryStatus;
            TVehicleId VehicleId;
            dt::TDateTime DateTimeOrderTaken;
            dt::TDateTime DateTimeOrderDelivered;
            TCurrency TotalOrderPrice;
            xvector<TOrderedPizza> Pizzas;
        }
    {
    };

    $struct+ TPizzaDeliveryDatabase <<multiline>> isa ceda::IPersistable :
        model
        {
            xmap<TVehicleId, TVehicle> Vehicles;
            xmap<TVehicleTypeId, TVehicleType> VehicleTypes;
            xmap<TEmployeeId, TEmployee> Employees;
            xmap<TToppingId, TTopping> Toppings;
            xmap<TCustomerId, TCustomer> Customers;
            //xmap<TOrderId, TPizzaOrder> Orders;
            xvector< movable< cref<TPizzaOrder> > > Orders;
        }
    {
    };

} // namespace pizza
