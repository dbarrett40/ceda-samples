// Shapes.h
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2012

@import "lxOperation.h"
@import "DateTime.h"

namespace geom
{
    $enum+ class EColour
    {
        red,
        green,
        blue,
        magenta,
        cyan,
        yellow
    };

    $model+ TPoint
    {
        float32 X;
        float32 Y;
    };

    $model+ TLine
    {
        TPoint P1;
        TPoint P2;
    };

    $model+ TSquare
    {
        TPoint P1;   // bottom left corner
        float32 W;  // width and height
    };

    $model+ TRect
    {
        TPoint P1;   // bottom left corner
        TPoint P2;   // top right corner
    };

    $model+ TCircle
    {
        TPoint C;
        float32 R; 
    };
    
    $model+ TPolygon
    {
        xvector<TPoint> V;
    };

    $variant+ TShape
    {
        default TCircle(TPoint(0,0),1);
        TPoint;
        TLine;
        TSquare;
        TRect; 
        TCircle; 
        TPolygon;
    };
    
    $model+ TPicture <<multiline>>
    {
        string8 Name;
        string8 Author;
        dt::TDateTime CreationDate;
        EColour Colour;
        TRect BoundingBox;
        xvector<TShape> Shapes;
        xvector<TRect> Rectangles;
        xvector<TCircle> Circles;
        xvector<TLine> Lines;
    };
}