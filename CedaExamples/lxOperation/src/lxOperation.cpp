// lxOperation.cpp

@import "lxOperation.h"
@import "DateTime.h"
@import "Jigsaw.h"
@import "PizzaDeliveries.h"
@import "Shapes.h"
@import "DrillHoles.h"
@import "Ceda/cxObject/TargetRegistry.h"

mRegisterLeafTarget(
    // Releases
    {
        // version         year mon day
        { "Release 0.0",   {2008,8,24}  },      // Release #0
    })

