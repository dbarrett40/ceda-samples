// exPersistStore.cpp
//

#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"
@import "Ceda/cxObject/TargetRegistry.h"

mRegisterLeafTarget(
    {
        // version         year mon day
        { "Release 0.0",   {2008,8,24}   },      // Release #0
    })

/*
Proposal for examples       (DBL)
---------------------

*   OpenPersistStore should actually provide an example of bootstrapping a root
    rather than merely assume it is always creating a new store.

*   Demonstrate schema evolution of a data model
    Show that members can be added, dropped and evolved
    
*   Demonstrate cref<>

*   Demonstrate pref<>

*   Demonstrate datasource with nested models

*   Demonstrate MarkPersistentAsDirty() and MarkPersistentAsDirtyWithoutTrace()

*   Demonstrate VisitPrefs(IPrefVisitor& v), RepresentsSubTree()

*   Demonstrate 
        TypeMismatchException, 
        DerefDanglingOidException, 
        DeserialisePersistentObjectException
        OpenPSpaceException
        
*   Demonstrate PersistStoreTxn::DestroyPSpace()

*   Demonstrate PSpace   GetRoot(), AddRoot(), RemoveRoot()

*   Demonstrate AsyncPermanentlyDeleteSubTree()

*   Demonstrate hooks for OID serialisation.

It would be appropriate to develop these examples in tandem with proper docmentation of 
cxPersistStore.dll in the Ceda technical overview documentation.
*/

namespace DataSources { void Run(); }
namespace MultiPSpaceTxn { void Run(); }
namespace OpenMode { void Run(); }
namespace OpenPersistStore { void Run(); }
namespace ModifyPO { void Run(); }
namespace DeleteObjects { void Run(); }
namespace DeleteObjects2 { void Run(); }
namespace PDequeEx { void Run(); }
namespace BPlusTreeEx { void Run(); }
namespace TransferObjects { void Run(); }
namespace VariantEx { void Run(); }
namespace AsyncIO { void Run(); }
namespace xsetEx { void Run(); }
namespace TypeOpsEx { void Run(); }             
namespace SingleObjectWithSerialiseFunction { void Run(); }
namespace EagerSchemaEvolution { void Run(); }

///////////////////////////////////////////////////////////////////////////////////////////////////
// exPersistStore_RunAllExamples()

void exPersistStore_RunAllExamples()
{
    Tracer() << "----------------------------------- exPersistStore examples  -----------------------------------\n";
    ceda::HPTimer timer;

    SingleObjectWithSerialiseFunction::Run();

    // todo: trips because it tries to lock a CSpace that is already locked
    //xsetEx::Run();
    
    DataSources::Run();
    MultiPSpaceTxn::Run();
    OpenMode::Run();
    OpenPersistStore::Run();
    ModifyPO::Run();
    DeleteObjects::Run();
    
    // todo: trips because GetNumPersistableObjects() is called by a thread that has locked the CSpace
    // and GetNumPersistableObjects() tries to flush the dirty object set, and therefore tries to lock
    // the CSpace that is already locked.
    //DeleteObjects2::Run();
    
    PDequeEx::Run();
    BPlusTreeEx::Run();
    TransferObjects::Run();
    VariantEx::Run();
    AsyncIO::Run();
    TypeOpsEx::Run();
    EagerSchemaEvolution::Run();

    timer.Done() << "exPersistStore examples completed successfully";
}

///////////////////////////////////////////////////////////////////////////////////////////////////

CEDA_MAIN_FUNCTION(argc,argv)
{
    CEDA_REGISTER_PROJECT
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exPersistStore_RunAllExamples();
#ifdef CEDA_CHECK_ASSERTIONS
    ReportLeaks(Tracer());
#endif
    ceda::DestroyTheTraceFile();
    return 0;
}


