// PDequeEx.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "Ceda/cxPersistStore/IPersistStore.h"
@import "Ceda/cxPersistStore/PDeque.h"
@import "Ceda/cxObject/IObjectVisitor.h"
#include "Ceda/cxUtils/TracerUtils.h"
#include "Ceda/cxUtils/Environ.h"

/*
Steps
    1.  Open or create store
    2.  Bootstrap a root object that is a PDeque
    3.  Push some values onto the deque
    4.  Print the deque
*/

namespace PDequeEx
{
    mDefinePDeque(MyDeque,ceda::int32,false,4096)
    
    void Run()
    {
        ceda::TraceGroup g("PDeque example");

        // Open or create store
        ceda::xstring path = ceda::GetCedaTestPath("PDequeEx.ced");
        ceda::close_ptr<ceda::PersistStore> pstore(ceda::OpenPersistStore(path.c_str()));

        // Open or create PSpace
        ceda::WPSpace pspace(ceda::OpenPSpace(pstore.get(), "P"));

        // Bootstrap MyDeque as a root of the PSpace
        MyDeque* root = ceda::BootstrapPSpaceRoot<MyDeque>("R");
        
        // Push element onto PDeque, and display PDeque
        {
            ceda::CSpaceTxn txn;

            root->push_back( (ceda::int32) root->size());
            
            Tracer() << "Size = " << root->size() << '\n';
            for (MyDeque::iterator i = root->begin() ; i != root->end() ; ++i)
            {
                Tracer() << *i << ' ';
            }
            Tracer() << '\n';
        }
    }
}


