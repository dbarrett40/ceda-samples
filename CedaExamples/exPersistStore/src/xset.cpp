// xset.cpp
//
// Author Jesse Pepper
// (C)opyright Cedanet Pty Ltd 2012

@import "Ceda/cxObject/WCSpace.h"
@import "Ceda/cxPersistStore/PrettyPrint.h"
@import "Ceda/cxPersistStore/xset.h"
#include "Ceda/cxUtils/TracerUtils.h"

namespace ceda
{
    mImplement_xset(int, true, true, false, false, false );
}

namespace xsetEx
{

struct Group
{
    ceda::xset<int> Members;
};

$model+ MopContainingModel
{
    ceda::xmap<int32,int32> MupMember;
};

ceda::xostream& operator<<( ceda::xostream& os, const Group& g )
{
    os << g.Members;
    return os;
}

void Run()
{
    ceda::TraceGroup g("xset");
    
    ceda::LocalCSpace lcs;
    ceda::CSpaceLock _;
    ceda::xvector<Group> groups(4);
    //ceda::array<Group> groups2(4);
    Group groups2[4];
    MopContainingModel groups3[4];
    
    groups[0].Members.insert( 7 );
    groups2[0].Members.insert( 8 );
    groups3[0].MupMember[0] = 7;
    
    for ( int i = 0 ; i < 4 ; ++i )
    {
        Tracer () << groups2[i] << ceda::endl;
    }    
    
    Tracer() << "Groups are: \n" << groups << ceda::endl;
    cxAssert( groups2[1].Members.size() == 0 );
    cxAssert( groups[1].Members.size() == 0 );
}

}
