// TypeOpsEx.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2012

@import "CedaExamples/lxPersistStore/BinaryNode.h"
@import "Ceda/cxPersistStore/IPersistStore.h"
@import "Ceda/cxPersistStore/pref.h"
@import "Ceda/cxPersistStore/IPrefVisitor.h"
@import "Ceda/cxObject/IObjectVisitor.h"
@import "Ceda/cxObject/ReflectionByteCodeTypes.h"
@import "Ceda/cxObject/ReflectionByteCodeValue.h"
@import "Ceda/cxObject/OpenVariant.h"
#include "Ceda/cxUtils/TracerUtils.h"

namespace ceda
{
    // Hmmm: unless this appears in namespace ceda it doesn't work!  Why?
    $type+ int32[3];
}

namespace TypeOpsEx
{
    $model+ X
    {
    };
    
    $enum+ E
    {
    };
    
    $adt+ A
    {
    };

    $adt+ implement A
    {
    }   ;

    $type+ ceda::xvector<X>;
    $type+ ceda::xvector<E>;
    
    void Run()
    {
        ceda::TraceGroup g("TypeOps example");
        
        @for (T in 
            [
                void,
                ceda::int8,
                ceda::int16,
                ceda::int32,
                ceda::int64,
                ceda::uint8,
                ceda::uint16,
                ceda::uint32,
                ceda::uint64,
                ceda::float32,
                ceda::float64,
                ceda::char8,
                ceda::char16,
                ceda::string8,
                ceda::string16,
                const void,
                const ceda::int8,
                const ceda::int16,
                const ceda::int32,
                const ceda::int64,
                const ceda::uint8,
                const ceda::uint16,
                const ceda::uint32,
                const ceda::uint64,
                const ceda::float32,
                const ceda::float64,
                const ceda::char8,
                const ceda::char16,
                const ceda::string8,
                const ceda::string16,
                ceda::ptr<ceda::IObject>,
                ceda::ptr<const ceda::IObject>,
                ceda::openvariant<ceda::IObject>,
                ceda::openvariant<const ceda::IObject>,
                ceda::pref<ceda::IObject>,
                ceda::pref<const ceda::IObject>,
                ceda::cref<BinaryNode>,
                ceda::cref<const BinaryNode>,
                ceda::IObject,
                BinaryNode,
                X,
                E,
                A,
                ceda::int32[3],
                ceda::xvector<X>,
                ceda::xvector<E>
            ])
        {
            // todo : compiler errors
            //Tracer() << @str(T : ) << ceda::GetReflectionByteCode< T >() << " typeops = " << ceda::FindTypeOps(ceda::GetReflectionByteCode< T >()) << '\n';

        }

        // todo : support openvariant<>.  Why does ptr<> work?
        @for (T in 
            [
                ceda::int8,
                ceda::int16,
                ceda::int32,
                ceda::int64,
                ceda::uint8,
                ceda::uint16,
                ceda::uint32,
                ceda::uint64,
                ceda::float32,
                ceda::float64,
                ceda::char8,
                ceda::char16,
                ceda::string8,
                ceda::string16,
                ceda::ptr<ceda::IObject>,
                ceda::ptr<const ceda::IObject>,
                ceda::pref<ceda::IObject>,
                ceda::pref<const ceda::IObject>,
                ceda::cref<BinaryNode>,
                ceda::cref<const BinaryNode>,
                BinaryNode,
                X,
                E,
                A,
                ceda::int32[3],
                ceda::xvector<X>,
                ceda::xvector<E>
            ])
        {
            ceda::GetTypeOps<T>();

            // todo : compiler errors
            //cxVerify(ceda::FindTypeOps(ceda::GetReflectionByteCode< T >()));
            
        }

        const ceda::TypeOps& t1 = ceda::GetTypeOps< ceda::pref<ceda::IObject> >();
        ceda::ReflectionByteCode r1 = ceda::GetReflectionByteCode< ceda::pref<ceda::IObject> >();

        const ceda::TypeOps& t2 = ceda::GetTypeOps< ceda::int32[3] >();
    }
}


