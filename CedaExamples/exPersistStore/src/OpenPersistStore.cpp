// OpenPersistStore.cpp
//
// Author Jesse Pepper, David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "Ceda/cxPersistStore/IPersistStore.h"
@import "Ceda/cxObject/CSpace.h"
#include "Ceda/cxUtils/TracerUtils.h"
#include "Ceda/cxUtils/Environ.h"

namespace OpenPersistStore
{
ceda::ConstStringZ pSpaceName = "myPspace";
ceda::ConstStringZ rootName = "myRoot";

///////////////////////////////////////////////////////////////////////////////////////////////////
// X
//
// This is about the simplest class implementing IPersistable you could possibly write.

$struct X isa ceda::IPersistable 
{
    void Serialise(ceda::Archive& ar) const {}
    void Deserialise(ceda::InputArchive& ar) {}
};

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
This example creates a new store, adds a PSpace to the store and adds a root to the PSpace
This is done using the C style API defined in IPersistStore.h
*/

void CreateNewStore1()
{
    ceda::TraceGroup g("Create new store - using C API");
    
    // OM_CREATE_ALWAYS will delete an existing store with the same name

    ceda::xstring storePath = ceda::GetCedaTestPath("OpenPersistStore0.ced");
    Tracer() << "Creating new store " << storePath << '\n';
    ceda::PersistStore* ps = ceda::OpenPersistStore(storePath.c_str(), ceda::OM_CREATE_ALWAYS);
    
    {
        ceda::PSpace* p = NULL;
        {
            // Open a transaction that doesn't lock any PSpaces (which it can't because there 
            // aren't any!)
            //ceda::PersistStoreTxn* txn = OpenTxn(ps,NULL,0);
            
            Tracer() << "Creating PSpace " << pSpaceName << '\n';
            p = OpenPSpace(ps, pSpaceName, ceda::OM_CREATE_ALWAYS);

            ceda::SetThreadPSpace(p);
            ceda::SetThreadPtr<ceda::CSpace>(GetCSpace(p));
            
            Tracer() << "Creating root " << rootName << '\n';
            ceda::ptr<ceda::IPersistable> x = new X;
            ceda::RegisterGcObject(x);
            AddRoot(p, rootName, x);
            
            ceda::SetThreadPSpace(NULL);
            ceda::SetThreadPtr<ceda::CSpace>(NULL);

            //Close(txn);
        }
        Close(p);
    }
    Close(ps);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Example using C++ interface

void CreateNewStore2()
{
    ceda::TraceGroup g("Create new store - using C++ API");
    
    // OM_CREATE_ALWAYS will delete an existing store with the same name
    ceda::xstring storePath = ceda::GetCedaTestPath("OpenPersistStore0.ced");
    Tracer() << "Creating new store " << storePath << '\n';
    ceda::close_ptr<ceda::PersistStore> pstore(ceda::OpenPersistStore(storePath.c_str(), ceda::OM_CREATE_ALWAYS));

    // Open or create pspace
    Tracer() << "Creating PSpace " << pSpaceName << '\n';
    ceda::WPSpace pspace(ceda::OpenPSpace(pstore.get(), pSpaceName));

    // Open or create an X as a root of the pspace
    Tracer() << "Creating root " << rootName << '\n';
    X* root = ceda::BootstrapPSpaceRoot<X>(rootName);
}

void Run()
{
    // todo: trips an assertion because ceda::RegisterGcObject(x) is called without a lock on the CSpace.
    //CreateNewStore1();
    
    CreateNewStore2();
}

}