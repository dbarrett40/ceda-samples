// SingleObjectWithSerialiseFunction.cpp
//
// David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2013

@import "Ceda/cxPersistStore/IPersistStore.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"
#include "Ceda/cxUtils/Environ.h"
#include <assert.h>

/*
Example generated output:

    SingleObjectWithSerialiseFunction
      Create store, write object with 10000000 integers, close store......  164.65 ms
      Reopen store, read object with 10000000 integers, close store.......  143.23 ms
*/

namespace SingleObjectWithSerialiseFunction
{
    $struct X isa ceda::IPersistable
    {
        void Serialise(ceda::Archive& ar) const 
        {
            ar << buffer_;
        }
        void Deserialise(ceda::InputArchive& ar) 
        { 
            ar >> buffer_;
        }
        ceda::xvector<int> buffer_;
    };

    void Run()
    {
        Tracer() << "SingleObjectWithSerialiseFunction\n";
        ceda::TraceIndenter indent;

        ceda::xstring path = ceda::GetCedaTestPath("SingleObjectWithSerialiseFunction.ceda");
        const int count = 10000000;   // Size of store will be about 40MB
        {
            ceda::HPTimer timer;
            ceda::PersistStore* pstore = ceda::OpenPersistStore(path.c_str(),ceda::OM_CREATE_ALWAYS);
            {
                ceda::WPSpace pspace(ceda::OpenPSpace(pstore, "MyPSpace"));
                X* root = ceda::BootstrapPSpaceRoot<X>("MyRoot");

                ceda::CSpaceTxn txn;
                root->buffer_.resize(count);
                for (int i=0 ; i < count ; ++i) root->buffer_[i] = i;
            }
            Close(pstore);
            timer.Done() << "Create store, write object with " << count << " integers, close store";
        }
        {
            ceda::HPTimer timer;
            ceda::PersistStore* pstore = ceda::OpenPersistStore(path.c_str());
            {
                ceda::WPSpace pspace(ceda::OpenPSpace(pstore, "MyPSpace"));
                X* root = ceda::BootstrapPSpaceRoot<X>("MyRoot");

                ceda::CSpaceTxn txn;
                assert(root->buffer_.size() == count);
                for (int i=0 ; i < count ; ++i) assert(root->buffer_[i] == i);
            }
            Close(pstore);
            timer.Done() << "Reopen store, read object with " << count << " integers, close store";
        }
    }
}