// ModifyPO.cpp
//
// Author Jesse Pepper, David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "Ceda/cxPersistStore/IPersistStore.h"
#include "Ceda/cxUtils/TracerUtils.h"
#include "Ceda/cxUtils/FileException.h"
#include "Ceda/cxUtils/Environ.h"
#include "Ceda/cxUtils/TestTimer.h"

/*
Example generated output:

    Modify PO
    {
        Value = 5
        Value = 6
        Value = 7
    }
*/

namespace ModifyPO
{
    $struct X isa ceda::IPersistable :
        model
        {
            ceda::int32 m_value;
        }
    {
    };

    void Work()
    {
        // Open or create store
        ceda::xstring path = ceda::GetCedaTestPath("ModifyPO.ced");
        ceda::close_ptr<ceda::PersistStore> pstore(ceda::OpenPersistStore(path.c_str()));
        
        // Open or create pspace named P
        ceda::WPSpace pspace(ceda::OpenPSpace(pstore.get(), "P"));

        // Bootstrap an X as a root of the pspace named R
        X* root = ceda::BootstrapPSpaceRoot<X>("R");
        
        // Increment value
        {
            ceda::CSpaceTxn txn;

            {
                const int N = 100000;
                ceda::RateTimer timer("Increment m_value", N);
            
                // Note that this implicity marks the object as dirty for us.
                // i.e. there is no need for
                //    root->MarkAsDirtyWithoutTrace();
                for (int i=0 ; i < N ; ++i)
                {
                    root->m_value = root->m_value + 1;
                }
            }
                        
            Tracer() << "Value = " << root->m_value << '\n';
        }
    }
    
    void Run()
    {
        ceda::TraceGroup g("Modify PO");
        for (int i=0 ; i < 3 ; ++i)
        {
            Work();
        }
    }
}