// DataSources.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2009

@import "Ceda/cxPersistStore/IPersistStore.h"
@import "Ceda/cxObject/PrintReflectedVariable.h"
#include "Ceda/cxUtils/TracerUtils.h"
#include "Ceda/cxUtils/AutoCloser.h"
#include "Ceda/cxUtils/Environ.h"

namespace DataSources
{
    $struct X isa ceda::IPersistable :
        model
        {
            ceda::float64 x;
            ceda::int32 A[10];
            ceda::xvector<ceda::float32> L;
            ceda::cref<X> p;
            ceda::xvector<ceda::cref<X> > P;
        }
    {
    };
    
    void Run()
    {
        ceda::TraceGroup g("DataSources Example");
        
        ceda::xstring path = ceda::GetCedaTestPath("ds.ced");
        {
            // Create store
            ceda::close_ptr<ceda::PersistStore> pstore(ceda::OpenPersistStore(path.c_str(),ceda::OM_CREATE_ALWAYS));

            // Create pspace and bootstrap an X as a root in the PSpace
            ceda::WPSpace pspace(ceda::OpenPSpace(pstore.get(), "P"));
            X* r = ceda::BootstrapPSpaceRoot<X>("R");
            
            // Sleep for long enough for the original object to persist.  This verifies that
            // the subsequent operations implicitly mark the object as dirty as required.
            //Sleep(3000);
            
            {
                ceda::CSpaceTxn txn;
                
                r->x = 3.14;
                
                r->A[3] = 10;
                r->A[4] = 20;
                
                float v[] = {1.2f, 7.1f, 10.0f};
                r->L.insert(0,v,3);
                
                r->p = $new X;

                ceda::xvector< ceda::cref<X> > t;
                t.push_back($new X);
                t.push_back($new X);
                r->P.insert(0, t.data(), t.size());
            }
        }

        {
            // Open existing store
            ceda::close_ptr<ceda::PersistStore> pstore(ceda::OpenPersistStore(path.c_str(), ceda::OM_OPEN_EXISTING));

            // Open pspace and the root
            ceda::WPSpace pspace(ceda::OpenPSpace(pstore.get(), "P"));
            X* r = ceda::BootstrapPSpaceRoot<X>("R");
            
            {
                ceda::CSpaceTxn txn; //(ceda::ECSpaceLockMode::SharedRead);
                ceda::PrintInstance(Tracer(), r);
            }
        }
    }
}