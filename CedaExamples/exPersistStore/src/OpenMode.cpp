// OpenMode.cpp
//
// Author Jesse Pepper, David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "Ceda/cxPersistStore/IPersistStore.h"
@import "Ceda/cxObject/CSpace.h"
#include "Ceda/cxUtils/FileException.h"
#include "Ceda/cxUtils/TracerUtils.h"
#include "Ceda/cxUtils/FileTools.h"
#include "Ceda/cxUtils/Environ.h"

/*
This example demonstrates the use of the EOpenMode value which is passed to the OpenPersistStore 
function.

The behaviour for all scenarios is summarised in Ceda/cxUtils/pub/OpenMode.h.

This test assumes the directory c:/ceda/test is defined
*/

/*
Generated output:
    OpenMode Example
    {
        File exists : NO     OM_CREATE_NEW
            Store opened successfully
            [OK]

        File exists : NO     OM_DELETE_EXISTING
            Exception: File error (h:/OpenMode.ced) : The system cannot find the file specified.
            [OK]

        File exists : NO     OM_CREATE_ALWAYS
            Store opened successfully
            [OK]

        File exists : NO     OM_OPEN_EXISTING
            Exception: File error (h:/OpenMode.ced) : The system cannot find the file specified.
            [OK]

        File exists : NO     OM_OPEN_ALWAYS
            Store opened successfully
            [OK]

        File exists : YES    OM_CREATE_NEW
            Exception: File error (h:/OpenMode.ced) : The file exists.
            [OK]

        File exists : YES    OM_DELETE_EXISTING
            Store opened successfully
            [OK]

        File exists : YES    OM_CREATE_ALWAYS
            Store opened successfully
            [OK]

        File exists : YES    OM_OPEN_EXISTING
            Store opened successfully
            [OK]

        File exists : YES    OM_OPEN_ALWAYS
            Store opened successfully
            [OK]

    }
*/

namespace OpenMode
{
    void OpenStoreTest(const ceda::xchar* storePath, ceda::EOpenMode mode, bool fileExists, bool shouldSucceed)
    {
        if (fileExists)
        {
            cxAssert(ceda::FileExists(storePath));
        }
        else
        {
            ceda::DeleteFile_u8(storePath); 
        }
        
        Tracer() << "File exists : " << (fileExists ? "YES" : "NO ") << "    " << ceda::GetReflectedEnum<ceda::EOpenMode>().GetString(mode) << '\n';
        
        try
        {
            ceda::PersistStore* ps = ceda::OpenPersistStore(storePath,mode);
            if (shouldSucceed)
            {
                Tracer() << "    Store opened successfully\n"
                         << "    [OK]\n\n";
            }
            else
            {
                cxAssert(0);
            }
            Close(ps);
        }
        catch(ceda::FileException& e)
        {
            if (shouldSucceed)
            {
                cxAssert(0);
            }
            else
            {
                Tracer() << "    Exception: " << e
                         << "    [OK]\n\n";
            }
        }
    }
    
    void Run()
    {
        ceda::TraceGroup g("OpenMode Example");

        ceda::xstring storePath = ceda::GetCedaTestPath("OpenMode.ced");
        const ceda::xchar* path = storePath.c_str();

        // Delete any existing store
        ceda::DeleteFile_u8(path);
        
        @def FILE_EXISTS = true
        @def SUCCESS = true

        // Tests when file doesn't already exist
        OpenStoreTest(path, ceda::OM_CREATE_NEW,      !FILE_EXISTS, SUCCESS);
        OpenStoreTest(path, ceda::OM_DELETE_EXISTING, !FILE_EXISTS, !SUCCESS);
        OpenStoreTest(path, ceda::OM_CREATE_ALWAYS,   !FILE_EXISTS, SUCCESS);
        OpenStoreTest(path, ceda::OM_OPEN_EXISTING,   !FILE_EXISTS, !SUCCESS);
        OpenStoreTest(path, ceda::OM_OPEN_ALWAYS,     !FILE_EXISTS, SUCCESS);

        // Tests when file already exists
        OpenStoreTest(path, ceda::OM_CREATE_NEW,      FILE_EXISTS, !SUCCESS);
        OpenStoreTest(path, ceda::OM_DELETE_EXISTING, FILE_EXISTS, SUCCESS);
        OpenStoreTest(path, ceda::OM_CREATE_ALWAYS,   FILE_EXISTS, SUCCESS);
        OpenStoreTest(path, ceda::OM_OPEN_EXISTING,   FILE_EXISTS, SUCCESS);
        OpenStoreTest(path, ceda::OM_OPEN_ALWAYS,     FILE_EXISTS, SUCCESS);

        // Clean up
        ceda::DeleteFile_u8(path);
    }
}
