// EagerSchemaEvolution.cpp
//
// David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2013

@import "Ceda/cxPersistStore/IPersistStore.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"
#include "Ceda/cxUtils/Environ.h"
#include <assert.h>

/*
This example illustrates the fact that an implementation of Deserialise() is able to perform eager
schema evolution - i.e. it can modify the state and call MarkAsDirtyWithoutTrace() to cause the 
object to be marked as dirty and written back to disk when the dirty object set is next processed.  
It is even possibe to assign pref members with $new'd objects and make calls to DeclareReachable() 
so they are allocated oids.

Example generated output:

    EagerSchemaEvolution

      Open store
      X::OnCreate()
          X::Serialise()

      Set currentSchema = 2

      Open store
      X::Deserialise()
        schema = 1
        Create Y eagerly during deserialisation
      X::GetY()
      y.v = 0
      Assigned y.v = 1
          X::Serialise()

      Open store
      X::Deserialise()
        schema = 2
      X::GetY()
      y.v = 1
      Assigned y.v = 2
*/

namespace EagerSchemaEvolution
{
    $class Y isa ceda::IPersistable : 
        model
        {
            ceda::int32 v;
        }
    {
    };

    // A hack to simulate schema evolution.  We use a global variable which is initially 1 and
    // this is incremented in order to emulate schema evolution when we reopen the store.
    int currentSchema = 1;
    
    @def bool EAGER = true

    $class X isa ceda::IPersistable 
    {
    public:
        void OnCreate(bool local)
        {
            Tracer() << "X::OnCreate()\n";
            ceda::TraceIndenter indent;
            if (currentSchema == 2)
            {
                Tracer() << "Create Y\n";
                y = $new Y;
            }
        }

        Y& GetY()
        {
            Tracer() << "X::GetY()\n";
            ceda::TraceIndenter indent;
            cxAssert(currentSchema == 2);

            @if (EAGER)
            {
                cxAssert(y);
            }
            @else
            {
                if (!y)
                {
                    Tracer() << "Create Y lazily in GetY()\n";
                    Y* py = $new Y;
                    y = py;
                    ceda::DeclareReachable(this,py);
                    MarkAsDirtyWithoutTrace();
                }
            }
            return *y;
        }

        void Serialise(ceda::Archive& ar) const
        {
            Tracer() << "X::Serialise()\n";
            ceda::TraceIndenter indent;
            ceda::SerialiseSchema(ar,currentSchema);
            if (currentSchema == 2)
            {
                ar << y;
            }
        }

        void Deserialise(ceda::InputArchive& ar)
        {
            Tracer() << "X::Deserialise()\n";
            ceda::TraceIndenter indent;
            ceda::schema_t schema;
            ceda::DeserialiseSchema(ar,2,schema);
            Tracer() << "schema = " << schema << '\n';

            if (currentSchema == 2)
            {
                if (schema == 2)
                {
                    cxAssert(currentSchema == 2);
                    ar >> y;
                }
                else
                {
                    @if (EAGER)
                    {
                        // Can we $new Y and mark X as dirty here?
                        Tracer() << "Create Y eagerly during deserialisation\n";
                        Y* py = $new Y;
                        y = py;
                        ceda::DeclareReachable(this,py);
                        MarkAsDirtyWithoutTrace();
                    }
                    @else
                    {
                        y = ceda::null;            
                    }
                }
            }
        }

    private:
        ceda::cref<Y> y;
    };

    void Run()
    {
        Tracer() << "EagerSchemaEvolution\n";
        ceda::TraceIndenter indent;

        ceda::xstring path = ceda::GetCedaTestPath("EagerSchemaEvolution.ceda");
        {
            Tracer() << "\nOpen store\n";
            ceda::PersistStore* pstore = ceda::OpenPersistStore(path.c_str(),ceda::OM_CREATE_ALWAYS);
            {
                ceda::WPSpace pspace(ceda::OpenPSpace(pstore, "MyPSpace"));
                X* root = ceda::BootstrapPSpaceRoot<X>("MyRoot");
            }
            Close(pstore);
        }
        Tracer() << "\nSet currentSchema = 2\n";
        currentSchema = 2;

        for (int i=0 ; i < 2 ; ++i)
        {
            Tracer() << "\nOpen store\n";
            ceda::PersistStore* pstore = ceda::OpenPersistStore(path.c_str());
            {
                ceda::WPSpace pspace(ceda::OpenPSpace(pstore, "MyPSpace"));
                X* root = ceda::BootstrapPSpaceRoot<X>("MyRoot");

                ceda::CSpaceTxn txn;
                Y& y = root->GetY();
                Tracer() << "y.v = " << y.v << '\n';

                y.v = i+1;
                Tracer() << "Assigned y.v = " << y.v << '\n';
            }
            Close(pstore);
        }
    }
}