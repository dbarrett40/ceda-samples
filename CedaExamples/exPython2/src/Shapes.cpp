// Shapes.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2017

@import "Ceda/cxPython/cxPython.h"
#include "Ceda/cxUtils/TracerUtils.h"

namespace shapes1
{
    void Run()
    {
        ceda::TraceGroup g("shapes example 1");

        PyRun_SimpleString(
            @strx
            (
                dt = rootnamespace.dt
                geom = rootnamespace.geom

                print 'dir(geom.TShape) = ' + `dir(geom.TShape)`

                s = geom.TShape()
                print 'dir(s) = ' + `dir(s)`

                print 's = ' + `s`
                print 's.tag = ' + `s.tag`
                print 's.tagname = ' + `s.tagname`

                setattr(s, 'geom::TLine', geom.TLine( geom.TPoint(1,2), geom.TPoint(3,5) ) )
                print 's = ' + `s`
                print 's.tag = ' + `s.tag`
                print 's.tagname = ' + `s.tagname`

                polygon = geom.TPolygon( [ geom.TPoint(X=1,Y=2), geom.TPoint(X=7,Y=-2) ] )
                polygon.V.append( geom.TPoint(X=5,Y=1) )
                polygon.V[0].X = 1000
                polygon.V.append( polygon.V[1] )
                print 'polygon = ' + `polygon`

                picture = geom.TPicture(
                        Name = "my picture",
                        Author = "P. Hunt",
                        Colour = geom.EColour.magenta,
                        CreationDate = dt.TDateTime( Date = dt.TDate(2006,dt.TMonth.Jun,23) ),
                        BoundingBox = geom.TRect( geom.TPoint(0,0), geom.TPoint(800,600) ),
                        Rectangles =
                        [
                            geom.TRect( geom.TPoint(X=5,Y=8), geom.TPoint(15,12) ),
                            geom.TRect( geom.TPoint(Y=5,X=8), geom.TPoint(10,17) )
                        ],
                        Circles =
                        [
                            geom.TCircle( geom.TPoint(), 5 ),
                            geom.TCircle( R=2 ),
                            geom.TCircle( C=geom.TPoint(X=100) )
                        ]
                    )
                print 'picture = ' + `picture`
            ));
    }
}

namespace shapes
{
    void Run()
    {
        shapes1::Run();
    }
}

