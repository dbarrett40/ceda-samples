// exPython2.cpp

@import "Ceda/cxPython/cxPython.h"
@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/TargetRegistry.h"
#include "Ceda/cxUtils/MsWindows.h"
#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/TracerUtils.h"
#include "Ceda/cxUtils/HPTime.h"

mRegisterLeafTarget(
    {
        // version         year mon day
        { "Release 0.0",   {2008,8,24}  },      // Release #0
    })

#if defined(_WIN32)
    static HINSTANCE static_hModule = NULL;

    HINSTANCE GetLocalModuleHandle()
    {
	    cxAssert(static_hModule);
	    return static_hModule;
    }
#endif

namespace Models { void Run(); }
namespace Vectors { void Run(); }
namespace shapes { void Run(); }
namespace pizza { void Run(); }

$function+ void Assert(bool c)
{
    cxAssert(c);
}

#if defined(_WIN32)
    static ceda::DebugStringTraceObserver s_traceObserver;
#endif

void exPython2_RunAllExamples()
{
    Tracer() << "----------------------------------- exPython2 examples  -----------------------------------\n";
    ceda::HPTimer timer;
    #if defined(_WIN32)
        ceda::SetTraceObserver(&s_traceObserver);
    #endif

    ceda::xstring errorString;
    if (!ceda::InitialisePython(errorString))
    {
        Tracer() << "Error: " << errorString << '\n';
        return;
    }
    ceda::BindPythonSysOutputToTracer();
    
    PyRun_SimpleString(
        @strx
        (
            Assert = rootnamespace.Assert
        ));
    
    shapes::Run();
    Models::Run();
    Vectors::Run();
    pizza::Run();

    // Needed to avoid memory leaks.  Also to stop assertions that wrapper counts fall to zero.
    Py_Finalize();

    timer.Done() << "exPython2 examples completed successfully";
}

CEDA_MAIN_FUNCTION(argc,argv)
{
    CEDA_REGISTER_PROJECT
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exPython2_RunAllExamples();
#ifdef CEDA_CHECK_ASSERTIONS
    ReportLeaks(Tracer());
#endif
    ceda::DestroyTheTraceFile();
    return 0;
}

