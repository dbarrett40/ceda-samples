// BinaryNode.h
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2012

@import "lxPersistStore.h"
@import "Ceda/cxPersistStore/Models.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// BinaryNode

$struct+ BinaryNode isa ceda::IPersistable :
    model
    {
        pref<ceda::IObject> Info;
        movable<cref<BinaryNode> > Left;
        movable<cref<BinaryNode> > Right;
    }
{
};

