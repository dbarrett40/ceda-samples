// PsTreeNode.h
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2010

@import "lxPersistStore.h"
@import "Ceda/cxPersistStore/Models.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// PsTreeNode

$class+ PsTreeNode isa ceda::IPersistable
{
public:
    PsTreeNode();
    ~PsTreeNode();

    // Implementation of IObject
    void VisitObjects(ceda::IObjectVisitor& v) const { v << m_parent << m_children; }

    // Implementation of IPersistable
    $implementing ceda::IPersistable {Serialise Deserialise};
    void VisitPrefs(ceda::IPrefVisitor& v) const { v << m_children; v.VisitParent(m_parent); }

    bool operator==(const PsTreeNode& rhs) const;
    bool operator!=(const PsTreeNode& rhs) const { return !operator==(rhs); }
    
    void ValidateChildren() const;
    ceda::ssize_t GetNumChildren() const { return m_children.size(); }
    ceda::int32 GetAmount() const { return m_amount; }
    void OffsetAmount(ceda::int32 delta);
    PsTreeNode* RemoveChild(ceda::ssize_t index);
    void RemoveThisNode();
    void DeleteThisNode();
    void AddChild(ceda::ssize_t index, PsTreeNode* c);
    void CheckInvariant() const;
    void GetAllNodes(ceda::xvector<PsTreeNode*>& nodes);
    void TraceTree() const;

    // Returns true iff the entire tree is resident in the local store.
    bool IsResidentInStore() const;
    
    // Trace the largest subset of the tree which is itself a tree and for which the nodes are 
    // resident in the local store.
    // Returns true iff the entire tree is resident in the local store.
    bool TraceSubTreeResidentInStore() const;

    void GetOidsNotResidentInStore(ceda::xvector<ceda::OID>& oids) const;
    
    void BuildTree(ceda::ssize_t depth, ceda::ssize_t fanout);
    ceda::ssize_t GetNumNodes() const;

private:
    // Apply offset of 'delta' to the total amount in this node and all its ancestors
    // Marks all affected nodes as dirty as required
    void AdjustTotals(ceda::int32 delta);

private:
    ceda::cref<PsTreeNode> m_parent;

    // The amount for this node
    ceda::int32 m_amount;

    // The amount for this node plus all of its descdendents
    ceda::int32 m_totalAmount;

    ceda::xvector<ceda::cref<PsTreeNode> > m_children;
};

@api int GetNumAddedNodes();

