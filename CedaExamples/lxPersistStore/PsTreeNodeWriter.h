// PsTreeNodeWriter.h
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2010

@import "PsTreeNode.h"
#include "Ceda/cxUtils/IsaacRandom.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// PsTreeNodeWriter
/*
Allows for making random changes to a given tree of Nodes
*/

class @api PsTreeNodeWriter
{
public:
    PsTreeNodeWriter(PsTreeNode* rootNode, bool seedFromClock, bool enableTrace);
    void InitIsaac(const ceda::IsaacRandomNumberGen& isaac) { m_isaac = isaac; }
    PsTreeNode* PickNode();
    void AddRandomNode(PsTreeNode* c);
    void DeleteRandomNode();
    void MoveRandomNode();
    void AddRandomAmount();
    void Run(ceda::ssize_t minOps, ceda::ssize_t maxOps);
private:
    ceda::IsaacRandomNumberGen m_isaac;
    PsTreeNode* m_rootNode;
    bool m_enableTrace;
};


