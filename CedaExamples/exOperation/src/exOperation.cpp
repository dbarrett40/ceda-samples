// exOperation.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2008

#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"
@import "Ceda/cxObject/TargetRegistry.h"

mRegisterLeafTarget(
    // Releases
    {
        // version         year mon day
        { "Release 0.0",   {2008,8,24}   },      // Release #0
    })

namespace WithoutSocketsEx { void Run(); }
void TwoWorkingSets();
void ReplicateOnDemandExample();
void NongracefulShutdownTest();
namespace airlineres { void AirplaneReservationSystemExample(); }
void DeleteTest();
namespace RodPerformance { void Run(); }

void exOperation_RunAllExamples()
{
    Tracer() << "----------------------------------- exOperation examples  -----------------------------------\n";
    ceda::HPTimer timer;
    WithoutSocketsEx::Run();
    ReplicateOnDemandExample();
    TwoWorkingSets();
    airlineres::AirplaneReservationSystemExample();
    DeleteTest();
    NongracefulShutdownTest();
    //RodPerformance::Run();
    timer.Done() << "exOperation examples completed successfully";
}

CEDA_MAIN_FUNCTION(argc,argv)
{
    CEDA_REGISTER_PROJECT
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exOperation_RunAllExamples();
#ifdef CEDA_CHECK_ASSERTIONS
    ReportLeaks(Tracer());
#endif
    ceda::DestroyTheTraceFile();
    return 0;
}

