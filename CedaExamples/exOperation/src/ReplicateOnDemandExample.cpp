// ReplicateOnDemandExample.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2013

@import "Ceda/cxOperation/UTRoot.h"
@import "Ceda/cxOperation/IWorkingSetMachine.h"
@import "Ceda/cxWorkingSetIpc/WorkingSetIPC.h"
@import "Ceda/cxPersistStore/IPersistStore.h"
@import "Ceda/cxObject/DGNode.h"
@import "Ceda/cxObject/IObjectVisitor.h"
@import "Ceda/cxObject/PrintReflectedVariable.h"
#include "Ceda/cxUtils/PagedBufferAsArchive.h"
#include "Ceda/cxUtils/TestTimer.h"
#include "Ceda/cxUtils/HPTime.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/Environ.h"
#include "Ceda/cxUtils/Hex.h"

/*
Proposal
--------

We will add basic ROD support to WsipcSession.  ROD objects have higher priority than deltas.  Only
send a delta when no object has been requested.

There are three messages:
    delta
    ROD object
    ROD request

We will precede each message with a byte for multiplexing.  This is less work than adding multiple
readers / writers.

When a ROD request is received, the oid is inserted into a queue of oids to be requested.

Interaction with the rest of the system
---------------------------------------

We have a hook in cxPersistStore to know when async oid request fails because object is not found
in the LSS.  That should trigger the request for an object on the connection.

    $interface+ IAsyncBindRequestHandler
    {
        // is this called with a CSpace lock?
        void OnObjectNotFoundInLss(OID oid)
    };

A PSpace can have many working sets.  We expect the working set to be set in TLS when async bind
is called.  But we don't find out until a worker thread goes to the LSS, so no TLS is available.
So we can't use TLS to direct the OnObjectNotFoundInLss message to the approprate working set.

Our approach is to:
    -   When an object is received we use visitprefs to ensure the oidhighs of its outgoing
        pointers are inserted into the set<OidHigh> for rod objects recorded by the working set
        [or we assume a tree of objects all share the same oid high so this is unnecessary]

    -   Allow for multiple hooks to be registered.

    -   Hooks can be unregistered. A double linked list is the most appropriate data structure
        because it allows for fast removal from any position.

    -   Hooks are registered / unregistered in a CSpace lock

    -   All registered hooks receive the OnObjectNotFoundInLss message, in a CSpace lock

    -   Each working set records set<OidHigh> for rod objects, protected by a CSpace lock
        Therefore a working set can determine whether a OnObjectNotFoundInLss() message is
        relevant.

    -   WsipcSession implements IAsyncBindRequestHandler and registers itself on creation and
        unregisters itself when it is closed.

    -   WsipcSession::OnObjectNotFoundInLss(oid) tests the OidHigh to see if it's applicable.
        If so then it pushes the oid onto a member

            xvector<OID> pendingRodRequest_

        If oidsPendingRodRequest_ transitions to non-empty then WsipcSession calls
        NotifyMoreMessagesToWrite(m_c);

    -   WsipcSession::GetNextMessage() sends pendingRodRequest_ if non-empty, then clears
        it.  Note that an atomic swap seems appropriate.  Object request messages have the highest 
        priority.

    -   WsipcSession::ReadMessage() reads an octet used to multiplex messages.  If this is a
        ROD request then it deserialises the set of oids and pushes into a member
        
            xvector<OID> pendingRod_

        If pendingRod_ transitions to non-empty then WsipcSession calls
        NotifyMoreMessagesToWrite(m_c);

    -   WsipcSession has a $agent member which is active for the life of the WsipcSession.  This
        allows the WsipcSession to have async bind requests for objects which are loaded from disk
        or may be resolved using a connection to some other working set.

        The agent is associated with the calculation of a vector<pref> for pointers to objects that
        need to be sent.

        -   $agent is cleaned when :

        -   calc function:
                Calls AsyncBind() on a set of oids

    -   We will need to think about infinite loops for object requests etc.

done
----

-	PreparePrefs must account for object that is marked as replicate on demand, and allocate
	OIDs using the right OID high.

-	Provide example where there are two working sets (in the same PSpace doesn't matter) 
	and objects are declared as replicate on demand when they are created.  Confirm that
	these are not replicated to the other working set - i.e. we get a dangling oid exception.

-	Provide functions in cxOperation to send and receive replicated objects (without 
	committing to the protocol or when to send/receive objects)

Proposal for test
-----------------

We have a set of sites. Some are always custodians some are never custodians.  We allow for 
random connection topologies over time.  If a non-custodian connects to a non-custodian it
automatically kills the session without crashing the process.

Any site allows for generating random operations.  These involve inserting a tree of rod
objects.

Each site has an agent, with a calc function that can call async get on randomly selected
cref's.
*/

///////////////////////////////////////////////////////////////////////////////////////////////////
// RepNode

$struct+ RepNode <<rod -os>> isa ceda::IPersistable :
    model
    {
        int32 Value;
        cref<RepNode> Child1;
        cref<RepNode> Child2;
    }
{
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// RootNode

$struct+ RootNode <<-os>> isa ceda::IPersistable :
    model
    {
        int32 Value;
        cref<RepNode> Child1;
        cref<RepNode> Child2;
    }
{
};

///////////////////////////////////////////////////////////////////////////////////////////////////
void ReplicateOnDemandExample1()
{
    Tracer() << "ReplicateOnDemandExample1\n";
    ceda::TraceIndenter indent;

    const ceda::xstring UtEntryName("MyApp");
    ceda::xstring path = ceda::GetCedaTestPath("ReplicateOnDemandExample.ced");
    ceda::PersistStore* pstore = ceda::OpenPersistStore(path.c_str(), ceda::OM_CREATE_ALWAYS);
    {
        // Open or create a PSpace
        ceda::WPSpace pspace(ceda::OpenPSpace(pstore, "MyPSpace"));

        // Open or create two working sets in the PSpace
        ceda::WorkingSetMachine* ws1 = nullptr;
        ceda::WorkingSetMachine* ws2 = nullptr;
        {
            ceda::CSpaceTxn txn;
            ws1 = ceda::OpenWorkingSetMachine("WS1",true);
            ws2 = ceda::OpenWorkingSetMachine("WS2",false);
        }
        
        {
            // Create server and client
            ceda::TcpMsgServer* server;
            ceda::TcpMsgClient* client;
            {
                const ceda::WsipcSessionProtocolId protocolId = { "RODExample1", 1 };
                ceda::WsipcHostInfo localHostInfo;
                auto protocol = ceda::EProtocol::TCP_IPv4;
                int port = 3000;
                bool reuse_addr = false;
                const char* host = "127.0.0.1";
                const char* service = "3000";
                ceda::TcpMsgSessionSettings sessionSettings;

                ceda::CSpaceTxn txn;

                server = CreateTcpMsgServer(
                    protocol, port, reuse_addr, 
                    *CreateWsipcEndPoint(ws1, protocolId, localHostInfo), sessionSettings);

                client = CreateTcpMsgClient(
                    host, service, 
                    *CreateWsipcEndPoint(ws2, protocolId, localHostInfo), sessionSettings);
            }
            
            // Bootstrap RootNode under the UT root
            {
                ceda::CSpaceTxn txn;
                GetUTRoot(ws1).Map[UtEntryName].insert(0,$new RootNode);
            }

            // Generate some operations
            RepNode* c1;
            RepNode* c2;
            {
                ceda::CSpaceTxn txn;
                
                c1 = $new RepNode;
                c1->Value = 5;
                c2 = $new RepNode;
                c2->Value = 6;
                //ceda::SetReplicateOnDemand(c2);
                c1->Child1 = c2;
                //ceda::SetReplicateOnDemand(c1);

                RootNode* t = ceda::GetUTRootEntry<RootNode>(ws1,UtEntryName);
                t->Value = 10;             // Generate assignment operation
                t->Child1 = c1;

                Tracer() << "ws1 : c1 oid = " << GetOid(c1) << "  c2 oid = " << GetOid(c2) << '\n';
            }

            // Poll for quiescence
            Tracer() << "Polling for quiescence\n";
            while(1)
            {
                Sleep(10);
                ceda::CSpaceTxn txn;
                if (HistoryBufferSize(ws2) == 2) break;
            }

            /*
            Manually replicate objects
            */
            /*
            {
                ceda::RodWriter* w = CreateRodWriter(ws1);
                ceda::RodReader* r = CreateRodReader(ws2);

                using namespace ceda;

                xvector<octet_t> buffer;
                {
                    PagedBuffer pb;
                    {
                        OutputArchiveOnPagedBuffer ar(pb);
                        CSpaceTxn txn;
                        SerialiseObject(w,ar,c1);
                        SerialiseObject(w,ar,c2);
                    }
                    pb.WriteTo(buffer);
                }
    
                Tracer() << "buffer size = " << buffer.size() << '\n';

                {
                    TracerX os;
                    HexDump(os, 16, true, buffer.data(), buffer.data_end());
                    os << '\n';
                }

                {
                    CSpaceTxn txn;
                    InputArchive ar(buffer.data());
                    ptr<IPersistable> po;
                    DeserialiseObject(r,ar,po);
                    DeserialiseObject(r,ar,po);
                    cxAssert(ar == buffer.data() + buffer.size());
                }

                Close(w);
                Close(r);
            }
            */
            
            Tracer() << "Showing result on ws2\n";
            {
                ceda::CSpaceTxn txn;
                RootNode* t = ceda::GetUTRootEntry<RootNode>(ws2,UtEntryName);
                ceda::PrintInstance(Tracer(), t);
            }
            
            Close(server);
            Tracer() << "closed server\n";
            Close(client);
            Tracer() << "closed client\n";
        }
        
        {
            ceda::CSpaceTxn txn;
            Close(ws1);
            Close(ws2);
            Tracer() << "closed working sets\n";
        }
    }
    Close(pstore);
    Tracer() << "closed pstore\n";
}

$struct Y isa ceda::IObject
{
    $dep void A
        invalidate 
        {
            Tracer() << "[invalidate agent]\n";
        } 
        calc
        {
            Tracer() << "[calc agent]\n";

            ceda::PSpace* pspace = ceda::GetAssociatedPSpace(x);
            cxAssert(pspace);
            
            cxAssert(pspace == ceda::GetThreadPtr<ceda::PSpace>());
            
            ceda::cref<RepNode> const& C1 = x->Child1.read();
            ceda::cref<RepNode> const& C2 = x->Child2.read();
            
            allLoaded = true;
            /*
            for (ceda::ssize_t i = 0 ; i < C.size() ; ++i)
            {
                X* p = C[i].AsyncGet();
                
                Tracer() << "i = " << i 
                         << "  oid = " << C[i].GetOid() 
                         << "  p = " << p
                         << "  v = " << (p ? p->Value : -1)
                         << '\n';
                
                if (!p) allLoaded = false;
                
                //ceda::int32 v = C[i]->Value;
                //Tracer() << "i = " << i << "  v = " << v << '\n';
            }
            */

            RepNode* p1 = C1.AsyncGet();
                
            Tracer() << "  oid = " << C1.GetOid() 
                     << "  p = " << p1
                     << "  v = " << (p1 ? p1->Value : -1)
                     << '\n';
            if (!p1) allLoaded = false;

            RepNode* p2 = C2.AsyncGet();
                
            Tracer() << "  oid = " << C2.GetOid() 
                     << "  p = " << p2
                     << "  v = " << (p2 ? p2->Value : -1)
                     << '\n';
            //if (!p2) allLoaded = false;
        };
        
    RootNode* x;    
    mutable bool allLoaded;
};        

void ReplicateOnDemandExample()
{
    Tracer() << "ReplicateOnDemandExample\n";
    ceda::TraceIndenter indent;

    const ceda::xstring UtEntryName("MyApp");
    ceda::xstring path = ceda::GetCedaTestPath("ReplicateOnDemandExample.ced");
    ceda::PersistStore* pstore = ceda::OpenPersistStore(path.c_str(), ceda::OM_CREATE_ALWAYS);
    {
        // Open or create a PSpace
        ceda::WPSpace pspace(ceda::OpenPSpace(pstore, "MyPSpace"));

        // Open or create two working sets in the PSpace
        ceda::WorkingSetMachine* ws1 = nullptr;
        ceda::WorkingSetMachine* ws2 = nullptr;
        {
            ceda::CSpaceTxn txn;
            ws1 = ceda::OpenWorkingSetMachine("WS1",true);
            ws2 = ceda::OpenWorkingSetMachine("WS2",false);
        }
        
        {
            // Create server and client
            ceda::TcpMsgServer* server;
            ceda::TcpMsgClient* client;
            {
                const ceda::WsipcSessionProtocolId protocolId = { "RODExample1", 1 };
                ceda::WsipcHostInfo localHostInfo;
                auto protocol = ceda::EProtocol::TCP_IPv4;
                int port = 3000;
                bool reuse_addr = false;
                const char* host = "127.0.0.1";
                const char* service = "3000";
                ceda::TcpMsgSessionSettings sessionSettings;

                ceda::CSpaceTxn txn;

                server = CreateTcpMsgServer(
                    protocol, port, reuse_addr, 
                    *CreateWsipcEndPoint(ws1, protocolId, localHostInfo), sessionSettings);

                client = CreateTcpMsgClient(
                    host, service, 
                    *CreateWsipcEndPoint(ws2, protocolId, localHostInfo), sessionSettings);
            }
            
            // Bootstrap RootNode under the UT root
            {
                ceda::CSpaceTxn txn;
                GetUTRoot(ws1).Map[UtEntryName].insert(0,$new RootNode);
            }

            // Generate some operations
            RepNode* c1;
            RepNode* c2;
            {
                ceda::CSpaceTxn txn;

                c1 = $new RepNode;
                c1->Value = 5;
                c2 = $new RepNode;
                c2->Value = 6;
                //ceda::SetReplicateOnDemand(c2);
                c1->Child1 = c2;
                //ceda::SetReplicateOnDemand(c1);

                RootNode* t = ceda::GetUTRootEntry<RootNode>(ws1,UtEntryName);
                t->Value = 10;             // Generate assignment operation
                t->Child1 = c1;

                Tracer() << "ws1 : root oid = " << GetOid(t) << '\n'
                         << "ws1 : c1 oid = " << GetOid(c1) << '\n' 
                         << "ws1 : c2 oid = " << GetOid(c2) << '\n'; 
            }

            // Poll for quiescence
            Tracer() << "Polling for quiescence\n";
            while(1)
            {
                Sleep(10);
                ceda::CSpaceTxn txn;
                if (HistoryBufferSize(ws2) == 2) break;
            }

            Tracer() << "Showing result on ws2\n";
            RootNode* root2;
            {
                ceda::CSpaceTxn txn;
                root2 = ceda::GetUTRootEntry<RootNode>(ws2,UtEntryName);
                AddGcRoot(root2);
            }

            Y y;
            {
                ceda::CSpaceTxn txn;
                Tracer() << "\nActivate agent\n";
                RegisterNonGcObject(&y);
                AddGcRoot(&y);
                y.x = root2;
                //y.A.SetAgentStatus(true);
            }

            {
                ceda::RateTimer timer("Initial access (i.e. forming edges, queuing I/O)", 2);
                ceda::CSpaceTxn txn;
                y.A();
            }
        
            {
                ceda::RateTimer timer("async load", 2);
                while(1)
                {
                    Sleep(1);
                    {
                        ceda::CSpaceTxn txn;
                        y.A();
                    }
                    if (y.allLoaded) break;
                }
            }        
        
            /*
            {
                ceda::CSpaceTxn txn;
                Tracer() << "Deactivate agent\n";
                y.A.SetAgentStatus(false);
            }
            */

            /*
            Manually replicate objects
            */
            /*
            {
                ceda::RodWriter* w = CreateRodWriter(ws1);
                ceda::RodReader* r = CreateRodReader(ws2);

                using namespace ceda;

                xvector<octet_t> buffer;
                {
                    PagedBuffer pb;
                    {
                        OutputArchiveOnPagedBuffer ar(pb);
                        CSpaceTxn txn;
                        SerialiseObject(w,ar,c1);
                        SerialiseObject(w,ar,c2);
                    }
                    pb.WriteTo(buffer);
                }
    
                Tracer() << "buffer size = " << buffer.size() << '\n';

                {
                    TracerX os;
                    HexDump(os, 16, true, buffer.data(), buffer.data_end());
                    os << '\n';
                }

                {
                    CSpaceTxn txn;
                    InputArchive ar(buffer.data());
                    ptr<IPersistable> po;
                    DeserialiseObject(r,ar,po);
                    DeserialiseObject(r,ar,po);
                    cxAssert(ar == buffer.data() + buffer.size());
                }

                Close(w);
                Close(r);
            }
            */
            
            Sleep(1000);
            
            Tracer() << "Showing result on ws2\n";
            {
                ceda::CSpaceTxn txn;
                RootNode* t = ceda::GetUTRootEntry<RootNode>(ws2,UtEntryName);
                ceda::PrintInstance(Tracer(), t);
                y.EvictDgsNodes();
            }
            
            Close(server);
            Tracer() << "closed server\n";
            Close(client);
            Tracer() << "closed client\n";
        }
        
        {
            ceda::CSpaceTxn txn;
            Close(ws1);
            Close(ws2);
            Tracer() << "closed working sets\n";
        }
    }
    Close(pstore);
    Tracer() << "closed pstore\n";
}
