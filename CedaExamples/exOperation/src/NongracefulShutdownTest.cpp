// NongracefulShutdownTest.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2016

@import "Ceda/cxOperation/UTRoot.h"
@import "Ceda/cxOperation/IWorkingSetMachine.h"
@import "Ceda/cxWorkingSetIpc/WorkingSetIPC.h"
@import "Ceda/cxPersistStore/IPersistStore.h"
@import "Ceda/cxObject/IObjectVisitor.h"
@import "Ceda/cxObject/PrintReflectedVariable.h"
#include "Ceda/cxLss/ILogStructuredStore.h"
#include "Ceda/cxUtils/HPTime.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/Environ.h"

namespace ceda
{
    cxLss_API void AbortiveClose(ceda::ILogStructuredStore* lss);
    cxLss_API void StopWriting(ceda::ILogStructuredStore* lss);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// TestDS2

$struct+ TestDS2 <<-os>> isa ceda::IPersistable :
    model
    {
        xvector<int32> L;
    }
{
};

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Represents a store with a single PSpace containing a single working set
*/
const ceda::WsipcSessionProtocolId protocolId = { "TwoWorkingSets", 1 };
static ceda::WsipcHostInfo localHostInfo;
const ceda::xstring UtEntryName("MyApp");

class Store
{
public:
    Store(const char* filename, bool dataSetMaster) :
        dataSetMaster(dataSetMaster),
        pspace(nullptr),
        pstore(nullptr),
        ws(nullptr),
        server(nullptr),
        client(nullptr)
    {
        path = ceda::GetCedaTestPath(filename);
    }

    void PrepareThreadLocalStorage()
    {
        cxAssert(pspace);
        ceda::SetThreadPtr(pspace);
        ceda::SetThreadPtr(ceda::GetCSpace(pspace));
    }

    void CreateOrOpen(bool create)
    {
        Tracer() << "Opening store " << path << '\n';
        pstore = ceda::OpenPersistStore(path.c_str(), create ? ceda::OM_CREATE_ALWAYS : ceda::OM_OPEN_EXISTING);

        ceda::ILogStructuredStore* lss = GetLss(pstore);
        ceda::MSSN mssn = lss->GetMissingShutdownSeqNum();
        Tracer() << "MSSN = " << mssn << '\n';

        // Open or create a PSpace
        pspace = ceda::OpenPSpace(pstore, "MyPSpace");

        PrepareThreadLocalStorage();

        {
            ceda::CSpaceTxn txn;
            ws = ceda::OpenWorkingSetMachine("MyWorkingSet",dataSetMaster);
        }

        if (dataSetMaster)
        {
            ceda::CSpaceTxn txn;
            auto protocol = ceda::EProtocol::TCP_IPv4;
            int port = 3000;
            bool reuse_addr = false;
            ceda::WsipcHostInfo localHostInfo;
            ceda::TcpMsgSessionSettings sessionSettings;
            server = CreateTcpMsgServer(
                protocol, port, reuse_addr, 
                *CreateWsipcEndPoint(ws, protocolId, localHostInfo), sessionSettings);
        }
        else
        {
            ceda::CSpaceTxn txn;
            const char* host = "127.0.0.1";
            const char* service = "3000";
            ceda::WsipcHostInfo localHostInfo;
            ceda::TcpMsgSessionSettings sessionSettings;
            client = CreateTcpMsgClient(
                host, service, 
                *CreateWsipcEndPoint(ws, protocolId, localHostInfo), sessionSettings);
        }
    }

    void Reopen(bool abortive = false)
    {
        Close(abortive);
        CreateOrOpen(false);
    }

    void PollForQuiescence(int numOps)
    {
        // Poll for quiescence
        Tracer() << "Polling for quiescence\n";
        while(1)
        {
            PrepareThreadLocalStorage();
            Sleep(10);
            ceda::CSpaceTxn txn;
            if (HistoryBufferSize(ws) == numOps) break;
        }
    }

    void ShowResult()
    {
        Tracer() << "Showing result on " << path << '\n';
        PrepareThreadLocalStorage();
        ceda::CSpaceTxn txn;
        TestDS2* t = ceda::GetUTRootEntry<TestDS2>(ws,UtEntryName);
        ceda::PrintInstance(Tracer(), t);
    }

    void Close(bool abortive = false)
    {
        if (abortive)
        {
            ceda::ILogStructuredStore* lss = GetLss(pstore);
            //ceda::AbortiveClose(lss);
            ceda::StopWriting(lss);
        }
        
        if (pspace)
        {
            PrepareThreadLocalStorage();
        }

        if (server)
        {
            ceda::Close(server);
            server = nullptr;
            Tracer() << "closed server\n";
        }
        
        if (client)
        {
            ceda::Close(client);
            client = nullptr;
            Tracer() << "closed client\n";
        }

        if (ws)
        {
            ceda::CSpaceTxn txn;
            ceda::Close(ws);
            ws = nullptr;
            Tracer() << "closed working set\n";
        }
        
        if (pspace)
        {
            ceda::Close(pspace);
            pspace = nullptr;
            Tracer() << "closed pspace\n";
        }
        
        if (pstore)
        {
            ceda::Close(pstore);
            pstore = nullptr;
            Tracer() << "closed store " << path << '\n';
        }
    }

    ceda::xstring path;
    bool dataSetMaster;
    ceda::PSpace* pspace;
    ceda::PersistStore* pstore;
    ceda::WorkingSetMachine* ws;
    ceda::TcpMsgServer* server;
    ceda::TcpMsgClient* client;
};

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
*/
void NongracefulShutdownTest()
{
    Tracer() << "NongracefulShutdownTest example\n";
    ceda::TraceIndenter indent;

    const ceda::xstring UtEntryName("MyApp");

    Store serverStore("server.ceda",true);
    Store clientStore("client.ceda",false);

    serverStore.CreateOrOpen(true);
    clientStore.CreateOrOpen(true);

    // Bootstrap TestDS under the UT root
    {
        serverStore.PrepareThreadLocalStorage();
        ceda::CSpaceTxn txn;
        GetUTRoot(serverStore.ws).Map[UtEntryName].insert(0,$new TestDS2);
    }
    serverStore.Reopen();

    // Generate some operations
    Tracer() << "\nGenerating 5 operations on server\n";
    for (int i=0 ; i < 5 ; ++i)
    {
        serverStore.PrepareThreadLocalStorage();
        ceda::CSpaceTxn txn;
        TestDS2* t = ceda::GetUTRootEntry<TestDS2>(serverStore.ws,UtEntryName);
        t->L.push_back(i);
    }
    serverStore.ShowResult();

    Tracer() << "\nWaiting for 5 operations to be applied to the client\n";
    clientStore.PollForQuiescence(6);
    clientStore.ShowResult();

    Tracer() << "\nAbortive close of the server so the 5 operations it created are lost\n";
    serverStore.Reopen(true);
    serverStore.ShowResult();

    Tracer() << "\nSleep one second so server receives the 5 operations from client that the server had originally generated\n";
    Sleep(1000);
    serverStore.ShowResult();

    Tracer() << "\nGenerate 10 operations on server\n";
    for (int i=5 ; i < 15 ; ++i)
    {
        serverStore.PrepareThreadLocalStorage();
        ceda::CSpaceTxn txn;
        TestDS2* t = ceda::GetUTRootEntry<TestDS2>(serverStore.ws,UtEntryName);
        t->L.push_back(i);
    }
    serverStore.ShowResult();

    Tracer() << "\nWait for operations to appear on client\n";
    clientStore.PollForQuiescence(16);
    clientStore.Reopen();
    clientStore.ShowResult();

    Tracer() << "\nTest completed\n";
    serverStore.Close();
    clientStore.Close();
}