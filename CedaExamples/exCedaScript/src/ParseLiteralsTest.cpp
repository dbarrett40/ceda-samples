// ParseLiteralsTest.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2012

@import "Ceda/cxCedaScript/cxCedaScript.h"
@import "Ceda/cxPersistStore/pref.h"
@import "Ceda/cxPersistStore/IPrefVisitor.h"
@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/IObjectVisitor.h"
@import "Ceda/cxObject/WCSpace.h"
#include "Ceda/cxUtils/Tracer.h"

namespace ParseLiteralsTest
{
    $model+ Mbool
    {
        bool b;
    };
    $struct+ Dbool <<-os>> isa ceda::IPersistable : model Mbool {};

    $model+ Mint
    {
        ceda::int8 i8;
        ceda::int16 i16;
        ceda::int32 i32;
        ceda::int64 i64;
    };
    $struct+ Dint <<-os>> isa ceda::IPersistable : model Mint {};

    $model+ Muint
    {
        ceda::uint8 ui8;
        ceda::uint16 ui16;
        ceda::uint32 ui32;
        ceda::uint64 ui64;
    };
    $struct+ Duint <<-os>> isa ceda::IPersistable : model Muint {};

    $model+ Mfloat
    {
        ceda::float32 f32;
        ceda::float64 f64;
    };
    $struct+ Dfloat <<-os>> isa ceda::IPersistable : model Mfloat {};

    $model+ Mchar
    {
        ceda::char8 c8;
        ceda::char16 c16;
    };
    $struct+ Dchar <<-os>> isa ceda::IPersistable : model Mchar {};

    $model+ Mstring
    {
        ceda::string8 s8;
        ceda::string16 s16;
    };
    $struct+ Dstring <<-os>> isa ceda::IPersistable : model Mstring {};

    template <typename D, typename M>
    void Test(const ceda::xstring& s, const M& expected)
    {
        Tracer() << s << '\n';
        ceda::xstring errorMsg;
        ceda::ptr<ceda::IObject> obj = ParseDataSourceFromString(s, errorMsg);
        if (!errorMsg.empty()) 
        {
            Tracer() << errorMsg << '\n';
            cxAssert(0);
        }
        cxAssert(obj);
        D* result = ceda::tryqccast<D>(obj);
        cxAssert(result);
        if (result->_model_ != expected)
        {
            Tracer() << result->_model_ << '\n';
            cxAssert(0);
        }
    }

    #ifdef _MSC_VER
        //warning C4146: unary minus operator applied to unsigned type, result still unsigned
        #pragma warning(disable: 4146)
    #endif

    void Run()
    {
        Tracer() << "ParseLiteralsTest\n";
        ceda::TraceIndenter indent;

        ceda::CSpaceCreator cspace;
        ceda::CSpaceLock lock;

        Test<Dbool>(@str(ParseLiteralsTest.Dbool { b = false }),Mbool(false));
        Test<Dbool>(@str(ParseLiteralsTest.Dbool { b = 1<0 }),Mbool(false));
        Test<Dbool>(@str(ParseLiteralsTest.Dbool { b = true }),Mbool(true));
        Test<Dbool>(@str(ParseLiteralsTest.Dbool { b = 1000*1000*1000*1000 == 1000000000000 }),Mbool(true));

        Test<Dint>(@str(ParseLiteralsTest.Dint { i8 = 0 i16 = 0 i32 = 0 i64 = 0 }),Mint(0,0,0,0));
        Test<Dint>(@str(ParseLiteralsTest.Dint { i8 = 0x34 i16 = 0x16ef i32 = 0x123467ff i64 = 0x12345678abcdef56 }),Mint(0x34, 0x16ef, 0x123467ff, 0x12345678abcdef56));
        Test<Dint>(@str(ParseLiteralsTest.Dint { i8 = 2^^7-1 i16 = 2^^15-1 i32 = 2^^31-1 i64 = 2^^63-1 }),Mint(0x7F,0x7FFF,0x7FFFFFFF,0x7FFFFFFFFFFFFFFF));
        Test<Dint>(@str(ParseLiteralsTest.Dint { i8 = -2^^7 i16 = -2^^15 i32 = -2^^31 i64 = -2^^63 }),Mint(-0x80,-0x8000,-0x80000000,-0x8000000000000000));
        Test<Dint>(@str(ParseLiteralsTest.Dint { i8 = -0x80 i16 = -0x8000 i32 = -0x80000000 i64 = -0x8000000000000000 }),Mint(-0x80,-0x8000,-0x80000000,-0x8000000000000000));

        Test<Duint>(@str(ParseLiteralsTest.Duint { ui8 = 0 ui16 = 0 ui32 = 0 ui64 = 0 }),Muint(0,0,0,0));
        //Test<Duint>(@str(ParseLiteralsTest.Duint { ui8 = 2^^8-1 ui16 = 2^^16-1 ui32 = 2^^32-1 ui64 = 2^^64-1 }),Muint(0xFF,0xFFFF,0xFFFFFFFF,0xFFFFFFFFFFFFFFFF));
        Test<Duint>(@str(ParseLiteralsTest.Duint { ui8 = 2^^8-1 ui16 = 2^^16-1 ui32 = 2^^32-1 ui64 = 0xFFFFFFFFFFFFFFFF }),Muint(0xFF,0xFFFF,0xFFFFFFFF,0xFFFFFFFFFFFFFFFF));

        //Test<Duint>(@str(ParseLiteralsTest.Duint { ui8 = -1 ui16 = 2^^16-1 ui32 = 2^^32-1 ui64 = 0xFFFFFFFFFFFFFFFF }),Muint(0xFF,0xFFFF,0xFFFFFFFF,0xFFFFFFFFFFFFFFFF));

        Test<Dfloat>(@str(ParseLiteralsTest.Dfloat { f32 = 0 f64 = 0 }),Mfloat(0,0));
        Test<Dfloat>(@str(ParseLiteralsTest.Dfloat { f32 = 1.0 f64 = 1.0 }),Mfloat(1,1));
        
        Test<Dchar>(@str(ParseLiteralsTest.Dchar { c8 = 0 c16 = 0 }),Mchar(0,0));
        Test<Dchar>(@str(ParseLiteralsTest.Dchar { c8 = 'a' c16 = 'b' }),Mchar('a','b'));

        #ifdef _WIN32
            Test<Dstring>(@str(ParseLiteralsTest.Dstring { s8 = "x" s16 = "y" }),Mstring("x",(const ceda::char16*)L"y"));
        #endif
    }
}
