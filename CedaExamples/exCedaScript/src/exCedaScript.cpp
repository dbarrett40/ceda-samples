// exCedaScript.cpp

@import "Ceda/cxObject/TargetRegistry.h"
#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"

mRegisterLeafTarget(
    {
        // version         year mon day
        { "Release 0.0",   {2008,8,24} },      // Release #0
    })

namespace ParseDataSource { void Run(); }
namespace ParseLiteralsTest { void Run(); }
namespace ParseRunnableEx { void Run(); }

void exCedaScript_RunAllTests()
{
    Tracer() << "----------------------------------- exCedaScript examples  -----------------------------------\n";
    ceda::HPTimer timer;
    ParseDataSource::Run();
    ParseLiteralsTest::Run();
    ParseRunnableEx::Run();
    timer.Done() << "exCedaScript examples completed successfully";
}

CEDA_MAIN_FUNCTION(argc,argv)
{
    CEDA_REGISTER_PROJECT
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exCedaScript_RunAllTests();
    ceda::DestroyTheTraceFile();
    return 0;
}

