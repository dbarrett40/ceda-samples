// exThread.cpp

#include "Ceda/cxUtils/xstring.h"
#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/HPTime.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// RunAllExamples()

void CalcTanExample();

void exThread_RunAllExamples()
{
    Tracer() << "----------------------------------- exThread examples -----------------------------------\n";
    ceda::HPTimer timer;
    CalcTanExample();
    timer.Done() << "exThread examples completed successfully";
}

CEDA_MAIN_FUNCTION(argc,argv)
{
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exThread_RunAllExamples();
    ceda::DestroyTheTraceFile();
    return 0;
}

