// CalcTan.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2011

#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxMessage/IThreadPool.h"
#include <math.h>

 ///////////////////////////////////////////////////////////////////////////////////////////////////

struct CalcTan : public ceda::IThreadPoolTask
{ 
    CalcTan(double t) : theta(t) {} 
    virtual void ExecuteTask() { result = tan(theta); } 
    double theta; 
    double result; 
}; 

double SyncCalcTan(double x, double y)
{
    return tan(x) + tan(y);
}

// Calculate tan(x) + tan(y) using the given thread pool 
double AsyncCalcTan(double x, double y, ceda::IThreadPool* tp)
{
    ceda::ITaskExecuter* e = tp->CreateTaskExecuter();
    CalcTan task1(x); e->PostTask(&task1); 
    CalcTan task2(y); e->PostTask(&task2); 
    e->Close(); 
    return task1.result + task2.result; 
}

void CalcTanExample()
{
    Tracer() << "Calculate tan(x) + tan(y) asynchronously using a thread pool\n";
    ceda::TraceIndenter indent;

    // Create thread pool
    const ceda::ssize_t numThreads = 2;
    ceda::IThreadPool* tp = ceda::CreateThreadPool(numThreads);

    double x = 1.234243;
    double y = 3.454376943;

    // Calculate tan(x) + tan(y) synchronously
    double t1 = SyncCalcTan(x,y);
    
    // Calculate tan(x) + tan(y) asynchronously
    double t2 = AsyncCalcTan(x,y,tp);
    
    Tracer() << "Async " << t1 << " sync " << t2 << '\n';
    cxAssert(t1 == t2);

    tp->Close();
}


