// Adt.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

#include "Ceda/cxUtils/TracerUtils.h"
@import "Ceda/cxPython/cxPython.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
/*
ADT
---

All methods of an ADT are reflected, and can be called as though they were member functions on the
abstract type.

An ADT brings the responsibility of destroying objects onto the Python programmer!
*/

namespace Adt1
{
    ////////////// X.h ////////////////
    $adt+ X
    {
        ceda::int32 Getx() const;
        void Setx(ceda::int32 x);
        void Destroy();
    };

	X* CreateX();

    ////////////// X.cpp ////////////////
    
    // Implementation of the ADT
    $adt+ implement X
    {
        X() : m_x(0) {}
        ceda::int32 Getx() const { return m_x; }
        void Setx(ceda::int32 x) { m_x = x; }
        void Destroy() { delete this; }
        ceda::int32 m_x;
    };
    
	$function+ X* CreateX()
	{
	    return new X();
	}

    ////////////// Client ////////////////
	
	void Run()
	{
		ceda::TraceGroup g("Adt example 1");
		
        PyRun_SimpleString(
            @strx
            (
                Adt1 = rootnamespace.Adt1
                x = Adt1.CreateX()
                print 'dir(x) = ' + `dir(x)`
                x.Setx(10)
                print 'x.Getx() = ' + `x.Getx()`
                
                # Typically an ADT will have a Close() or Destroy() method, or something similar
                # This must be called to avoid a memory leak
                x.Destroy()
            ));
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Adt
{
    void Run()
    {
        Adt1::Run();
    }
}

