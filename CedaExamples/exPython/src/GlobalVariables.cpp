// GlobalVariables.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

#include "Ceda/cxUtils/xstring.h"
#include "Ceda/cxUtils/TracerUtils.h"
@import "Ceda/cxPython/cxPython.h"

// These example shouldn't be taken as promotion of the use of global variables.  Generally 
// speaking, global variables are something to avoid!


///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Global variables
----------------

Python has read and write access to reflected global variables, accessed in the namespace 
hierarchy under the root ceda namespace (rootnamespace).
*/

namespace GlobalVariables1
{
    $struct+ Point
    {
        Point() : x(0), y(0) {}
        
        $int32 x;
        $int32 y;    
    };

    // Some reflected global variables
    $var+ int32 x = 0;
    $var+ string8 s = "hello";
    $var+ Point p;
	
	void Run()
	{
		ceda::TraceGroup g("Global variables example 1");
		
        PyRun_SimpleString(
            @strx
            (
                # print variable with the given name
                @def pv(name) = 
                {
                    print @str(name = ) + `rootnamespace.GlobalVariables1.name`
                }
                
                # set variable
                @def sv(name,val) = 
                {
                    rootnamespace.GlobalVariables1.name = val
                }
                
                pv(x)
                sv(x,10)
                pv(x)
                
                pv(s)
                sv(s,'world')
                pv(s)
                
                pv(p)
                sv(p.x,100)
                sv(p.y,200)
                pv(p)
            ));
	}	
}


///////////////////////////////////////////////////////////////////////////////////////////////////

namespace GlobalVariables
{
    void Run()
    {
        GlobalVariables1::Run();
    }
}

