// InterfaceMethods.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

#include "Ceda/cxUtils/TracerUtils.h"
@import "Ceda/cxPython/cxPython.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Interface methods
-----------------

Interface methods may be directly called from Python.
*/

namespace InterfaceMethods1
{
    $interface+ Ix : ceda::IObject
    {
        ceda::int32 Getx() const;
        void Setx(ceda::int32 x);
    };

    $struct+ X isa Ix
    {
        X() : m_x(0) {}
        ceda::int32 Getx() const { return m_x; }
        void Setx(ceda::int32 x) { m_x = x; }
        ceda::int32 m_x;
    };

    $function+ X CreateX() { return X(); }
	
	void Run()
	{
		ceda::TraceGroup g("Interface methods example 1");
		
        PyRun_SimpleString(
            @strx
            (
                InterfaceMethods1 = rootnamespace.InterfaceMethods1
                x = InterfaceMethods1.CreateX()
                print 'dir(x.Ix) = ' + `dir(x.Ix)`
                x.Ix.Setx(10)
                print 'x.Ix.Getx() = ' + `x.Ix.Getx()`
            ));
	}	
}


///////////////////////////////////////////////////////////////////////////////////////////////////

namespace InterfaceMethods
{
    void Run()
    {
        InterfaceMethods1::Run();
    }
}

