// Exceptions.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "Ceda/cxPython/cxPython.h"
#include "Ceda/cxUtils/TracerUtils.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Exceptions
----------

It is permissible for a reflected C++ function called from Python to throw an exception, as long
as the exception inherits from IException.

In the example below, Python calls the function FlyToMoon() which throws an exception of type
Apollo13Failure.  As required, this inherits from IException.  In Python, this causes a 
RuntimeError exception to be thrown, with a string value equal to the string written by the 
virtual Write() method of the C++ IException.
*/

namespace Exceptions1
{
	struct Apollo13Failure : public ceda::IException
	{
	    virtual void Write(ceda::xostream& os) const
	    {
	        os << "Houston, we have a problem";
	    }
	};
	
	$function+ void FlyToMoon()
	{
	    throw Apollo13Failure();
	}
	
	void Run()
	{
		ceda::TraceGroup g("Exceptions example 1");
		
        PyRun_SimpleString(
            @strx
            (
                Exceptions1 = rootnamespace.Exceptions1
                try:
                    Exceptions1.FlyToMoon()
                except RuntimeError, e:
                    print e
            ));
	}	
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Reflected Exceptions
--------------------

This example shows how a reflected exception class can be written that allows for access to the 
reflected member variables, methods and supported interfaces.

It is necessary for the C++ exception class to inherit from IReflectedException instead of 
IException.  This requires implementation of the virtual method GetReflectedClass(), which will
generally be implemented as below.
*/
namespace Exceptions2
{
	$struct+ TooMuchCO2 <<os>> : public ceda::IReflectedException 
	{
	    TooMuchCO2(int _v) : v(_v) {}
	    
	    virtual void Write(ceda::xostream& os) const
	    {
	        os << "Too much C02 : v = " << v;
	    }
	    virtual const ceda::ReflectedClass& GetReflectedClass() const { return ceda::GetReflectedClass<TooMuchCO2>(); }
	    
	    $ceda::int32 v;
	};
	
	$function+ void ReturnToEarth()
	{
	    throw TooMuchCO2(80);
	}
	
	void Run()
	{
		ceda::TraceGroup g("Exceptions example 2");
		
        PyRun_SimpleString(
            @strx
            (
                Exceptions2 = rootnamespace.Exceptions2

                try:
                    Exceptions2.ReturnToEarth()

                except RuntimeError, e:
                    print 'v = ' + `e.args[0].v`
                    #print `e.args[0]`
                    print e
            ));
	}	
}
 
///////////////////////////////////////////////////////////////////////////////////////////////////
namespace Exceptions
{
    void Run()
    {
        Exceptions1::Run();
        Exceptions2::Run();
    }
}

