// ExInt64.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

#include "Ceda/cxUtils/TracerUtils.h"
@import "Ceda/cxPython/cxPython.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
//
// Creation, printing and operations on signed and unsigned 64 bit values
//
// See Comparisons.cpp for an example of comparisons on 64 bit values.

namespace ExInt64_1
{
	void Run()
	{
		ceda::TraceGroup g("Int64 example 1");

        PyRun_SimpleString(
            @strx
            (
                Int64 = rootnamespace.Int64
                UInt64 = rootnamespace.UInt64
                
                print 'dir(Int64) = ' + `dir(Int64)`
                print 'dir(UInt64) = ' + `dir(UInt64)`
                
                @def pri(m,v) = print m + `v`
                
                # -----------------------------------------------------------------------
                # Int64.Make(low,high) builds a signed 64 bit value from the given low and
                # high 32 bit values

                x = Int64.Make(1,0)
                pri('1 = ',x)
                
                x = Int64.Make(0,1)
                pri('2^32 = ',x)

                x = Int64.Make(-1,-1)
                pri('-1 = ',x)
                
                # -----------------------------------------------------------------------
                # UInt64.Make(low,high) builds an unsigned 64 bit value
                
                x = UInt64.Make(-1,-1)
                pri('2^64 - 1 = ',x)
                
                # -----------------------------------------------------------------------
                # binary operators +,-,*,/
                
                pri('10 + 5 = ', Int64.Add(Int64.Make(10,0), Int64.Make(5,0)))
                pri('10 - 5 = ', Int64.Sub(Int64.Make(10,0), Int64.Make(5,0)))
                pri('10 * 5 = ', Int64.Mult(Int64.Make(10,0), Int64.Make(5,0)))
                pri('10 / 5 = ', Int64.Div(Int64.Make(10,0), Int64.Make(5,0)))
                
                # -----------------------------------------------------------------------
                # Unary -
                
                pri('-10 = ', Int64.Neg(Int64.Make(10,0)))
            ));
	}	
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Marshalling of int64 values
---------------------------

*/

namespace ExInt64_2
{
    $struct+ X
    {
        X() : i64(-5000000000), ui64(-5000000000) {}
        
        $int64 i64;
        $uint64 ui64;
    };
    
    $function+ int64 add(int64 x, int64 y)
    {
        return x+y;        
    }

	$function+ X CreateX()
	{
	    return X();
	}
	
	void Run()
	{
		ceda::TraceGroup g("Int64 example 2");

        PyRun_SimpleString(
            @strx
            (
                @def mPrint(v) = print @str(v = ) + `v`

                Int64 = rootnamespace.Int64
                UInt64 = rootnamespace.UInt64
                
                ns = rootnamespace.ExInt64_2
                x = ns.CreateX()
                mPrint(x.i64)
                mPrint(x.ui64)
                
                x.i64 = 10
                mPrint(x.i64)

                x.i64 = Int64.Make(100,1)
                mPrint(x.i64)
                
                mPrint(ns.add(x.i64, x.ui64))
            ));
	}	
}



///////////////////////////////////////////////////////////////////////////////////////////////////

namespace ExInt64
{
    void Run()
    {
        ExInt64_1::Run();
        ExInt64_2::Run();
    }
}

