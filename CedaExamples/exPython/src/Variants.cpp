// Variants.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2017

@import "Ceda/cxPython/cxPython.h"
@import "CedaExamples/lxOperation/Shapes.h"
@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/IObjectVisitor.h"
@import "Ceda/cxObject/WCSpace.h"
#include "Ceda/cxUtils/TracerUtils.h"

/*
Output:

Variants example 1
{
    dir(Variants1::V1) = ['GetField', 'GetName', 'GetNumFields', 'GetSize']
    Variants1::V1.GetSize() = 8
    Variants1::V1.GetName() = Variants1::V1
    Variants1::V1.GetNumFields() = 2
    Variants1::V1.GetField(0) = ReflectedField(void:4)
    dir(Variants1::V1.GetField(0)) = ['GetName', 'GetOffset']
    Variants1::V1.GetField(0).GetName() = void
    Variants1::V1.GetField(0).GetOffset() = 4
    Variants1::V1.GetField(1) = ReflectedField(ceda::int32:4)
    dir(Variants1::V1.GetField(1)) = ['GetName', 'GetOffset']
    Variants1::V1.GetField(1).GetName() = ceda::int32
    Variants1::V1.GetField(1).GetOffset() = 4

    dir(Variants1::V2) = ['GetField', 'GetName', 'GetNumFields', 'GetSize']
    Variants1::V2.GetSize() = 28
    Variants1::V2.GetName() = Variants1::V2
    Variants1::V2.GetNumFields() = 4
    Variants1::V2.GetField(0) = ReflectedField(ceda::int32:4)
    dir(Variants1::V2.GetField(0)) = ['GetName', 'GetOffset']
    Variants1::V2.GetField(0).GetName() = ceda::int32
    Variants1::V2.GetField(0).GetOffset() = 4
    Variants1::V2.GetField(1) = ReflectedField(f:4)
    dir(Variants1::V2.GetField(1)) = ['GetName', 'GetOffset']
    Variants1::V2.GetField(1).GetName() = f
    Variants1::V2.GetField(1).GetOffset() = 4
    Variants1::V2.GetField(2) = ReflectedField(ceda::string8:4)
    dir(Variants1::V2.GetField(2)) = ['GetName', 'GetOffset']
    Variants1::V2.GetField(2).GetName() = ceda::string8
    Variants1::V2.GetField(2).GetOffset() = 4
    Variants1::V2.GetField(3) = ReflectedField(ceda::xvector<ceda::int32>:4)
    dir(Variants1::V2.GetField(3)) = ['GetName', 'GetOffset']
    Variants1::V2.GetField(3).GetName() = ceda::xvector<ceda::int32>
    Variants1::V2.GetField(3).GetOffset() = 4

    dir(Variants1::V3) = ['GetField', 'GetName', 'GetNumFields', 'GetSize']
    Variants1::V3.GetSize() = 28
    Variants1::V3.GetName() = Variants1::V3
    Variants1::V3.GetNumFields() = 4
    Variants1::V3.GetField(0) = ReflectedField(i:4)
    dir(Variants1::V3.GetField(0)) = ['GetName', 'GetOffset']
    Variants1::V3.GetField(0).GetName() = i
    Variants1::V3.GetField(0).GetOffset() = 4
    Variants1::V3.GetField(1) = ReflectedField(f:4)
    dir(Variants1::V3.GetField(1)) = ['GetName', 'GetOffset']
    Variants1::V3.GetField(1).GetName() = f
    Variants1::V3.GetField(1).GetOffset() = 4
    Variants1::V3.GetField(2) = ReflectedField(s:4)
    dir(Variants1::V3.GetField(2)) = ['GetName', 'GetOffset']
    Variants1::V3.GetField(2).GetName() = s
    Variants1::V3.GetField(2).GetOffset() = 4
    Variants1::V3.GetField(3) = ReflectedField(v:4)
    dir(Variants1::V3.GetField(3)) = ['GetName', 'GetOffset']
    Variants1::V3.GetField(3).GetName() = v
    Variants1::V3.GetField(3).GetOffset() = 4

    x = void
    dir(x) = ['ceda::int32', 'void']
    x.tag = 0
    x.tagname = 'void'
    getattr(x,'tag') = 0
    void
        getattr(v,void) = None
      SET v.void = None
        getattr(v,void) = None
        v.tag = 0
        v.tagname = 'void'
    ceda::int32
        getattr(v,ceda::int32) = None
      SET v.ceda::int32 = 20
        getattr(v,ceda::int32) = 20
        v.tag = 1
        v.tagname = 'ceda::int32'

    x = 10
    dir(x) = ['ceda::int32', 'ceda::string8', 'ceda::xvector<ceda::int32>', 'f']
    x.tag = 0
    x.tagname = 'ceda::int32'
    getattr(x,'tag') = 0
    ceda::int32
        getattr(v,ceda::int32) = 10
      SET v.ceda::int32 = 20
        getattr(v,ceda::int32) = 20
        v.tag = 0
        v.tagname = 'ceda::int32'
    ceda::string8
        getattr(v,ceda::string8) = None
      SET v.ceda::string8 = 'ciao'
        getattr(v,ceda::string8) = 'ciao'
        v.tag = 2
        v.tagname = 'ceda::string8'
    ceda::xvector<ceda::int32>
        getattr(v,ceda::xvector<ceda::int32>) = None
      SET v.ceda::xvector<ceda::int32> = [2, 3, 5, 7, 11, 13]
        getattr(v,ceda::xvector<ceda::int32>) =  = [2,3,5,7,11,13]
        v.tag = 3
        v.tagname = 'ceda::xvector<ceda::int32>'
    f
        getattr(v,f) = None
      SET v.f = 2.71828
        getattr(v,f) = 2.71828
        v.tag = 1
        v.tagname = 'f'
    f
        x.f = 2.71828
      SET x.f = 0.86602540379
        x.f = 0.86602540379
        x.tag = 1
        x.tagname = 'f'

    x = s(hello world)
    dir(x) = ['f', 'i', 's', 'v']
    x.tag = 2
    x.tagname = 's'
    getattr(x,'tag') = 2
    i
        getattr(v,i) = None
      SET v.i = 30
        getattr(v,i) = 30
        v.tag = 0
        v.tagname = 'i'
    f
        getattr(v,f) = None
      SET v.f = 2.71828
        getattr(v,f) = 2.71828
        v.tag = 1
        v.tagname = 'f'
    s
        getattr(v,s) = None
      SET v.s = 'greetings'
        getattr(v,s) = 'greetings'
        v.tag = 2
        v.tagname = 's'
    v
        getattr(v,v) = None
      SET v.v = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]
        getattr(v,v) =  = [1,2,3,4,5,1,2,3,4,5]
        v.tag = 3
        v.tagname = 'v'
    i
        x.i = None
      SET x.i = 12345678
        x.i = 12345678
        x.tag = 0
        x.tagname = 'i'
    f
        x.f = None
      SET x.f = 0.86602540379
        x.f = 0.86602540379
        x.tag = 1
        x.tagname = 'f'
    s
        x.s = None
      SET x.s = hey
        x.s = 'hey'
        x.tag = 2
        x.tagname = 's'
    v
        x.v = None
      SET x.v = [10,-1,7]
        x.v =  = [10,-1,7]
        x.tag = 3
        x.tagname = 'v'
}
*/
namespace Variants1
{
    $variant+ V1
    {
        void;
        int32;
    };

    $variant+ V2
    {
        default 10;
        int32;
        float64 f;
        string8;
        xvector<int32>;
    };

    $variant+ V3
    {
        default ceda::string8("hello world");
        int32 i;
        float64 f;
        string8 s;
        xvector<int32> v;
    };

    void Run()
    {
		ceda::TraceGroup g("Variants example 1");

        PyRun_SimpleString(
            @strx
            (
                V1 = rootnamespace.Variants1.V1
                V2 = rootnamespace.Variants1.V2
                V3 = rootnamespace.Variants1.V3

                for V in [V1,V2,V3]:
                    #print `V` + '.__members__ = ' + `V.__members__`
                    print 'dir(' + `V` + ') = ' + `dir(V)`
                    print `V` + '.GetSize() = ' + `V.GetSize()`
                    print `V` + '.GetName() = ' + `V.GetName()`
                    print `V` + '.GetNumFields() = ' + `V.GetNumFields()`
                    for i in range(int(str(V.GetNumFields()))):
                        print `V` + '.GetField(' + `i` + ') = ' + `V.GetField(i)`
                        print 'dir(' + `V` + '.GetField(' + `i` + ')) = ' + `dir(V.GetField(i))`
                        print `V` + '.GetField(' + `i` + ').GetName() = ' + `V.GetField(i).GetName()`
                        print `V` + '.GetField(' + `i` + ').GetOffset() = ' + `V.GetField(i).GetOffset()`
                    print

                def setAttributeByStringName(v,name,value):
                    if hasattr(v,name):
                        print name
                        print '    getattr(v,' + name + ') = ' + `getattr(v,name)`
                        print '  SET v.' + name + ' = ' + `value`
                        setattr(v, name, value)
                        print '    getattr(v,' + name + ') = ' + `getattr(v,name)`
                        print '    v.tag = ' + `v.tag`
                        print '    v.tagname = ' + `v.tagname`
                
                for x in [V1(), V2(), V3()]:
                    print 'x = ' + `x`
                    print 'dir(x) = ' + `dir(x)`

                    print 'x.tag = ' + `x.tag`
                    print 'x.tagname = ' + `x.tagname`
                    print "getattr(x,'tag') = " + `getattr(x,'tag')`

                    setAttributeByStringName(x,'void',None)
                    setAttributeByStringName(x,'ceda::int32',20)
                    setAttributeByStringName(x,'ceda::float64',3.14159)
                    setAttributeByStringName(x,'ceda::string8','ciao')
                    setAttributeByStringName(x,'ceda::xvector<ceda::int32>',[2,3,5,7,11,13])
                    setAttributeByStringName(x,'i',30)
                    setAttributeByStringName(x,'f',2.71828)
                    setAttributeByStringName(x,'s','greetings')
                    setAttributeByStringName(x,'v',[1,2,3,4,5]*2)

                    if hasattr(x,'i'):
                        print 'i'
                        print '    x.i = ' + `x.i`
                        print '  SET x.i = 12345678'
                        x.i = 12345678
                        print '    x.i = ' + `x.i`
                        print '    x.tag = ' + `x.tag`
                        print '    x.tagname = ' + `x.tagname`

                    if hasattr(x,'f'):
                        print 'f'
                        print '    x.f = ' + `x.f`
                        print '  SET x.f = 0.86602540379'
                        x.f = 0.86602540379
                        print '    x.f = ' + `x.f`
                        print '    x.tag = ' + `x.tag`
                        print '    x.tagname = ' + `x.tagname`

                    if hasattr(x,'s'):
                        print 's'
                        print '    x.s = ' + `x.s`
                        print '  SET x.s = hey'
                        x.s = 'hey'
                        print '    x.s = ' + `x.s`
                        print '    x.tag = ' + `x.tag`
                        print '    x.tagname = ' + `x.tagname`

                    if hasattr(x,'v'):
                        print 'v'
                        print '    x.v = ' + `x.v`
                        print '  SET x.v = [10,-1,7]'
                        x.v = [10,-1,7]
                        print '    x.v = ' + `x.v`
                        print '    x.tag = ' + `x.tag`
                        print '    x.tagname = ' + `x.tagname`

                    print
            ));
    }
}

namespace Variants
{
    void Run()
    {
        Variants1::Run();
    }
}

