// Maps.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2017

@import "Ceda/cxPython/cxPython.h"
@import "Ceda/cxPersistStore/xmap.h"
#include "Ceda/cxUtils/TracerUtils.h"

/*
Output:
    Maps example 1
    {
        x1 = Maps1::X1
        {
            Map[map<int32,int32>] error: can't print non-model map field
        }

        x2 = X2({},{})
        len(x2.Map) = 0
        x2.Map[1] = 0
        len(x2.Map) = 1
        x2.Map[2] = 0
        len(x2.Map) = 2
          len(x2.Map) = 2
          x2.Map.has_key(1) = True
          x2.Map.has_key(2) = True
          1 in x2.Map = True
          2 in x2.Map = True
          x2.Map[1] = 10
          x2.Map[2] = 20
          value: 10
          value: 20
          item: (1, 10)
          item: (2, 20)
          key: 1  value: 10
          key: 2  value: 20
          x2.Map.iterkeys() = .1 =   10
        .2 = 20
          x2.Map.itervalues() = .1 =   10
        .2 = 20
          x2.Map.iteritems() = .1 =   10
        .2 = 20
        1 is in x2.Map
        2 is in x2.Map
          len(x2.Map) = 3
          x2.Map.has_key(1) = True
          x2.Map.has_key(2) = True
          x2.Map.has_key(3) = True
          1 in x2.Map = True
          2 in x2.Map = True
          3 in x2.Map = True
          x2.Map[1] = 10
          x2.Map[2] = 20
          x2.Map[3] = 30
          value: 10
          value: 20
          value: 30
          item: (1, 10)
          item: (2, 20)
          item: (3, 30)
          key: 1  value: 10
          key: 2  value: 20
          key: 3  value: 30
          x2.Map.iterkeys() = .1 =   10
        .2 = 20
        .3 = 30
          x2.Map.itervalues() = .1 =   10
        .2 = 20
        .3 = 30
          x2.Map.iteritems() = .1 =   10
        .2 = 20
        .3 = 30
        c = 100000
          len(x2.StringMap) = 4
          x2.StringMap.has_key('bee') = True
          x2.StringMap.has_key('bird') = True
          x2.StringMap.has_key('elephant') = True
          x2.StringMap.has_key('fish') = True
          'bee' in x2.StringMap = True
          'bird' in x2.StringMap = True
          'elephant' in x2.StringMap = True
          'fish' in x2.StringMap = True
          x2.StringMap['bee'] = 'hive'
          x2.StringMap['bird'] = 'flock'
          x2.StringMap['elephant'] = 'herd'
          x2.StringMap['fish'] = 'school'
          value: 'hive'
          value: 'flock'
          value: 'herd'
          value: 'school'
          item: ('bee', 'hive')
          item: ('bird', 'flock')
          item: ('elephant', 'herd')
          item: ('fish', 'school')
          key: 'bee'  value: 'hive'
          key: 'bird'  value: 'flock'
          key: 'elephant'  value: 'herd'
          key: 'fish'  value: 'school'
          x2.StringMap.iterkeys() = .bee =     hive
        .bird = flock
        .elephant = herd
        .fish = school
          x2.StringMap.itervalues() = .bee =     hive
        .bird = flock
        .elephant = herd
        .fish = school
          x2.StringMap.iteritems() = .bee =     hive
        .bird = flock
        .elephant = herd
        .fish = school
    }
*/

namespace Maps1
{
    $class+ X1
    {
        $xmap<int32,int32> Map;
    };

    $model+ X2
    {
        xmap<int32,int32> Map;
        xmap<string8,string8> StringMap;
    };

    void Run()
    {
		ceda::TraceGroup g("Maps example 1");

        PyRun_SimpleString(
            @strx
            (
                ceda = rootnamespace.ceda

                def printMap(name,m):
                    print '  len(' + name + ') = ' + `len(m)`
                    for key in m:
                        print '  ' + name + '.has_key(' + `key` + ') = ' + `m.has_key(key)`
                    for key in m:
                        print '  ' + `key` + ' in ' + name + ' = ' + `key in m`
                    for key in m.iterkeys():
                        print '  ' + name + '[' + `key` + '] = ' + `m[key]`
                    for value in m.itervalues():
                        print '  value: ' + `value`
                    for item in m.iteritems():
                        print '  item: ' + `item`
                    for key,value in m.iteritems():
                        print '  key: ' + `key` + '  value: ' + `value`
                    print '  ' + name + '.iterkeys() = ' + `m.iterkeys()`
                    print '  ' + name + '.itervalues() = ' + `m.itervalues()`
                    print '  ' + name + '.iteritems() = ' + `m.iteritems()`

                cs = ceda.CreateCSpace2(1000)
                ceda.SetThreadCSpace(cs)
                cs.Lock(ceda.ECSpaceLockMode.Exclusive)
                X1 = rootnamespace.Maps1.X1
                X2 = rootnamespace.Maps1.X2

                x1 = X1()

                # todo: currently fails to print the map because reflected fields of non-model fields dont have a m_mapOrSetFns member
                # (see ReflectedField versus ReflectedModelField)
                print 'x1 = ' + `x1`

                x2 = X2()
                print 'x2 = ' + `x2`
                print 'len(x2.Map) = ' + `len(x2.Map)`

                print 'x2.Map[1] = ' + `x2.Map[1]`
                print 'len(x2.Map) = ' + `len(x2.Map)`
                print 'x2.Map[2] = ' + `x2.Map[2]`
                print 'len(x2.Map) = ' + `len(x2.Map)`

                x2.Map[1] = 10
                x2.Map[2] = 20
                printMap('x2.Map', x2.Map)

                for k in range(5):
                    if k in x2.Map:
                        print `k` + ' is in x2.Map'

                x2.Map[3] = 30
                printMap('x2.Map', x2.Map)

                for k in range(100000):
                    x2.Map[k] = k

                c = 0
                for k in range(100000):
                    if k in x2.Map:
                        c += 1
                print 'c = ' + `c`

                x2.StringMap['elephant'] = 'herd'
                x2.StringMap['bee'] = 'hive'
                x2.StringMap['bird'] = 'flock'
                x2.StringMap['fish'] = 'school'
                printMap('x2.StringMap', x2.StringMap)

                cs.Unlock()
                cs.Destroy()
            ));
    }
}

namespace Maps
{
    void Run()
    {
        Maps1::Run();
    }
}
