// exPython.cpp

#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/CedaAssert.h"
#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/TracerUtils.h"
#include "Ceda/cxUtils/HPTime.h"
@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/TargetRegistry.h"
@import "Ceda/cxPython/cxPython.h"

mRegisterLeafTarget(
    {
        // version         year mon day
        { "Release 0.0",   {2008,8,24}  },      // Release #0
    })

namespace GlobalFunctions { void Run(); }
namespace Creation { void Run(); }
namespace Models { void Run(); }
namespace ClassMethods { void Run(); }
namespace ClassMemberVariables { void Run(); }
namespace InterfaceMethods { void Run(); }
namespace Adt { void Run(); }
namespace Marshall { void Run(); }
namespace Reflection { void Run(); }
namespace Strings { void Run(); }
namespace ExInt64 { void Run(); }
namespace Exceptions { void Run(); }
namespace Comparisons { void Run(); }
namespace GlobalVariables { void Run(); }
namespace Pointers { void Run(); }
namespace VectorEx { void Run(); }
namespace Variants { void Run(); }
namespace Maps { void Run(); }

$function+ void Assert(bool c)
{
    cxAssert(c);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// exPython_RunAllExamples()

#if defined(_WIN32)
    static ceda::DebugStringTraceObserver s_traceObserver;
#endif

void exPython_RunAllExamples()
{
    Tracer() << "----------------------------------- exPython examples  -----------------------------------\n";
    ceda::HPTimer timer;
    #if defined(_WIN32)
        ceda::SetTraceObserver(&s_traceObserver);
    #endif

    ceda::xstring errorString;
    if (!ceda::InitialisePython(errorString))
    {
        Tracer() << "Error: " << errorString << '\n';
        return;
    }
    ceda::BindPythonSysOutputToTracer();

    PyRun_SimpleString(
        @strx
        (
            Assert = rootnamespace.Assert
            Assert(rootnamespace == ceda.GetTheRootNameSpace())
        ));

    GlobalFunctions::Run();
    Creation::Run();
    Models::Run();
    ClassMethods::Run();
    ClassMemberVariables::Run();
    InterfaceMethods::Run();
    Adt::Run();
    Marshall::Run();
    Reflection::Run();
    Strings::Run();
    ExInt64::Run();
    Exceptions::Run();
    Comparisons::Run();
    GlobalVariables::Run();
    Pointers::Run();
    VectorEx::Run();
    Variants::Run();
    Maps::Run();

    // Needed to avoid memory leaks.  Also to stop assertions that wrapper counts fall to zero.
    Py_Finalize();

    timer.Done() << "exPython examples completed successfully";
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// main()

CEDA_MAIN_FUNCTION(argc,argv)
{
    CEDA_REGISTER_PROJECT
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);
    exPython_RunAllExamples();
#ifdef CEDA_CHECK_ASSERTIONS
    ReportLeaks(Tracer());
#endif
    ceda::DestroyTheTraceFile();
    return 0;
}

