// ClassMethods.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

#include "Ceda/cxUtils/TracerUtils.h"
@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/WCSpace.h"
@import "Ceda/cxPython/cxPython.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Class methods
-------------

Reflected class methods may be directly called from python.
*/

namespace ClassMethods1
{
    $struct+ X
    {
        X(ceda::int32 offset = 0) : m_offset(offset) { Tracer() << "X()\n"; }
        ~X() { Tracer() << "~X()\n"; }
        
        $ceda::int32 Offset(ceda::int32 x) const
        {
            Tracer() << "Offset()\n";
            return m_offset + x;
        }
        
        $ceda::int32 m_offset;
    };

	$function+ X CreateX(ceda::int32 offset)
	{
	    return X(offset);
	}
	
	void Run()
	{
		ceda::TraceGroup g("Class methods example 1");
		
        PyRun_SimpleString(
            @strx
            (
                ns = rootnamespace.ClassMethods1
                
                print 'ns.X = ' + `ns.X`

                print 'ns.CreateX(1).Offset(2) = ' + `ns.CreateX(1).Offset(2)`
                
                #y = ns.CreateX(100).Offset(10)
                #y = ns.CreateX(100).m_offset
                
                x = ns.CreateX(100)
                y = x.Offset(10)
                print 'y = ' + `y`
            ));
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////

namespace ClassMethods
{
    void Run()
    {
        ClassMethods1::Run();
    }
}

