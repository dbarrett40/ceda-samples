// ValidateSerialisation.h
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2009

#include "Ceda/cxUtils/Tracer.h"
#include "Ceda/cxUtils/Archive.h"
#include "Ceda/cxUtils/PagedBufferAsStream.h"
#include "Ceda/cxUtils/ArchiveCollection.h"
@import "Ceda/cxObject/Object.h"

namespace ceda
{

// This function is used to verify that T can be serialised to/from an archive
template<typename T>
void ValididateSerialisation(const T& L1)
{
    Tracer() << "Checking serialisation\n";
    
    Tracer() << "    src = " << L1 << '\n';

    // Serialise L1 to a xvector<octet_t>
    xvector<octet_t> buffer;
    {
        PagedBuffer pb;
        PagedBufferToOutputStreamAdapter os(pb);
        {
            Archive ar(&os);
            ar << L1;
        }
        pb.WriteTo(buffer);
    }
    
    // Deserialise into L2
    T L2;
    InputArchive arIn(buffer.data());
    arIn >> L2;
    cxAssert(arIn == buffer.data_end());
    Tracer() << "    dst = " << L2 << '\n';
    
    cxAssert(L1 == L2);
}

}