// ReflectedMemberFunctions.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/PrintReflectedVariable.h"
@import "ExampleUtils.h"
#include <assert.h>

/*
Reflected Member Functions
--------------------------

When a class is reflected we allow for its methods to be reflected.  The framework write a stub 
global function that calls the class method.

At present we only support reflection of non-static member functions
*/                           
namespace ReflectedMemberFunctions1
{
    $struct+ X isa ceda::IObject
    {
        $void foo()
        {
            s = "hello";
        }

        $void foo(ceda::int32 x)
        {
        }
        
        $ceda::int32 bar(ceda::float64 y) const;
        
        $ceda::string8 s;
    };

    ceda::int32 X::bar(ceda::float64 y) const
    {
        return 10;
    }
    
    $function+ ceda::int32 Foo(ceda::int64 x, ceda::int32 y)
    {
        return (ceda::int32) x + 1;
    }
    
    void Run()
    {
		ceda::TraceGroup g("ReflectedMemberFunctions 1");

        X x;
        ceda::ptr<ceda::IObject> p = &x;
        ceda::PrintInstance(Tracer(), p);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace ReflectedMemberFunctions
{
    void Run()
    {
        ReflectedMemberFunctions1::Run();
    }
}


