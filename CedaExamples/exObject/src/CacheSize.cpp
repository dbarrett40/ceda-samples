// CacheSize.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2019

@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/DGNode.h"
#include "Ceda/cxUtils/xstring.h"
#include "Ceda/cxUtils/xvector.h"
@import "ExampleUtils.h"
#include <vector>
#include <map>
#include <set>

///////////////////////////////////////////////////////////////////////////////////////////////////

namespace ClearCacheExamples
{
	void Run()
	{
		ceda::TraceGroup g("Clear cache examples");

        // Clearing a cache value doesn't mean resetting it to its default value, it's only about 
        // reclaiming memory where possible.
        ceda::int32 i = 10;
        ceda::ClearCacheValue(i);
        cxAssert(i == 10);

        ceda::xstring s = "hello";
        ceda::ClearCacheValue(s);
        cxAssert(s.empty());

        std::vector<int> v{1,2,3};
        ceda::ClearCacheValue(v);
        cxAssert(v.empty());

        std::map<int,int> m;
        m[1] = 1;
        m[2] = 2;
        ceda::ClearCacheValue(m);
        cxAssert(m.empty());
    }
}

namespace CacheSize1
{
	void Run()
	{
		ceda::TraceGroup g("Cache size example 1");

        static_assert( ceda::TypeHasAdditionalSize<ceda::char8>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::char16>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::int8>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::int16>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::int32>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::int64>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::uint8>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::uint16>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::uint32>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::uint64>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::float32>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::float64>() == false );
        static_assert( ceda::TypeHasAdditionalSize<ceda::Guid>() == false );

        static_assert( ceda::TypeHasAdditionalSize<ceda::xstring>() == true );
        static_assert( ceda::TypeHasAdditionalSize<ceda::xvector<int>>() == true );
        static_assert( ceda::TypeHasAdditionalSize<std::vector<int>>() == true );
        static_assert( ceda::TypeHasAdditionalSize<std::map<int,int>>() == true );
        static_assert( ceda::TypeHasAdditionalSize<std::set<int>>() == true );

        ceda::xstring s0 = "012";
        cxAssert( ceda::CacheValueAdditionalSize(s0) == ceda::HeapAllocationOverhead + 3 );
        cxAssert( ceda::CacheValueSize(s0) == sizeof(ceda::xstring) + ceda::HeapAllocationOverhead + 3 );

        ceda::xstring s1 = "01234";
        cxAssert( ceda::CacheValueAdditionalSize(s1) == ceda::HeapAllocationOverhead + 5 );
        cxAssert( ceda::CacheValueSize(s1) == sizeof(ceda::xstring) + ceda::HeapAllocationOverhead + 5 );

        ceda::xstring s2 = "0123456789";
        cxAssert( ceda::CacheValueAdditionalSize(s2) == ceda::HeapAllocationOverhead + 10 );
        cxAssert( ceda::CacheValueSize(s2) == sizeof(ceda::xstring) + ceda::HeapAllocationOverhead + 10 );

        ceda::xvector< ceda::xstring > v;
        v.push_back(s0);
        v.push_back(s1);
        v.push_back(s2);
        Tracer() << "CacheValueAdditionalSize(v) = " << ceda::CacheValueAdditionalSize(v) << '\n';
        cxAssert( ceda::CacheValueAdditionalSize(v) == 4*ceda::HeapAllocationOverhead + 3*sizeof(ceda::xstring) + 3+5+10 );

        auto p1 = std::make_pair(s0,s1);
        Tracer() << "CacheValueAdditionalSize(p1) = " << ceda::CacheValueAdditionalSize(p1) << '\n';
        cxAssert( ceda::CacheValueAdditionalSize(p1) == 2*ceda::HeapAllocationOverhead + 3+5 );

        auto p2 = std::make_pair(p1,s2);
        Tracer() << "CacheValueAdditionalSize(p2) = " << ceda::CacheValueAdditionalSize(p2) << '\n';
        cxAssert( ceda::CacheValueAdditionalSize(p2) == 3*ceda::HeapAllocationOverhead + 3+5+10 );

        std::map<ceda::xstring, ceda::xstring> m;
        m[s0] = s1;
        Tracer() << "CacheValueAdditionalSize(m) = " << ceda::CacheValueAdditionalSize(m) << '\n';
        cxAssert( ceda::CacheValueAdditionalSize(m) == ceda::StdMapElementOverhead + ceda::CacheValueSize(s0) + ceda::CacheValueSize(s1) );
    }	
}


///////////////////////////////////////////////////////////////////////////////////////////////////
namespace CacheSize
{
    void Run()
    {
        ClearCacheExamples::Run();
        CacheSize1::Run();
    }
}






