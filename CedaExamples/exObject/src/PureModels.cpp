// PureModels.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2011

@import "Ceda/cxObject/PrintReflectedType.h"
@import "Ceda/cxObject/PrintReflectedVariable.h"
@import "Ceda/cxObject/IObjectVisitor.h"
@import "ValidateSerialisation.h"
@import "ExampleUtils.h"
#include <math.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
// PureModels1

/*
A model is rather like a C data struct, where the intention is to treat the members as public.

Ceda provides some specialised support for models:

    -   constructor notation
    -   reflection
    -   conversion to/from textual representations
    -   gui generation for viewing and editing
    -   efficient binary serialisation
    -   persistence
    -   operational transformation
    -   representing independent nodes of the Dependency Graph System (DGS).
    
Models can be embedded in other classes.  In this example we are instead considering "pure models",
also called "free standing models".

Serialisation
-------------

Typically serialiation functions (operator<< and operator>>) are automatically generated.  This is 
disabled when the model contains members that don't support serialisation (e.g. pointers).

Get/Set methods
---------------

Get/Set accessor functions to the members are written automatically

Structural versus nominative type systems
-----------------------------------------

There are two possible views with regard to structural versus nominative type systems:

1) A pure model introduces a named type, that is distinct from all other types in the type system.
   In that sense pure models are associated with a nominative type system.  For example, a pure model
   named 'Circle' is intended to be regarded as a representation of Circle values, rather than a 
   representation of tuples which are in turn a representation of Circle values.

2) A pure model is actually just a tuple type, and you need a class to redefine equality.

[At the moment 2) is closer to the truth because we currently don't allow the developer to to redefine 
operator==() on a pure model.  Currently xcpp always generates a memberwise equality test implementation 
as though a pure model is a tuple type]

Output
------

    Size of Point = 16 bytes
    Size of Circle = 24 bytes
    Size of Line = 32 bytes
    p = Point(0,0)
    c = Circle(Point(0,0),0)
    l = Line(Point(0,0),Point(0,0))
    unitCircle = Circle(Point(0,0),1)
    p == Point(0,0)
    Checking serialisation
        src = Circle(Point(0,0),1)
        dst = Circle(Point(0,0),1)
    m_name = PureModels1::Circle
    m_flags = 2
    m_size = 24
    m_numModelFields = 2
    model PureModels1::Circle
    {
        (0) PureModels1::Point Centre;
        (16) float64 Radius;
    };
*/

namespace PureModels1
{
    /*
    Models can be defined using platform independent basic types such as
    
        bool
        char8, char16,
        int8, int16, int32, int64,
        uint8, uint16, uint32, uint64
        float32, float64
        string8, string16

    Note that platform dependent types like short, int, float cause compile time errors
    
    All members of a model are public.  There is no supported syntax for defining private 
    or protected members.
    
    A model cannot have any methods defined
    
    User defined destructors are not permitted
    
    Dependent variables (declared using $dep) are not permitted
    */
    $model+ Point
    {
        ceda::float64 X;
        ceda::float64 Y;
    };

    /*
    Models can nest.  E.g. a Circle model can use a Point model to represent the centre.	
    */
    $model+ Circle
    {
        Point Centre;
        ceda::float64 Radius;
    };

    $model+ Line
    {
        Point P1;
        Point P2;
    };
    
    void Run()
    {
        ceda::TraceGroup g("Pure models example 1");

        /*
        A pure model is represented in memory like a C style data struct, without any additional
        state such as a vtable pointer.  Alignment of members is platform dependent.

        For example, on most platforms the size of Point is 16 bytes, which is to be expected
        for storing a pair of 64 bit floating point numbers.

            Size of Point = 16 bytes
            Size of Circle = 24 bytes
            Size of Line = 32 bytes
        */
        Tracer() << "Size of Point = " << sizeof(Point) << " bytes\n";
        Tracer() << "Size of Circle = " << sizeof(Circle) << " bytes\n";
        Tracer() << "Size of Line = " << sizeof(Line) << " bytes\n";

        /*
        Unlike C structs, models are initialised with well defined default values.  For example
        the default value of a float64 is 0.  Therefore the default value of a Point is (0,0)
        */
        Point p;         // p = Point(0,0)
        Circle c;        // c = Circle(Point(0,0),0)
        Line l;          // l = Line(Point(0,0),Point(0,0))

        /*
        Models have constructors defined, where the arguments correspond to the order in which the
        members appear within the model
        */
        Circle unitCircle = Circle(Point(0,0),1);

        /*
        By default operator<<() to an xostream is defined on a model.  The following writes

            p = Point(0,0)
            c = Circle(Point(0,0),0)
            l = Line(Point(0,0),Point(0,0))
            unitCircle = Circle(Point(0,0),1) 
        */
        Tracer() << "p = " << p << '\n';
        Tracer() << "c = " << c << '\n';
        Tracer() << "l = " << l << '\n';
        Tracer() << "unitCircle = " << unitCircle << '\n';

        /*
        operator==() and operator!=() are automatically generated on models,  based on memberwise
        comparisons.
        */
        if (p == Point(0,0)) Tracer() << "p == Point(0,0)\n";
        if (p != Point(0,0)) Tracer() << "p != Point(0,0)\n";

        /*
        Pure models typically support serialisation using an Archive and deserialisation with an 
        InputArchive        
        */
        ceda::ValididateSerialisation(unitCircle);

        /*
        Pure models are reflected using structs of type 'ReflectedClass' and registered by the
        fully qualified C++ name (i.e. using '::' namespace separator)
        
        A ReflectedClass has a number of members.  Some relevant here are:
        
            m_name = PureModels1::Circle
            m_flags = 2 (i.e. 1 << RCF_MODEL)
            m_size = 24
            m_numModelFields = 2

        There is no means for preventing reflection of models - they are always 
        reflected.  Also, all members of a model are necessarily reflected
        */
        const ceda::ReflectedClass* rc = ceda::TryFindReflectedClass("PureModels1::Circle");
        Tracer() << "name = " << rc->name << '\n'
                 << "flags = " << rc->flags << '\n'
                 << "size = " << rc->ops.size << '\n'
                 << "numModelFields = " << rc->numModelFields << '\n';

        /*
        Defined in PrintClass.h.  Allows a given ReflectedClass to be written to a given
        xostream.  Outputs
        
            model PureModels1::Circle
            {
                (0) PureModels1::Point Centre;
                (16) float64 Radius;
            };
        */
        ceda::PrintReflectedClass(Tracer(), *rc);

        /*
        For a member named M, M can be accessed directly by name
        */
        l.P1 = Point(1,2);
        l.P2 = l.P1;
        
        /*
        For simple types GetXXX() and SetXXX() methods are defined
        */
        p.SetX(p.GetX() + 5);
        
        /*
        For nested models, the GetXXX() and SetXXX() functions are defined according to the 
        granulairity of assignment operations for the purposes of Operational Transformation.
        */
        l.SetP1Y(l.GetP2X() + 2);
    }
}

namespace UserDefinedCtorsOnModels
{
    /*
    User defined constructors can be declared on pure models.  These must be inlined and named 
    '$$'.
    
    A user-defined default constructor $$() stops the auto-generated one.
    
    The constructors must be declared after the members and before any $dropped, $schema, $evolve
    sections.
    */
    $model Point
    {
        ceda::float64 X;
        ceda::float64 Y;
        
        // User defined default constructor stops the auto-generated one.
        $$() : X(-1), Y(-1) {}
        
        // Single arg ctors can be declared 'explicit' to prevent implicit conversions
        explicit $$(ceda::float64 x) : X(x), Y(0) {}
    };
    
    // This cannot be made a constructor because it would clash with the auto-generated 
    // constructor
    //
    //      Point(float64 x,float64 y)
    Point MakePolar(ceda::float64 r, ceda::float64 t)
    {
        return Point(r*cos(t), r*sin(t));
    }
    
    void Run()
    {
        ceda::TraceGroup g("UserDefinedCtorsOnModels");

        /*
        Output:

            p1 = Point(-1,-1)
            p2 = Point(0.707107,0.707106)
            p3 = Point(10,0)
        */
        Point p1;
        Point p2 = MakePolar(1,3.14159/4);
        Point p3 = Point(10.0);
        Tracer() << "p1 = " << p1 << '\n';
        Tracer() << "p2 = " << p2 << '\n';
        Tracer() << "p3 = " << p3 << '\n';
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Output
------
    
    p = Point([0,0,0])
    x = X([])
    x = X([0,0,-3,0,0,7])
    x = X([0,0,-3,0,0,7,0,0,0,0,0])
    y = Y([[[[],[]],[[],[]]],[[[],[]],[[],[]]],[[[],[]],[[X([]),X([]),X([]),X([]),X([0,0,0,5])],[]]]])
*/
namespace ArraysInModels
{
    /*
    Fixed size arrays can be used in models
    */
    $model Point
    {
        ceda::float64 C[3];
    };
    
    /*
    Dynamic arrays are supported.  The size grows implicitly as a result of operations on the 
    elements.
    */
    $model X
    {
        ceda::int32 A[];
    };
    
    /*
    Fixed arrays, dynamic arrays and models can nest in arbitrary ways
    */
    $model Y
    {
        X P[][2][2][];
    };
    
    void Run()
    {
        ceda::TraceGroup g("ArraysInModels");

        /*
            p = Point([0,0,0])
        */
        Point p;
        Tracer() << "p = " << p << '\n';

        // todo: would be nice if this worked
        //float64 A[3] = {1,0,0};
        //Point p2(A);
        //Tracer() << "p2 = " << p2 << '\n';

        /*
        Dynamic arrays are initially empty

            x = X([])
        */
        X x;
        Tracer() << "x = " << x << '\n';

        /*
        Dynamic arrays grow as required according to the accesses
        
            x = X([0,0,-3,0,0,7])
        */
        x.A[5] = 7;		
        x.A[2] = -3;		
        Tracer() << "x = " << x << '\n';

        /*
        Simply reading an element of a dynamic array can cause it to grow!
        
            x = X([0,0,-3,0,0,7,0,0,0,0,0])
        */
        ceda::int32 a = x.A[10];		
        Tracer() << "x = " << x << '\n';

        /*
            y = Y([[[[],[]],[[],[]]],[[[],[]],[[],[]]],[[[],[]],[[X([]),X([]),X([]),X([]),X([0,0,0,5])],[]]]])

        which if you expand out is:

            y = Y
            (
                [
                    [
                        [
                            [],[]
                        ],
                        [
                            [],[]
                        ]
                    ],
                    [
                        [
                            [],[]
                        ],
                        [
                            [],[]
                        ]
                    ],
                    [
                        [
                            [],[]
                        ],
                        [
                            [
                                X([]),
                                X([]),
                                X([]),
                                X([]),
                                X([0,0,0,5])
                            ],
                            []
                        ]
                    ]
                ]
            )

        */
        Y y;
        y.P[2][1][0][4].A[3] = 5;
        Tracer() << "y = " << y << '\n';
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Models support xvector members.  push_back() can be used to append elements.

Output
------

    p = PointSet([Point(0,0),Point(1,0)])
*/
namespace VectorsInModels
{
    $model Point
    {
        ceda::int32 X;
        ceda::int32 Y;
    };

    $model PointSet
    {
        ceda::xvector<Point> L;
    };

    void Run()
    {
        ceda::TraceGroup g("VectorsInModels");
        PointSet p;
        p.L.push_back(Point(0,0));
        p.L.push_back(Point(1,0));
        Tracer() << "p = " << p << '\n';
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Non-models in models
--------------------

The rule is:  every $class/$struct member of a $model defaults to assignment semantics on the 
entire member, even if the $class/$struct has a model of its own.  E.g. in this case assignments 
to X,Y components of an end point of a line aren't available.

    $struct Point :
        model
        {
            ceda::int32 X;
            ceda::int32 Y;
        }
    {
    };

    $struct Line isa ceda::IPersistable :
        model
        {
            Point P1;
            Point P2;
        }
    {
    };

This is equivalent to

    $struct Line isa ceda::IPersistable :
        model
        {
            assignable<Point> P1;
            assignable<Point> P2;
        }
    {
    };

The only way to get finer grained operations is to embed a pure model in a model.   So instead 
use

    $model Point
    {
        ceda::int32 X;
        ceda::int32 Y;
    };

It is recommended you favour pure models and use assignable<> to control the granularity of 
assignment.

Output
------

    m = M(0)
    m = M(3)
*/
namespace ClassesInModels
{
    /*
    A $class/$struct can be used in a $model as long as it provides certain minimum functionality
    
        default constructor
        copy constructor
        copy assignment
        operator==()
        operator<()
        writing to an xostream
        
    There is also some optional functionality:
        
        writing to an Archive, reading from an InputArchive
    
    In this case, X doesn't reflect serialisation methods, and therefore a model in which X appears
    won't support serialisation either.
    */
    $struct X
    {
        explicit X(ceda::int32 v=0) : v(v) {}
        bool operator==(const X& rhs) const { return v == rhs.v; }
        bool operator<(const X& rhs) const { return v < rhs.v; }
        ceda::int32 v;
    };
    ceda::xostream& operator<<(ceda::xostream& os, const X& x)
    {
        os << x.v;
        return os;
    }
    
    $model M
    {
        X x;
    };

    void Run()
    {
        ceda::TraceGroup g("ClassesInModels");

        /*
        output:
            m = M(0)
        */
        M m;
        Tracer() << "m = " << m << '\n';

        /*
        output:
            m = M(3)
        */
        m.x = X(3);
        Tracer() << "m = " << m << '\n';
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Pointers in models
------------------

Pointers can appear in models, but the affect is to disable support for serialisation and 
persistence.  Transient models are nevertheless useful in certain contexts - such as to allow
transient independent variables for the Dependency Graph System (DGS).

Output (on x86)
------

    m = M(00000000,00000000,00000000,00000000,null,null)
*/
namespace PointersInModels
{
    $model M
    {
        ceda::int32* A;
        const ceda::int32* B;
        void* C;
        const void* D;
        ceda::ptr<ceda::IObject> E;
        ceda::ptr<const ceda::IObject> F;
    };

    void Run()
    {
        ceda::TraceGroup g("PointersInModels");

        /*
        output:
            m = M(00000000,00000000,00000000,00000000,null,null)
        */
        M m;
        Tracer() << "m = " << m << '\n';
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Schema1
/*
Schema evolution of models
--------------------------

It is assumed every model and every class that implements IPersistable is defined in a certain 
target.  A target is either a DLL or EXE.  E.g. a target may be named "Ceda/Geom/cxGeom.dll"

Every target is assumed to have a linear sequence of formal releases.  For each target this leads 
to the concept of a Release Sequence Number (RSN) numbered 0,1,2,...

Consider an object identified by an OID that cxPersistStore writes to the LSS as a single serial 
element (i.e. sequence of bytes).  The serial element begins with the current RSN of the target DLL/EXE
in which the object's class (that implements IPersistable) is defined.  The RSN is written as a 
variable length integer - probably as a single byte.  It is assumed this uniquely defines the schema 
of all nested models written to the same serial element.  So there is no need for each instance of a 
model to write some kind of schema number.  Hence schema evolution is achieved with minimal space 
overhead (i.e. typically only one byte per object with an OID).

Ceda provides a mechanism for relating RSNs of one target to the RSNs of a subtarget that it 
statically links against - either directly or indirectly.  See TargetRegistry.h in cxObject.  It is 
assumed that if a target defines an IPersistable class, then for every model that is serialised to 
the same serial element

    a)  the model is defined in either the same target or else a target to which there is a static 
        linking dependency; and

    b)  we can determine the RSN of the target in which the model is defined.

Therefore schema evolution on a model is related back to RSNs of the target in which the model is
defined.

A model, initially defined like this

    $model Point
    {
        ceda::float64 x;
        ceda::float64 y;
    };

has serialise/deserialise methods automatically generated as follows

    void Point::Serialise(ceda::Archive& _ar) const
    {
        _ar << x << y;
    }
    void Point::Deserialise(ceda::InputArchive& _ar)
    {
        _ar >> x >> y;
    }

Over time we allow fields to be added and dropped.  This information is defined in a $schema block
within a $model.

In this example we illustrate how a $model may evolve over time, and how the changes are related back
to RSNs of the target in which it is defined.

Reflection of schema evolution
------------------------------

All current and dropped fields are reflected.  Each field is reflected using a ReflectedModelField
which provides a range [rsn1,rsn2) of rsns for which the field was present, and using rsn2 = -1
for fields that are present in the latest schema.

        static const octet_t Point_x[] =
        {
            0x0d
        };
        static const octet_t Point_y[] =
        {
            0x0d
        };
        static const octet_t Point_z[] =
        {
            0x0d
        };
        static const octet_t Point_r[] =
        {
            0x0d
        };
        static const octet_t Point_theta[] =
        {
            0x0d
        };
        static const ceda::ReflectedModelField Point_modelFields[] =
        {
            { "x", -1, Point_x, 3, 7 },
            { "y", -1, Point_y, 3, 7 },
            { "z", -1, Point_z, 4, 6 },
            { "r", offsetof(Point,r), Point_r, 7, -1 },
            { "theta", offsetof(Point,theta), Point_theta, 7, -1 },
        };

Generated serialise/deserialise functions
-----------------------------------------

    void Point::Serialise(ceda::Archive& _ar) const
    {
        _ar << r << theta;
    }
    void Point::Deserialise(ceda::InputArchive& _ar)
    {
        ceda::RSN _rsn = ceda::GetModelRsn();
        struct EvolveCode
        {
            static void calc_r(ceda::float64& r,const ceda::float64& x,const ceda::float64& y) { r = sqrt(x*x + y*y); }
            static void calc_theta(ceda::float64& theta,const ceda::float64& x,const ceda::float64& y) { theta = atan2(y,x); }
        };
        
        ceda::float64 x;
        if (_rsn < 7) _ar >> x;
        
        ceda::float64 y;
        if (_rsn < 7) _ar >> y;
        
        ceda::float64 z;
        if (_rsn <= 3) {  }
        else if (_rsn < 6) _ar >> z;
        
        if (_rsn <= 6) { EvolveCode::calc_r(r,x,y); }
        else _ar >> r;
        
        if (_rsn <= 6) { EvolveCode::calc_theta(theta,x,y); }
        else _ar >> theta;
    }
*/

namespace Schema1
{
    @if (false)
    {
        /*
        This is the initial version of the model, that appeared in release #3 (i.e. RSN=3 - which is
        actually the 4th release because they start at 0) of the target in which Point is defined.
        
        There is no schema block because there have been no schema changes yet. If a schema block was
        defined, it would be
        
            $schema
            {
                3:  +x +y              // Release #3    : (x,y)
            }
            
        to indicate that in release #3, fields x and y were added.
        */
        $model Point
        {
            ceda::float64 x;
            ceda::float64 y;
        };

        /*
        In releases #4, #5 it was decided to add a z coordinate to the Point.
        */
        $model Point
        {
            ceda::float64 x;
            ceda::float64 y;
            ceda::float64 z;
            
            $schema
            {
                3:  +x +y              // Release #3    : (x,y)
                4:  +z                 // Release #4,5  : (x,y,z)
            }
        };        

        /*
        In release #6 it was decided to drop the z coordinate        

        Note that the declaration of z has been moved into a $dropped section.  This provides the type 
        information for dropped fields.

        Once a member has been dropped it can never be reinstated.  Instead a new member with a different 
        name must be added.  Note that attribute names are not actually significant, so you can actually
        give a dropped field a different name in order to use the original name again for a different
        field.
        */
        $model Point
        {
            ceda::float64 x;
            ceda::float64 y;

            $dropped
            {
                ceda::float64 z;
            }
            
            $schema
            {
                3:  +x +y              // Release #3    : (x,y)
                4:  +z                 // Release #4,5  : (x,y,z)
                6:  -z                 // Release #6    : (x,y)
            }
        };        
    }
        
    /*
    From release #7 to the present it was decided to change to polar coordinates.  This means x and y 
    were dropped, and a $evolve block allows for specifying the initialisation of new members (r,theta)
    in terms of dropped ones (x,y).
    
    Note that in the $schema block we have the line

            7:  -x -y +r +theta    // Release #7-   : (r,theta)

    It is a requirement in the xcpp parser that variables must be dropped before variables are 
    added.  For example x,y are dropped before r,theta are added. 
    */
    $model Point
    {
        ceda::float64 r;
        ceda::float64 theta;
        
        $$() : r(0), theta(0) {}
        $$(ceda::float64 _r, ceda::float64 _t) : r(_r), theta(_t) {}
        
        $dropped
        {
            ceda::float64 x;
            ceda::float64 y;
            ceda::float64 z;
        }

        $schema
        {
            3:  +x +y              // Release #3    : (x,y)
            4:  +z                 // Release #4,5  : (x,y,z)
            6:  -z                 // Release #6    : (x,y)
            7:  -x -y +r +theta    // Release #7-   : (r,theta)
        }
        
        $evolve
        {
            r(x,y)     { r = sqrt(x*x + y*y); }
            theta(x,y) { theta = atan2(y,x); }
        }
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Some examples of error messages produced by xcpp

    @if (false)
    {
        $model A
        {
            ceda::int32 x;
            $schema
            {
                1: +x +x     // Field x already added in the schema
            }
        };

        $model A
        {
            ceda::int32 x;
            $schema
            {
                1: +x
                2: +x        // Field x already added in the schema
            }
        };

        $model A
        {
            ceda::int32 x;
            $dropped
            {
                ceda::int32 y;
            }    
            $schema
            {
                1: +x
                2: -y       // Error : Field y can't be dropped from the schema until it has been added
            }
        };

        $model A
        {
            ceda::int32 x;
            $dropped
            {
                ceda::int32 y;
            }    
            $schema
            {
                1: +x +y -y     // Syntax error: unexpected '-' : note that dropped fields must appear before added fields within a schema
            }
        };

        $model A
        {
            ceda::int32 x;
            $dropped
            {
                ceda::int32 y;
            }    
            $schema
            {
                1: +x +y
                2: -y
                3: +y         // Field y already added in the schema
            }
        };

        $model A
        {
            ceda::float64 X;
            $schema {}        // Field X never added in a schema
        };
        
        $model A
        {
            ceda::float64 X;
            $schema { +X }      // Release number expected when read +
        };
        
        $model A
        {
            $schema {1: +X}     // Field X not declared in the model
            ceda::float64 X;
        };
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace Schema2
{
    /*
    The order in which the variables are added in the $schema block takes precedence over the order
    in which they appear in the model.

    This order, determined by the $schema block is made for
        -   the order in which the members appear in memory
        -   the order in which the members are serialised to an archive
        -   the order in which the members are displayed in textual form
        -   the order of the arguments in a constructor such as Point(y,x)
        -   the order in which the members are reflected (i.e. the array ReflectedClass::m_modelFields)
    
    So this is equivalent to having written
    
        $model Point
        {
            float64 y;
            float64 x;

            $schema
            {
                10:  +y +x
            }
        };
    */
    $model Point
    {
        ceda::float64 x;
        ceda::float64 y;

        $schema
        {
            10:  +y +x
        }
    };

    void Run()
    {
        ceda::TraceGroup g("Schema2");

        /*
        output:
            p = Point(2,1)
        */
        Point p;
        p.x = 1;
        p.y = 2;
        Tracer() << "p = " << p << '\n';
    }
}

namespace Schema3
{
    /*
    A $evolve method is only ever used exactly once, when an object from the version before the 
    version where the member was added is being evolved to the latest version. A subsequent 
    evolution will not evolve the same member again using the same method.

    Note that when schema evolution is applied it doesn't itself mark the object as dirty (it
    is not considered reasonables for a read function to give you a dirty object).  So if the
    object is not marked as dirty before being removed from memory the next time the object is 
    deserialised it will be schema evolved again.

    Note that if the schema was changed from say version 10 to version 11, a member for an object 
    might be evolved, and then if the schema was changed again from version 11 to 12 (e.g. a 
    different member added), the evolve code for the already-evolved member does not again.
    In fact on entry into a $evolve method it is possible to assert that the new member has the 
    default constructed value.

    Note that $evolve is not concerned with changing existing members.  That is not supported.
    Instead it is important to think in terms of adding new members and dropping old ones.  $evolve 
    code is associated with the initialisation of a new member in terms of members in the previous 
    schema, and members can only be added once.
    
    One might wonder what would happen if you actually did want to evolve a member in multiple schema 
    versions. E.g. say you have a member representing a distance in millimeters (schema 0). In a later 
    schema you decide it�d be better to use centimeters (schema 1). And then you change your mind again 
    and want to use meters (schema 2).  This is easily supported by adding and dropping members, as
    shown in the example below

    It generates the following serialisation functions.  Explanatory comments have been added.

    void X::Serialise(ceda::Archive& _ar) const
    {
        // Note that X is a pure model so is not responsible for writing schema information.  That 
        // is only done by an IPersistable object

        _ar << this->mDist;       // Only write mDist, none of the dropped fields.  These don't 
                                  // exist as members of X anymore
    }
    void X::Deserialise(ceda::InputArchive& _ar)
    {
        // RSN is the Release Sequence Number, which relates to the numbers appearing in the 
        // $schema block
        ceda::RSN _rsn = ceda::GetModelRsn();

        // This struct is used to hold all the $evolve functions using static member functions.
        struct EvolveCode
        {
            static void calc_cmDist(ceda::float64& cmDist,ceda::float64 const& mmDist) { cmDist = mmDist/10; }
            static void calc_mDist(ceda::float64& mDist,ceda::float64 const& cmDist) { mDist = cmDist/100; }
        };
        
        // mmDist has been dropped so a local variable is used and it is only read from the archive
        // in the schema before it was dropped
        // It was dropped in schema 1, hence the test _rsn < 1
        ceda::float64 mmDist;
        if (_rsn < 1) _ar >> mmDist;
        
        // cmDist has been dropped so a local variable is used and it is only read in the schema 
        // before it was dropped
        // It was dropped in schema 2, hence the test _rsn < 2
        // cmDist was added in schema 1.  Therefore before then (_rsn <= 0) it needs the evolve 
        // code to run to initialise it from mmDist
        ceda::float64 cmDist;
        if (_rsn <= 0) { EvolveCode::calc_cmDist(cmDist,mmDist); }
        else if (_rsn < 2) _ar >> cmDist;
        
        // mDist has not been dropped so is a member of X not a local variable
        // It was added in schema 2.  Before then (_rsn <= 1) it must be evolved, otherwise it can 
        // be read directly from the archive
        if (_rsn <= 1) { EvolveCode::calc_mDist(mDist,cmDist); }
        else _ar >> this->mDist;
    }
    */
    $model X
    {
        float64 mDist;

        $dropped
        {
            float64 mmDist;
            float64 cmDist;
        }

        $schema
        {
            0 : +mmDist
            1 : -mmDist +cmDist
            2 : -cmDist +mDist
        }

        $evolve
        {
            cmDist(mmDist) { cmDist = mmDist/10; }
            mDist(cmDist) { mDist = cmDist/100; }
        }
    };
}

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace PureModels
{
    void Run()
    {
        PureModels1::Run();
        UserDefinedCtorsOnModels::Run();
        ArraysInModels::Run();
        VectorsInModels::Run();
        ClassesInModels::Run();
        PointersInModels::Run();
        Schema2::Run();
    }
}



