// exObject.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2006

@import "ExampleUtils.h"
@import "Ceda/cxObject/TargetRegistry.h"
#include "Ceda/cxUtils/HPTime.h"
#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/Tracer.h"
#include <iostream>
#include <map>

#ifdef CEDA_USING_VISUAL_LEAK_DETECTOR
#include <vld.h>
#endif

mRegisterLeafTarget(
    {
        // version    year  mon  day
        { "0.0",     {2008,  8,  24}  },      // Release #0
    })

namespace Adt { void Run(); }
namespace Functors { void Run(); }
namespace Interfaces { void Run(); }
namespace Classes { void Run(); }
namespace Reflect { void Run(); }
namespace Serialise { void Run(); }
namespace Mixin { void Run(); }
namespace Enum { void Run(); }
namespace DepGraph { void Run(); }
namespace DepGraph2 { void Run(); }
namespace DepGraphEviction { void Run(); }
namespace AsyncDepGraph { void Run(); }
namespace PureModels { void Run(); }
namespace ClassesWithModels { void Run(); }
namespace Variant { void Run(); }
namespace shape { void Run(); }
namespace OpenVariants { void Run(); }
namespace Vectors { void Run(); }
namespace Metadata { void Run(); }
namespace DynamicDispatch { void Run(); }
namespace CSpaceObjectCreation { void Run(); }
namespace CSpaceObjectEviction { void Run(); }
namespace Variables { void Run(); }
namespace CacheSize { void Run(); }

///////////////////////////////////////////////////////////////////////////////////////////////////
// exObject_RunAllExamples()

void exObject_RunAllExamples()
{
    Tracer() << "----------------------------------- exObject examples  -----------------------------------\n";
    ceda::HPTimer timer;

    Variables::Run();
    Adt::Run();
    Functors::Run();
    Interfaces::Run();
    Classes::Run();
    Reflect::Run();
    Serialise::Run();
    Mixin::Run();
    Enum::Run();
    CacheSize::Run();
    DepGraph::Run();
    DepGraph2::Run();
    DepGraphEviction::Run();
    AsyncDepGraph::Run();
    PureModels::Run();
    ClassesWithModels::Run();
    Variant::Run();
    shape::Run();
    OpenVariants::Run();
    Vectors::Run();
    Metadata::Run();
    DynamicDispatch::Run();
    
    // todo: currently fails
    //CSpaceObjectCreation::Run();
    
    CSpaceObjectEviction::Run();

    timer.Done() << "exObject examples completed successfully";
}

CEDA_MAIN_FUNCTION(argc,argv)
{
    
    // debug: 113 bytes per element
    /*
    std::map<int,int> m;
    for (int i=0 ; i < 1000000 ; ++i)
    {
        m[i] = i;
    }
    _getch();
    return 0;
    */
    
    /*
    // Measure the heap allocation overhead
    // This was used to find that on Windows overhead is 16 bytes in release, and 64 bytes in debug
    for (int i=0 ; i < 10000000 ; ++i)
    {
        new char;
    }
    _getch();
    return 0;
    */

    CEDA_REGISTER_PROJECT
    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);

    exObject_RunAllExamples();

#ifdef CEDA_CHECK_ASSERTIONS
    ReportLeaks(Tracer());
#endif
    ceda::DestroyTheTraceFile();

    return 0;
}
