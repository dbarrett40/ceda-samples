// DepGraphEviction.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2019

@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/DGNode.h"
@import "Ceda/cxObject/WCSpace.h"
@import "ExampleUtils.h"
#include "Ceda/cxUtils/IsaacRandom.h"
#include "Ceda/cxUtils/HPTime.h"
#include <iostream>

@def int EVICTION_QUEUE_INDEX = 3 

@def int ARRAY_SIZE = 1000000

namespace EvictionOfCachedStringsExample
{
    $struct X isa ceda::IObject
    {
        $cache <<queue(EVICTION_QUEUE_INDEX)>> ceda::xstring f(int i) const
        {
            // A string with i bytes
            return ceda::xstring(i, 'a');
        }
    };

    void Run()
    {
        ceda::TraceGroup g("Eviction of cached strings example");

        Tracer() << "RedBlackTreeElementOverhead = " << ceda::RedBlackTreeElementOverhead << '\n';

        ceda::int32 milliSecsPerGC = 5;
        ceda::CSpaceCreator cspace(milliSecsPerGC);
        ceda::SetMaxDgsTotalByteSize(EVICTION_QUEUE_INDEX, 100000*1024);

        ceda::ssize_t memoryUsage = ceda::GetMaxDgsTotalByteSize(EVICTION_QUEUE_INDEX);
        Tracer() << "Expected memory usage = " << memoryUsage/1024 << " kB\n";

        X* p = nullptr;
        {
            ceda::CSpaceTxn txn;
            p = $new X();
            ceda::AddGcRoot(p);
        }

        for (int i=1000000 ; i < 2000000 ; i += 5000)
        {
            Sleep(20);
            {
                ceda::CSpaceTxn txn;
                p->f(i);
            }
        }
    }
}

namespace EvictionOfIndepNodesExample
{
    $struct X isa ceda::IObject :
        model
        {
            bool start;
            char M[ARRAY_SIZE];
        }
    {
        X()
        {
            for (int i=0 ; i < ARRAY_SIZE ; ++i)
            {
                _model_.M[i] = (char)i;
            }
        }
        
        $cache int f() const
        {
            int n = 200;
            int sum = start.read();
            for (int i=0 ; i < n ; ++i)
            {
                int x = isaac.GetUniformDistInteger(0,ARRAY_SIZE);
                sum += M[x].read();
            }
            return sum;
        }

        mutable ceda::IsaacRandomNumberGen isaac;
    };

    void Run()
    {
        ceda::TraceGroup g("Eviction of indep nodes example");

        Tracer() << "RedBlackTreeElementOverhead = " << ceda::RedBlackTreeElementOverhead << '\n';

        ceda::int32 milliSecsPerGC = 5;
        ceda::CSpaceCreator cspace(milliSecsPerGC);
        ceda::SetMaxDgsTotalByteSize(ceda::DgsEvictionQueueIndex_SystemMemory, 30000*1024);

        ceda::ssize_t sizeOfVecDgiNode = ARRAY_SIZE*sizeof(void*);
        ceda::ssize_t memoryUsage = sizeof(X) + sizeOfVecDgiNode + ceda::GetMaxDgsTotalByteSize(ceda::DgsEvictionQueueIndex_SystemMemory);
        Tracer() << "Expected memory usage = " << memoryUsage/1024 << " kB\n";

        X* p = nullptr;
        {
            ceda::CSpaceTxn txn;
            p = $new X();
            ceda::AddGcRoot(p);
        }

        for (int i=0 ; i < 100 ; i++)
        {
            Sleep(10);
            {
                ceda::CSpaceTxn txn;
                p->start = i;
                p->f();
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace DepGraphEviction
{
    void Run()
    {
        EvictionOfCachedStringsExample::Run();
        EvictionOfIndepNodesExample::Run();
    }
}
