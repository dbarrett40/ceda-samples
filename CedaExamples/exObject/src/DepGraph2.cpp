// DepGraph2.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2014

@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/DGNode.h"
@import "Ceda/cxObject/WCSpace.h"
@import "ExampleUtils.h"

//$class M : model{} {};
//$class X : model M {};

//$class X isa ceda::IObject : model { int x; } {};
//$class Y : model { int x; } {};

//$class Z<typename T, int N> isa ceda::IObject : model { T x; } {};

//$class+ X<typename T, int N> {};

// todo : can't accept platform dependent type
//$mixin M : model { int x; } {};

//$mixin M <<genop>> : model { int32 x; } {};
//$class X isa ceda::IObject : mixin[M] {};



/*
///////////////////////// no model
$class X {};
    GetQualifiedName()= X
    HasDefaultCtor()
    WriteTraits()

$class X<typename T> {};
    GetQualifiedName()= X
    HasDefaultCtor()
    templateParameterArgList_= typename T

$mixin X {};
    GetQualifiedName()= X
    HasDefaultCtor()
    WriteTraits()
    IsUnderlyingStruct()
    IsMixin()
    m_mixinBaseClass= BC

$mixin X<typename T, int N> {};
    GetQualifiedName()= X
    HasDefaultCtor()
    IsUnderlyingStruct()
    IsMixin()
    templateParameterArgList_= typename T, int N
    m_mixinBaseClass= BC

///////////////////////// base model
$class X : model {} {};
    GetQualifiedName()= X
    HaveSomethingToRegister()
    HasEmbeddedModel()
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()
    GenerateReadBarriers()
    CanSerialiseEmbeddedModel()
    WriteInsertionExtractionInTermsOfSerialise()
    HasDefaultCtor()
    WriteTraits()
    AutoGenerateInsertionToXostream()
    m_inheritsModel
    m_numModelSchema= 1
    m_mixinBaseClass= X_model_mixin<ceda::BaseMixin<X,ceda::EmptyBase> >

$mixin X : model {} {};
    GetQualifiedName()= X
    HasEmbeddedModel()
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()
    GenerateReadBarriers()
    CanSerialiseEmbeddedModel()
    HasDefaultCtor()
    WriteTraits()
    IsUnderlyingStruct()
    IsMixin()
    m_inheritsModel
    m_numModelSchema= 1
    m_mixinBaseClass= X_model_mixin<BC>

$mixin X<typename T,int N> : model {} {};
    GetQualifiedName()= X
    HasEmbeddedModel()
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()
    GenerateReadBarriers()
    CanSerialiseEmbeddedModel()
    HasDefaultCtor()
    IsUnderlyingStruct()
    IsMixin()
    m_inheritsModel
    m_numModelSchema= 1
    templateParameterArgList_= typename T,int N
    m_mixinBaseClass= X_model_mixin<BC>

///////////////////////// refd base model
$model M {};
$class X : model M {};
    GetQualifiedName()= X
    HaveSomethingToRegister()
    GenerateReadBarriers()
    WriteInsertionExtractionInTermsOfSerialise()
    HasDefaultCtor()
    WriteTraits()
    AutoGenerateInsertionToXostream()
    m_mixinBaseClass= X_model_mixin<ceda::BaseMixin<X,ceda::EmptyBase> >
    m_inheritsModel
    m_referencedModel != nullptr
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()

$model M {};
$mixin X : model M {};
    GetQualifiedName()= X
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()
    GenerateReadBarriers()
    HasDefaultCtor()
    WriteTraits()
    IsUnderlyingStruct()
    IsMixin()
    m_inheritsModel
    m_referencedModel != nullptr
    m_mixinBaseClass= X_model_mixin<BC>

///////////////////////// mixin model
$class X : mixin [model{}] {};
    GetQualifiedName()= X
    HasDefaultCtor()
    WriteTraits()
    m_mixins.size()= 1
    m_mixinBaseClass= X_Mixin_0_model_mixin<ceda::BaseMixin<X,ceda::EmptyBase> >

$mixin X : mixin [model{}] {};
    GetQualifiedName()= X
    HasDefaultCtor()
    WriteTraits()
    IsUnderlyingStruct()
    IsMixin()
    m_mixins.size()= 1
    m_mixinBaseClass= X_Mixin_0_model_mixin<BC >

///////////////////////// refd mixin model
$model M {};
$class X : mixin [M] {};
    GetQualifiedName()= X
    HasDefaultCtor()
    WriteTraits()
    m_mixins.size()= 1
    m_mixinBaseClass= X_Mixin_0_model_mixin<BC >

$model M {};
$mixin X : mixin [M] {};
    GetQualifiedName()= X
    HasDefaultCtor()
    WriteTraits()
    IsUnderlyingStruct()
    IsMixin()
    m_mixins.size()= 1
    m_mixinBaseClass= X_Mixin_0_model_mixin<BC >

///////////////////////// nested model
$class X { $model{} };
    GetQualifiedName()= X
    HaveSomethingToRegister()
    HasEmbeddedModel()
    CanSerialiseEmbeddedModel()
    WriteInsertionExtractionInTermsOfSerialise()
    HasDefaultCtor()
    WriteTraits()
    m_numModelSchema= 1

$mixin X { $model{} };
    GetQualifiedName()= X
    HasEmbeddedModel()
    CanSerialiseEmbeddedModel()
    HasDefaultCtor()
    WriteTraits()
    IsUnderlyingStruct()
    IsMixin()
    m_numModelSchema= 1
    m_mixinBaseClass= BC

///////////////////////// no model IObject
$class X isa ceda::IObject {};
    GetQualifiedName()= X
    HaveSomethingToRegister()
    ImplementsIObject()
    HasDefaultCtor()
    WriteTraits()
    m_implementsIObject
    m_interfaces.size()= 1
    m_mixinBaseClass= ceda::IObjectBaseMixin<X,ceda::EmptyBase>

$mixin X isa ceda::IObject {};
    error : 'isa' prohibited on a $mixin

///////////////////////// no model IPersistable
$class X isa ceda::IPersistable {};
    GetQualifiedName()= X
    HaveSomethingToRegister()
    EffectivelyRegistered()
    m_reflected
    m_registered
    ImplementsIObject()
    ImplementsIPersistable()
    CanSerialiseEmbeddedModel()
    HasDefaultCtor()
    WriteTraits()
    m_implementsIPersistable
    m_implementsIObject
    m_interfaces.size()= 1
    m_mixinBaseClass= ceda::PersistableMixin<ceda::IObjectBaseMixin<X,ceda::EmptyBase> >

///////////////////////// pure model
$model M {};
    GetQualifiedName()= M
    HaveSomethingToRegister()
    WriteInsertionExtractionInTermsOfSerialise()
    HasDefaultCtor()
    WriteTraits()
    AutoGenerateInsertionToXostream()
    IsUnderlyingStruct()
    IsPureModel()
    HasEmbeddedModel()
    CanSerialiseEmbeddedModel()
    m_numModelSchema= 1

///////////////////////// base model and mixin model
$class X : model{} mixin [model{}] {};
    GetQualifiedName()= X
    HaveSomethingToRegister()
    HasEmbeddedModel()
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()
    GenerateReadBarriers()
    CanSerialiseEmbeddedModel()
    WriteInsertionExtractionInTermsOfSerialise()
    HasDefaultCtor()
    WriteTraits()
    AutoGenerateInsertionToXostream()
    m_inheritsModel
    m_numModelSchema= 1
    m_mixins.size()= 1
    m_mixinBaseClass= X_Mixin_0_model_mixin<X_model_mixin<ceda::BaseMixin<X,ceda::EmptyBase> > >

$class X : model{} mixin [model{}model{}] {};
    GetQualifiedName()= X
    HaveSomethingToRegister()
    HasEmbeddedModel()
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()
    GenerateReadBarriers()
    CanSerialiseEmbeddedModel()
    WriteInsertionExtractionInTermsOfSerialise()
    HasDefaultCtor()
    WriteTraits()
    AutoGenerateInsertionToXostream()
    m_inheritsModel
    m_numModelSchema= 1
    m_mixins.size()= 2
    m_mixinBaseClass= X_Mixin_1_model_mixin<X_Mixin_0_model_mixin<X_model_mixin<ceda::BaseMixin<X,ceda::EmptyBase> > > >

$class X isa ceda::IObject : model{} mixin [model{}model{}] {};
    GetQualifiedName()= X
    HaveSomethingToRegister()
    ImplementsIObject()
    HasEmbeddedModel()
    UsingModelBaseClass()
    QualifyModelFieldsWithUnderscores()
    GenerateReadBarriers()
    CanSerialiseEmbeddedModel()
    WriteInsertionExtractionInTermsOfSerialise()
    HasDefaultCtor()
    WriteTraits()
    AutoGenerateInsertionToXostream()
    m_implementsIObject
    m_inheritsModel
    m_numModelSchema= 1
    m_interfaces.size()= 1
    m_mixins.size()= 2
    m_mixinBaseClass= X_Mixin_1_model_mixin<X_Mixin_0_model_mixin<X_model_mixin<ceda::IObjectBaseMixin<X,ceda::EmptyBase> > > >

$class X : mixin[M1 M2] {};
    GetQualifiedName()= X
    HasDefaultCtor()
    WriteTraits()
    m_mixins.size()= 2
    m_mixinBaseClass= M2<M1<ceda::BaseMixin<X,ceda::EmptyBase> > >
*/

/*
namespace ceda
{
    $interface+ IPersistable : IObject {};
}
$class X isa ceda::IPersistable {};
*/


@if (true)
{

@def mTrace(x) = Tracer() << @str(x = ) << x << '\n';

/*
Proposal
--------

$struct X : model {...} {...};
    not reflected
    DataSourceReadBarrier() / DataSourceWriteBarrier()

$struct X isa IObject : model {...} {...};
    not reflected
    DataSourceReadBarrier() / DataSourceWriteBarrier()
    
$struct X isa IPersistable : model {...} {...};
    not reflected
    DataSourceReadBarrier() / DataSourceWriteBarrier()

$struct+ X : model {...} {...};
    reflected and registered
    IndepFieldReadBarrier() / IndepFieldWriteBarrier()

$struct+ X isa IObject : model {...} {...};
    reflected and registered
    IndepFieldReadBarrier() / genop_xxx()

What matters
    -   hasModel
    -   implementsIObject
    -   reflected
    -   registered

    if (reflected && implementsIObject)
    {
        IndepFieldReadBarrier() / genop_xxx()
    }
    else
    {
        // The old system with all its limitations
        DataSourceReadBarrier() / DataSourceWriteBarrier() 
    }
*/

namespace DepGraphTest1
{
    $model+ M
    {
        // xmap not supported in IObject
        //xmap<ceda::xstring,int64> g;
        int32 x1[];
        int32 x2;
    };
    
    $struct X <<reflect>> isa ceda::IObject
        :
        model
        {
            M m;
            int32 x3;
        }
    {
        $dep int32 y1 =
        {
            auto const& x1 = m.x1.read();
            y1 = x1[0] + x1[1] + x1[2] + m.x2;
            
            //y1 = m.x1 + m.x2;
			Tracer() << "[Calc y1]";
        };

        $dep int32 y2 =
        {
            y2 = y1 + x3 + m.x1[0];
			Tracer() << "[Calc y2]";
        };
    };

    void Run()
    {
        ceda::TraceGroup g("DepGraph example 1");

        ceda::CSpaceCreator cspace;
        ceda::SetMilliSecsPerGc(100);
        ceda::SetMaxDgsTotalByteSize(ceda::DgsEvictionQueueIndex_SystemMemory, 0);

        X ds;
        {
            ceda::CSpaceTxn txn;

            ds.m.x1[0] = 10;
            ds.m.x1[1] = 20;
            ds.m.x1[2] = 30;
            ds.m.x2 = 200;
            ds.x3 = 500;
            mTrace(ds.y1)
            mTrace(ds.y1)
            mTrace(ds.y2)
            mTrace(ds.y2)

            ds.m.x1[0] = 1000;
            mTrace(ds.y2)
        }

        {
            ceda::CSpaceTxn txn;
            ds.EvictDgsNodes();
        }
    }
}

namespace DepGraph2
{
    void Run()
    {
        DepGraphTest1::Run();
    }
}
}
@else
{
    namespace DepGraph2
    {
        void Run()
        {
        }
    }
}
