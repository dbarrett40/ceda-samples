// Variables.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2013

@import "Ceda/cxObject/PrintReflectedVariable.h"
@import "Ceda/cxObject/Object.h"
@import "ExampleUtils.h"

namespace Variables1
{
    $var+ int32 x = 0;

	void Run()
	{
		ceda::TraceGroup g("Variables example 1");

        ceda::TracerX os;
        ceda::PrintReflectedGlobalVariable(os, ceda::FindReflectedGlobalVariable("Variables1::x"));
        os << '\n';
	}	
}

namespace Variables
{
    void Run()
    {
        Variables1::Run();
    }
}

