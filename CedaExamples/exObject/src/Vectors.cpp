// Vectors.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "Ceda/cxObject/Object.h"
@import "Ceda/cxObject/IObjectVisitor.h"
@import "Ceda/cxObject/PrintReflectedVariable.h"
@import "Ceda/cxObject/WCSpace.h"
@import "ExampleUtils.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
// Vectors1
/*
The following example demonstrates how it is possible to define a vector type within a data source,
and the functions that can be called to insert, delete and perform moves on the data model field.

The cxObject framework provides a hook for allowing operations on the data source to be recorded.
In this example, the hook hasn't been initialised so no recording of the operations is performed.
*/
namespace Vectors1
{
    $struct+ X isa ceda::IObject :
        model
        {
            ceda::xvector<ceda::int32> L;
        }
    {
    };
    
    void Show(ceda::ConstStringZ msg, X& x)
    {
        Tracer() << msg << '\n';
        PrintInstance(Tracer(), &x);
    }

	void Run()
	{
		ceda::TraceGroup g("Vectors example 1");

        X x;
        Show("Initial state",x);
        
        ceda::int32 buffer[] = { 1,2,3,4,5,6,7,8,9,10 };
        x.L.insert(0, buffer,10);
        Show("After insert",x);
        
        x.L.erase_range(1,3);
        Show("After delete",x);

        /* todo asserts
        // Move
        {
            MoveBuffer<ceda::int32> m;
            
            x.L.extract(6,8,m);
            Show("After extract",x);    

            x.L.insert(0,m);
            Show("After move",x);
        }
        */
        
        // Assignment to an element isn't currently possible.  If required it could perhaps be
        // implemented in terms of insert/erase operations
        //x.L[0] = 100;
        //Show("After assignment to element",x);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Vectors2
/*
This example demonstrates an alternative syntax provided for accessing the vector member
*/
namespace Vectors2
{
    $struct+ X isa ceda::IObject :
        model
        {
            ceda::xvector<ceda::int32> L;
        }
    {
    };
    
    void Show(ceda::ConstStringZ msg, X& x)
    {
        Tracer() << msg << '\n';
        PrintInstance(Tracer(), &x);
    }

	void Run()
	{
		ceda::TraceGroup g("Vectors example 2");

        X x;
        Show("Initial state",x);
        
        ceda::int32 buffer[] = { 1,2,3,4,5,6,7,8,9,10 };
        x.CreateIntoL(0,buffer,10);
        Show("After insert",x);
        
        x.DeleteFromL(1,3);
        Show("After delete",x);

        /*
        todo : asserts
        // Move
        {
            MoveBuffer<ceda::int32> m;
            
            x.RecordExtractFromL(6,8,m);
            Show("After extract",x);

            x.MoveIntoL(0,m);
            Show("After move",x);
        }
        */
	}
}

namespace Vectors3
{
    $struct+ X isa ceda::IObject {};
    
    $struct+ Y isa ceda::IObject :
        model
        {
            ceda::xvector<X*> L;
        }
    {
    };
    
	void Run()
	{
		ceda::TraceGroup g("Vectors example 3");

        ceda::CSpaceCreator cspace;
        ceda::CSpaceLock lock;

        Y* y = $new Y;
        y->L.push_back($new X);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace Vectors
{
    void Run()
    {
        Vectors1::Run();
        Vectors2::Run();
        Vectors3::Run();
    }
}






