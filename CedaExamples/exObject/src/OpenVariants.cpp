// OpenVariants.cpp
//
// Author David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2011

@import "Ceda/cxObject/OpenVariant.h"
@import "ExampleUtils.h"
@import "ValidateSerialisation.h"
#include "Ceda/cxUtils/Tracer.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
Note that objects in open variants must be reflected
*/
namespace OpenVariants1
{
    $interface+ Ix : ceda::IObject
    {
        void foo();
    };
    
    $struct+ X isa Ix :
        model
        {
            ceda::int32 I;
            ceda::xstring S;
        }
    {
        X() {}
        X(ceda::int32 i, const ceda::xstring& s) { _model_.I = i; _model_.S = s; }
        void foo() {}
    };

    $struct+ Y isa Ix :
        model
        {
            ceda::float64 F;
        }
    {
        Y() {}
        Y(ceda::float64 f) { _model_.F = f; }
        void foo() {}
    };

    @def mExamples =
    {
        [
            X(22, "Hello"),
            X(23, "Hello"),
            X(22, "Hi"),
            Y(3.14),
            Y(4.0)
        ]
    }

	void Run()
	{
		ceda::TraceGroup g("OpenVariants example 1");

        @for (v in mExamples)
        {
            {
                ceda::openvariant<Ix> p1(v);
                ceda::openvariant<Ix> p2(p1);
                ceda::openvariant<Ix> p3;
                p3 = p1;
                ceda::openvariant<ceda::IObject> p4;
                p4 = p3;
                
                Tracer() << "p1 = " << p1 << '\n';
                Tracer() << "p2 = " << p2 << '\n';
                Tracer() << "p3 = " << p3 << '\n';
                Tracer() << "p4 = " << p4 << '\n';
                
                ValididateSerialisation(p1);
            }
        }

        @for (v1 in mExamples)
        {
            @for (v2 in mExamples)
            {
                {
                    ceda::openvariant<Ix> p1(v1);
                    ceda::openvariant<Ix> p2(v2);
                    
                    ceda::TracerX os;
                    os << p1 << ' ' << p2 << " : ";
                    if (p1 == p2) os << " == ";
                    if (p1 != p2) os << " != ";
                    if (p1 < p2) os << " < ";
                    if (p1 > p2) os << " > ";
                    if (p1 <= p2) os << " <= ";
                    if (p1 >= p2) os << " >= ";
                    os << '\n';
                }
            }
        }

        // Demonstrate serialisation of a xvector<openvariant<Ix> >
        {
            Tracer() << "\n";
            ceda::xvector<ceda::openvariant<Ix>> L;
            @for (v in mExamples)
            {
                L.push_back(v);
                
            }
            ValididateSerialisation(L);
        }
	}	
}

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace OpenVariants
{
    void Run()
    {
        OpenVariants1::Run();
    }
}

