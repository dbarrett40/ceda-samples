// Metadata.cpp
//
// Author Jesse Pepper, David Barrett-Lennard
// (C)opyright Cedanet Pty Ltd 2007

@import "ExampleUtils.h"
@import "Ceda/cxObject/Object.h"

/*
Xcpp uses the macro expander's expression parser to read literals (strings, bools and numbers) 
in the meta data.  That means you can do things like this:

    $typedef int32 a : ["a"+"b", 4+2*5, 2*3 > 4  ];

which is equivalent to 

    $typedef int32 a : ["ab", 14, true ];

The macro expander has no concept of wide versus narrow strings. Literal strings are always 
recorded as UTF-8 strings.
*/

/*
///////////////////////////////////////////////////////////////////////////////////////////////////
// Metadata Grammar

Xcpp allows metadata to be included as part of the reflection information stored for global 
variables, typedefs, enums, structs, interfaces, methods, functions and their arguments and return
types. 

Metadata is stored as bytecodes (in an array of bytes) that allows arbitrarily complicated metadata
to be applied, while maintaining a very small memory footprint and can be read (or decoded) at high
speed. Xcpp converts the metadata to C-style constant arrays of bytes which are compiled quickly by
the C++ compiler. They are then stored in the initialised memory segment for the process.

Xcpp metadata takes the form of a colon (:) followed by a metadata-list as defined by:

metadata-element:
    metadata-list
    metadata-functor
    literal-expression 

literal-expression:
    (as defined by the cxMacroExpander.dll's ExpressionParser, but must evaluate to one of:
    string-literal
    integer-literal
    bool-literal
    float-literal
    I.e. void & int64 are not allowed)

metadata-functor:
    identifier
    identifier( [metadata-element *,] )

metadata-list:
    [[ [metadata-element *,] ]]

The following example illustrates the various possibilities that the Xcpp metadata grammar 
provides, however this does not indicate a recommended convention for creating metadata, a further
example will suggest such a convention.
*/

namespace Metadata1
{
    $functor <<register>> void foo() : ['x'];
    
    //a list with a single string element
    $typedef+ int32 a : ["text"];
    
    // an integer
    $typedef <<register>> int32 b1 : [7];
        
    // a double precision floating point value
    $typedef <<reflect>> int32 c : [2.718];
      
    // a boolean
    $typedef+ int32 d : [false];
    
    // lists are comma separated and hetrogeneous
    $typedef+ int32 e : [3, 7.5, "abc"];
    
    // a functor with no arguments can be written in two equivalent ways (identical bytecode is produced)
    $typedef+ int32 f1 : [hex];
    $typedef+ int32 f2 : [hex()];
    
    // arguments of a functor are just metadata-elements just like in a metadata-list
    $typedef+ int32 g : [ftor(3, 7.5, "abc")];
    
    // metadata-lists can be metadata-elements (composition)
    $typedef+ int32 h : ["text", 7, 2.718, false, hex, ftor(3, 7.5, "abc"), [3, 7.5, "abc"]];
    
    // functors can also compose, and take lists as arguments
    $typedef+ int32 i : [ 
                            ftor
                            (
                                7,
                                2.718,
                                false,
                                ftor(3, 7.5, "abc"),
                                [3, 7.5, "abc", ftor( ftor2("xyz",abc) )],
                            )
                        ];

    // Literals can be entered as expressions that expand to a bool
    $typedef+ int32 j:[true && (len("abc") == 3)];
    
    // an integer
    $typedef+ int32 k : [4*1024];
    
    // a string
    $typedef+ int32 l : ["hello" + " world\n"];
    
    // a double
    $typedef+ int32 : [0 + cos(3.1415926535897932384626433832795)]m; //* sin(3.1415926535897932384626433832795/2)];

    void Run()
    {
        ceda::TraceGroup g("Metadata example 1");
    }	
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Metadata Placement
/*
As shown in the previous example, the Xcpp metadata grammar allows arbitrarily complex metadata.
This example illustrates the various constructs that allow metadata to be added.  It should be
understood that although simple metadata is applied, such as a list containing only a functor with
a name indicating what the metadata applies to.  For example the typedef T0 has a list containing
a single functor with no arguments called on_T0.
*/
namespace Metadata2
{
    $typedef+ int32 T0 : [on_T0];
    
    $enum+ E0 : [on_E0]
    {
        val0,
        val1
    };
    	
	$struct S0;        // can't go here
	$interface I0;     // or here
	
	$struct+ S0 : [on_S0]
	{
	   $int32 v0 : [on_v0];
	   	   
	   ceda::int32 v1;  // can't go here, it's not reflected
	   void fn1();      // can't go here, only on reflected functions
	};
	
	$class+ C0 : [here]
	{
	   $int32 v0 : [on_v0];
	   	   
	   ceda::int32 v1;  // only on reflected members
	   void fn1();      // not on methods
	};

    // All methods and attributes in an interface are reflected, so they can have metadata 
    // applied to them
	$interface+ I0 : [on_I0]
	{
	    ceda::int32 a : [on_a];
	    void fn ( int32 a0 : [on_a0], int32 a1 : [on_a1] ) : [on_fn];
	};
	
	$var+ float64 pi : [on_pi];
    
    // metadata can be applied to pointers and the values they point at
    $var+ ceda::float64 : [on_the_float64] * : [on_the_pointer] v0;
    $var+ ceda::float64 ** : [on_the_float64_ptr_ptr] * v1;
    $function+ void F0() : [on_F0] {}
    $function+ ceda::float64 F1(int32 arg) { return 0; }
    
    $var+ xvector<ceda::int32 : [on_elements]> v2 : [blah];
	
	// to structs with the same name in different namespaces
    namespace ns1 // : [on_ns1] error! namespaces can't have metadata
    {
	    $struct Point3 
	    {
	    };
    	
	    namespace ns1a
	    {
		    $struct+ Point3 : [on_ns1_ns1a_Point3] 
		    {
		    };
	    }
    }
    namespace ns2
    {
        $struct+ Point3 : [on_ns2_Point3]
        {
        };
    }
	
	void Run()
	{
		ceda::TraceGroup g("Metadata example 2");
	}	
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Metadata Conventions
/*
This example suggests a convention to be used when applying metadata with the Xcpp frontend.
Essentially, the idea is to represent metadata as a set of name-value pairs, where the name
is a functor and the value is the argument to that functor.

Xcpp produces bytecode to describe reflected constructs.  The class ReadMetaData defined in
Ceda/cxObject/ReadMetaData.h and its use in PrintClass.cpp are good examples of how metadata
bytecode can be consumed.
*/
namespace Metadata3
{
    // T0 has 3 name-value pairs for metadata, with a functor, a string and an integer value
    $typedef+ int32 T0 : [name0(value0), name1("value1"), name2(0xfeee)];
    
    // Functor names should use lowerCamelCase (see CamelCase in wikipedia) for functor names
    $typedef+ int32 T1 : [longFunctorName];

    // At times name-value pairs will have multiple values as such as:
    // TODO should ranges be half open intervals? range(0,12)?
    $typedef+ int32 MonthNum : [range(0,11)];
    
    // other times, a list is more appropriate
    $var+ int32 Zoom : [values([1,2,4,8,16,32])];
    
    // Metadata on typedefs can be used to provide 
    $typedef+ int32 x : [defaultValue(10)];
    
    // Metadata can reference external sources to obtain further information, for example
    // here we have an enum that can reference an ambient settings area to get the strings
    // for its possible values.  This has benefits such as allowing internationalisation
    // of these strings.    
    // TODO EDay vs Day, I vote for Day, because you don't go TWeekday in the typedef below
    $enum+ Day : [values("settings/general/enums/Day")]
    {
        dowMonday,
        dowTuesday,
        dowWednesday,
        dowThursday,
        dowFriday,
        dowSaturday,
        dowSunday        
    };    
    
    // Typedefs should be used often to provide a rich set of types
    $typedef+ int32 DayOfWeekNum : [range(0,6)];
    $typedef+ Day Weekday : [valid([dowMonday,dowTuesday,dowWednesday,dowThursday,dowFriday])];
    $typedef+ Day WeekendDay : [valid([dowSaturday,dowSunday])];
    
    // Tag style values should use functor names rather than strings as their tag name:
    $var+ int32 y2 : [editor(slider)];
    
    // While human readable text should be a string
    $function+ void DoSomethingQuiteSpecial() : [helpText("Does something quite special")]
    {
        //...
    }
    
    //list has functorName(value)
    // functor names like local variables start with lower case
    // capitalise subsequent words to show word breaks
    // treat acronyms as words, with only first letter capitalised (Gui not GUI)
    //   reason being that these acronyms should be learnt as though they are words,
    //   or they should not be acronyms
    //$typedef int32 x : [displayString("this is X"),unit("pixels"),hideInGui];
    
    // Metadata can be used to tag using a name (functor) with no value, for example:
    $struct+ UserBad
    {
        $string8 name;
        $string8 password : [hidden];
    };
    
    // Prefer using typedefs to using metadata tags, it's more concise
    $typedef+ string8 Password;
    
    $struct+ User
    {
        $string8 name;
        $Password password;
    };
        
    $typedef+ string8 RefByName;
    // rather than
    $var+ string8 f1 : [refByName];
    
    // Prefer typedefs for types close to the problem domain than generic abstract types
    // for example, prefer:
    $typedef+ string8 Comment;
    // rather than:
    $typedef+ string8 MultiLineString;
    
    // TODO this section needs some clarification.  I'm unsure whether it's best to suggest that
    // quantities are named after the unit or the english word.  For example, should a variable be
    // of type MetresPerSecond or Velocity.  If it's Velocity, should its units be implied or somehow
    // need to keep being specified.  How about having a default, and it's always stored underlying
    // in the defualt way, and displayed in a way similar to file sizes in windows (it uses KB, MB,
    // GB etc).  This wouldn't work well perhaps if astrophysicists stored distances in Metres when
    // they really wanted light years, but I guess then they could define a type like AstroDistance,
    // or perhaps use just LightYear

    $typedef+ float64 Metres : [unitStr("m")];
    
    // $typedef float64 Kilometre : [unitStr("km"), unit(prod(1000,Metre))];  would this be handled automatically in printing a metre?
    $typedef+ float64 LightYear : [unitStr("LightYrs"), unit(prod(9.5e12,Metre))];
    $typedef+ float64 Angstrom : [unitStr("A"), unit(div(Metre,1e10))];  // "�"
    $typedef+ float64 Second : [unitStr("s")];
    $typedef+ float64 Foot : [unitStr("\'"), unit(prod(0.3048,Metre))];
    $typedef+ float64 MetrePerSecond : [unitStr("m/s"), unit(div(metre,second))];
    $typedef+ float64 MetrePerSecondSquared : [unitStr("m/s^2"), unit(div(metre,prod(second,second)))];
    $typedef+ float64 LightSpeed : [unitStr("LightSpd"), unit(prod(3e8,MetrePerSecond))];
    
    // baseUnit means the unit it's stored in, but display will dynmaically adjust, either using
    // significant figures or km, mm etc.
    $typedef+ float64 Time : [baseUnit(Second)];
    $typedef+ float64 Distance : [baseUnit(Metre)];
    $typedef+ float64 ImperialDestance : [baseUnit(Foot)];
    $typedef+ float64 AstroDistance : [baseUnit(LightYear)];
    $typedef+ float64 AtomicDistance : [baseUnit(Angstrom)];
    $typedef+ float64 Velocity : [baseUnit(MetrePerSecond)];
    $typedef+ float64 AstroVelocity : [baseUnit(LightSpeed)];
    $typedef+ float64 Acceleration : [baseUnit(MetrePerSecondSquared)];
    
    $var+ Acceleration g = 9.8;
    
    $enum+ LineStyle : [values("settings/drawing/enums/LineStyle")]
    {
        lsNone,
        lsSolid,
        lsDashed,
        lsDotted,
        lsDotDash
    };

    $interface+ IStartable
    {
        void Start();
        bool isStarted : [readOnly];
    };
    
    $interface+ ICdPlayer
    {
        float64 m_volume : [editor(slider)];
    };
	
	void Run()
	{
		ceda::TraceGroup g("Metadata example 3");
	}	
}

/*
    $interface+ ICDPlayer : [release(2010,7,23)]
    {
        void Play(int32:[range(0,10*10)] speed);
    };

    $typedef+ int32 : [here] * : [here] Pointer;
    
    $function+ float64:[unit("m/s")] GetVelocity( float64:[unit("m")] s,  float64:[unit("s")] a )
         : [description("Calculate velocity when a stationary object accelerates at a for displacement s")]
    { 
        return sqrt(2*a*s);
    }
*/         

///////////////////////////////////////////////////////////////////////////////////////////////////
namespace Metadata
{
    void Run()
    {
        Metadata1::Run();
        Metadata2::Run();
        Metadata3::Run();
    }
}
