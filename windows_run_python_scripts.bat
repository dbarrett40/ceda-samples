@ECHO OFF

:: This script assumes the pyceda and pypizza modules have been installed

:: SOURCE = path to the ceda-samples root folder
SET PATH_TO_THIS_BATCH_FILE=%~dp0
SET SOURCE=%PATH_TO_THIS_BATCH_FILE%

:: SCRIPTS = path to where the python scripts are located
SET SCRIPTS=%SOURCE%\MyCompany\python

SET BUILD=%SOURCE%\..\_build\ceda-samples
MKDIR %BUILD%
CD %BUILD%

ECHO --------------- datetime_example.py --------------------
python %SCRIPTS%\datetime_example.py
IF errorlevel 1 GOTO:OnFailure

ECHO --------------- shapes_example.py --------------------
python %SCRIPTS%\shapes_example.py
IF errorlevel 1 GOTO:OnFailure

ECHO --------------- open_and_close_database_example.py --------------------
python %SCRIPTS%\open_and_close_database_example.py
IF errorlevel 1 GOTO:OnFailure

ECHO --------------- pizza_example.py --------------------
python %SCRIPTS%\pizza_example.py
IF errorlevel 1 GOTO:OnFailure

ECHO -----------------------------------
ECHO All examples ran.

PAUSE
GOTO:EOF

::-------------------------------------------------------------------------------------------------
:OnFailure
	ECHO.
	ECHO ERROR. Terminating batch file. errorlevel=%errorlevel%
    PAUSE
GOTO:EOF

