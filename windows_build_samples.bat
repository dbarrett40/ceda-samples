@ECHO OFF

SET CEDA_SDK_INSTALL_DIR=%ProgramFiles%\Ceda
SET VCVARS64=%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat
SET VCVARS32=%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars32.bat
SET NDK=%LOCALAPPDATA%\Android\Sdk\ndk-bundle

SET WINDOWS_COMPILER="cl.exe"
::SET WINDOWS_COMPILER="C:/Program Files/LLVM/bin/clang-cl.exe"

:: Directory containing the root CMakeLists.txt
SET PATH_TO_THIS_BATCH_FILE=%~dp0
SET SOURCE=%PATH_TO_THIS_BATCH_FILE%
SET BUILD=%SOURCE%..\_build\ceda-samples

SET ENABLE_WIN32=1
SET ENABLE_WIN64=1
SET ENABLE_ANDROID=1

SET ENABLE_RELEASE=1
SET ENABLE_DEBUG=1
SET ENABLE_SHARED=1
SET ENABLE_STATIC=1

::RMDIR /S /Q "%BUILD%"

CALL "%CEDA_SDK_INSTALL_DIR%\scripts\build_ceda_projects.bat" Build
PAUSE

