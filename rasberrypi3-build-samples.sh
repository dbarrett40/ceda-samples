#!/bin/bash

PATH_TO_FOLDER_CONTAINING_THIS_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SOURCE=$PATH_TO_FOLDER_CONTAINING_THIS_SCRIPT

CMAKEBIN=~/cmake/cmake-3.15.2/bin
BUILD=$SOURCE/../build/ceda-samples
SHARED_LIBS=ON
PLATFORM=linux-amv7l
LIBTYPE=shared  #static
#CONFIG=Debug  
CONFIG=RelWithDebInfo

FOLDER=$PLATFORM-$LIBTYPE-$CONFIG

echo "SOURCE=$SOURCE"
echo "BUILD=$BUILD"
echo "FOLDER=$FOLDER"

rm -rf $BUILD/$FOLDER

mkdir -p $BUILD/$FOLDER
cd $BUILD/$FOLDER

echo "-----------------------------------------------------------------------------"
echo "Running cmake configure ..."

$CMAKEBIN/cmake -G "Ninja" \
        -DCEDA_BUILD_SDK=ON \
        -DBUILD_SHARED_LIBS=$SHARED_LIBS \
        -DCMAKE_INSTALL_PREFIX:PATH="install" \
        -DCMAKE_CXX_STANDARD=17 \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_BUILD_TYPE=$CONFIG \
        -DCMAKE_MAKE_PROGRAM=ninja \
        --config $CONFIG \
        $SOURCE

echo "-----------------------------------------------------------------------------"
echo "Running cmake build and install ..."
#ninja -j1

$CMAKEBIN/cmake --build . --target install --config $CONFIG

#echo "-----------------------------------------------------------------------------"
#echo "Running cpack..."
#$CMAKEBIN/cpack -C $CONFIG

cd $SOURCE
