@ECHO OFF

:: Allow for selecting 1 of the 8 possible build variants
:: You can select debug/release,  shared/static,  x86/x64
SET DEBUG=1
SET SHARED=1
SET X64=1

:: This is where visual studio builds the files according to CMakeSettings.json
:: (note it is not where window_build_samples.bat puts them)

SET PATH_TO_THIS_BATCH_FILE=%~dp0

:: E.g. C:\cedanet\repos\ceda-samples\..\..\build-ceda-samples = C:\cedanet\build-ceda-samples
SET BUILD=%PATH_TO_THIS_BATCH_FILE%\..\..\build-ceda-samples

:: Location where the CEDA SDK is installed
SET CEDA_SDK=%ProgramFiles%\Ceda

IF %X64% EQU 1 (
    IF %DEBUG% EQU 1 (
        IF %SHARED% EQU 1 (
            SET "PATH=%CEDA_SDK%\windows-x64-debug\bin;%PATH%"
            CD "%BUILD%\windows-x64-debug\bin
        ) ELSE (
            SET "PATH=%CEDA_SDK%\windows-x64-static-debug\bin;%PATH%"
            CD "%BUILD%\windows-x64-static-debug\bin
        )
    ) ELSE (
        IF %SHARED% EQU 1 (
            SET "PATH=%CEDA_SDK%\windows-x64\bin;%PATH%"
            CD "%BUILD%\windows-x64\bin
        ) ELSE (
            SET "PATH=%CEDA_SDK%\windows-x64-static\bin;%PATH%"
            CD "%BUILD%\windows-x64-static\bin
        )
    )
) ELSE (
    IF %DEBUG% EQU 1 (
        IF %SHARED% EQU 1 (
            SET "PATH=%CEDA_SDK%\windows-x86-debug\bin;%PATH%"
            CD "%BUILD%\windows-x86-debug\bin
        ) ELSE (
            SET "PATH=%CEDA_SDK%\windows-x86-static-debug\bin;%PATH%"
            CD "%BUILD%\windows-x86-static-debug\bin
        )
    ) ELSE (
        IF %SHARED% EQU 1 (
            SET "PATH=%CEDA_SDK%\windows-x86\bin;%PATH%"
            CD "%BUILD%\windows-x86\bin
        ) ELSE (
            SET "PATH=%CEDA_SDK%\windows-x86-static\bin;%PATH%"
            CD "%BUILD%\windows-x86-static\bin
        )
    )
)

ECHO -------------------------------------------------------------------------------
ECHO PATH %PATH%
ECHO Running examples in directory %CD%

rem exMiddleware
rem exBlobSet
rem exRmi

exLss
IF errorlevel 1 GOTO:OnFailure
exObject
IF errorlevel 1 GOTO:OnFailure
exCedaScript
IF errorlevel 1 GOTO:OnFailure
exPersistStore
IF errorlevel 1 GOTO:OnFailure
exOperation
IF errorlevel 1 GOTO:OnFailure
exPython
IF errorlevel 1 GOTO:OnFailure
exPython2
IF errorlevel 1 GOTO:OnFailure
exThread
IF errorlevel 1 GOTO:OnFailure

ECHO -------------------------------------------------------------------------------
ECHO All examples have been run

PAUSE
GOTO:EOF

::-------------------------------------------------------------------------------------------------
:OnFailure
	ECHO.
	ECHO ERROR. Terminating batch file. errorlevel=%errorlevel%
    PAUSE
GOTO:EOF
