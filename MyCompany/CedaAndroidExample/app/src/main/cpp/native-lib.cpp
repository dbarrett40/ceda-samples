#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL Java_com_cedanet_cedaandroidexample_MainActivity_stringFromJNI(
    JNIEnv *env,
    jobject /* this */)
{
    return env->NewStringUTF("hello world");
}
