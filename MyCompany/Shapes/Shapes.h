// Shapes.h

@import "Ceda/cxPersistStore/Models.h"

CEDA_DEFINE_PROJECT_API_MACRO

namespace geom
{
    $model+ TPoint
    {
        float32 X;
        float32 Y;
    };

    $model+ TPolygon
    {
        xvector<TPoint> V;
    };

    $model+ TCircle
    {
        float32 Radius;
    };

    $model+ TRectangle
    {
        float32 Width;
        float32 Height;
    };

    $variant+ TShape
    {
        default TCircle(200);
        TCircle Circle;
        TRectangle Rectangle;
        TPolygon Polygon;
    };
}
