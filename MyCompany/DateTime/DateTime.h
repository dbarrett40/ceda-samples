// DateTime.h

@import "Ceda/cxPersistStore/Models.h"

CEDA_DEFINE_PROJECT_API_MACRO

namespace dt
{
    $enum+ class TMonth
    {
        Jan,
        Feb,
        Mar,
        Apr,
        May,
        Jun,
        Jul,
        Aug,
        Sep,
        Oct,
        Nov,
        Dec
    };

    $model+ TDate <<-os>>
    {
        int32 Year;
        TMonth Month;   // 1-12
        int32 Day;      // 1-31
    };

    inline ceda::xostream& operator<<(ceda::xostream& os, const TDate& d)
    {
        os << ceda::setfill('0') 
           << d.Month << ' ' 
           << ceda::setw(2) << d.Day << ' ' 
           << ceda::setw(4) << d.Year;
        return os;
    }

    $model+ TTime <<-os>>
    {
        int32 Hour;     // 0-23
        int32 Minute;   // 0-59
        int32 Second;   // 0-59
    };

    inline ceda::xostream& operator<<(ceda::xostream& os, const TTime& t)
    {
        os << ceda::setfill('0') 
           << ceda::setw(2) << t.Hour << ':' 
           << ceda::setw(2) << t.Minute;
        return os;
    }

    $model+ TDateTime <<-os>>
    {
        TDate Date;
        TTime Time;
    };
    
    inline ceda::xostream& operator<<(ceda::xostream& os, const TDateTime& dt)
    {
        os << dt.Date << ' ' << dt.Time; 
        return os;
    }
} // namespace dt
