// Test.cpp

#include "MyCompany/Pizza/Pizza.h"
#include "Ceda/cxUtils/HPTime.h"
#include "Ceda/cxUtils/CRTDebug.h"
#include "Ceda/cxUtils/Tracer.h"
#include <iostream>
#include <map>

void ShapeTest();

int main(int argc, char* argv[])
{
    Register_Project_Pizza();

    ceda::EnableMemoryLeakDetection();
    ceda::SetTraceFile("stdout", false, false);

    Tracer() << "Hello world\n";
    ShapeTest();

    ceda::DestroyTheTraceFile();
    return 0;
}
