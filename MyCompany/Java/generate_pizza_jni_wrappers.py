import sys
import pyceda
import pypizza
import pyceda.generate_jni

if len(sys.argv) < 3:
    print("no command line arguments provided")
    sys.exit()    

# Location for where to write the java files
javaFolderPath = sys.argv[1]

# Location for where to write the cpp files
cppFolderPath = sys.argv[2]

# includes paths which are inserted into every cpp file
projectIncludes = [
    'Ceda/cxJava/Utils.h',
    'MyCompany/Pizza/PizzaDeliveries.h'
]

# Mapping from C++ namespace to Java package name
cppNamespaceToJavaPackageMap = {
    'dt' : 'com.MyCompany.dt',
    'geom' : 'com.MyCompany.geom',
    'pizza' : 'com.MyCompany.pizza' 
}

# The set of C++ namespaces to be processed
cppNamespacesToProcess = [
    'dt',
    'geom',
    'pizza'
]

# The set of C++ projects to be registered
projectsToRegister = [
    'MyCompany/DateTime',
    'MyCompany/Shapes',
    'MyCompany/Pizza'
]

# The qualified name of the java class for which an Initialise() method is created to register 
# all the projects
javaQualifiedNameOfInitialiseClass = 'com.MyCompany.pizza.Pizza'

pyceda.generate_jni.generateCedaJniBindings(
    cppNamespaceToJavaPackageMap, 
    cppNamespacesToProcess, 
    javaFolderPath, 
    cppFolderPath, 
    projectIncludes, 
    projectsToRegister, 
    javaQualifiedNameOfInitialiseClass)
