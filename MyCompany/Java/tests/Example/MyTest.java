// Test.java

import com.cedanet.ceda.*;
import com.MyCompany.dt.*;
import com.MyCompany.pizza.*;

public class MyTest 
{  
    static 
    {
        System.loadLibrary("cxJava"); // Load native library cxJava.dll (Windows) or libcxJava.so (Unixes)
                                   //  at runtime
        System.out.println("Initialising ceda\n");
        CedaJni.InitialiseCeda();

        System.loadLibrary("pizza_jni");
        Pizza.Initialise();
    }

    static void CreatePersistStoreTest1()
    {
        System.out.println("CreatePersistStoreTest1()\n");

        String path = "java_test_ceda_database_1";
        PersistStore pstore = PersistStore.openPersistStore(path, EOpenMode.OM_CREATE_ALWAYS);

        PSpace pspace = pstore.openPSpace("MyPSpace");
        pspace.close();

        pstore.close();
    }

    static void CreatePersistStoreTest2()
    {
        String path = "java_test_ceda_database_2";
        try(PersistStore pstore = PersistStore.openPersistStore(path, EOpenMode.OM_CREATE_ALWAYS)) {
            try(PSpace pspace = pstore.openPSpace("MyPSpace")) {
                try(WorkingSet ws = pspace.openWorkingSet("MyWorkingSet", true)) {
                    TPizzaOrder pizzaOrder = new TPizzaOrder(ws.bootstrapRootObject("MyUtRoot", "pizza::TPizzaOrder"), false);
                    pizzaOrder.printInstance();
                }
            }
        }
    }

    static void OpenPersistStoreTest()
    {
        String path = "java_test_ceda_database_3";
        try(PersistStore pstore = PersistStore.openPersistStore(path, EOpenMode.OM_OPEN_EXISTING)) {
            try(PSpace pspace = pstore.openPSpace("MyPSpace")) {
                try(WorkingSet ws = pspace.openWorkingSet("MyWorkingSet", true)) {
                    TPizzaOrder pizzaOrder = new TPizzaOrder(ws.getRootObject("MyUtRoot" ), false);
                    pizzaOrder.printInstance();
                }
            }
        }
    }

    static void OpenPersistStoreTest2()
    {
        String path = "java_test_ceda_database_4";
        try(PersistStore pstore = PersistStore.openPersistStore(path, EOpenMode.OM_OPEN_EXISTING)) {
            try(PSpace pspace = pstore.openPSpace("MyPSpace")) {
                try(WorkingSet ws = pspace.openWorkingSet("MyWorkingSet", true)) {
                    try(com.cedanet.ceda.Server server = ws.openServer("MyProtocol", 1, 3000 )) {
                        try(com.cedanet.ceda.Client client = ws.openClient("MyProtocol", 1, "127.0.0.1", 3000 )) {
                            TPizzaOrder pizzaOrder = new TPizzaOrder(ws.bootstrapRootObject("MyUtRoot", "pizza::TPizzaOrder"), false);
                            pizzaOrder.printInstance();
                        }
                    }
                }
            }
        }
    }

    static TRectangle makeRectangle(float width, float height)
    {
        TRectangle rect = TRectangle.make();
        rect.Width.set(width);
        rect.Height.set(height);
        return rect;
    }

    static void showPizzaOrders(PSpace pspace, TPizzaDeliveryDatabase pdd)
    {
        try (Transaction txn = pspace.OpenTxn())
        {
            pdd.printInstance();

            int numOrders = pdd.sizeOfOrders();
            System.out.println("Num pizza orders = " + numOrders);
            for (int i = 0; i < numOrders; ++i)
            {
                TPizzaOrder order = pdd.elementInOrders(i);
                order.printInstance();
            }
        }
    }

    static void showPizzaToppings(PSpace pspace, TPizzaDeliveryDatabase pdd)
    {
        try (Transaction txn = pspace.OpenTxn())
        {
            for (TPizzaDeliveryDatabase.typeof_Toppings.Element1 i : pdd.Toppings)
            {
                System.out.println("Topping " + i.Description.get() + " has price " + i.Price.get());
            }
        }
    }

    /*
    static void showCar(PSpace pspace, TPizzaDeliveryDatabase pdd)
    {
        try (Transaction txn = pspace.OpenTxn())
        {
            TCar car = pdd.Car.get();
            if (car != null)
            {
                car.printInstance();
            }
        }
    }

    static void setTest(TCar.typeof_Test test)
    {
        test.b.set(true);
        test.i8.set((byte) -15);
        test.i16.set((short) -1234);
        test.i32.set(-123456);
        test.i64.set(-1234567890L);
        test.ui8.set((byte) 10);
        test.ui16.set((short) 1234);
        test.ui32.set(123456);
        test.ui64.set(1234567890L);
        test.f32.set(3.14f);
        test.f64.set(2.71828);
        test.L.at(0).set(100);
        test.L.at(3).set(200);
        test.s8.set("hi");

        for (int i=0 ; i < 4 ; ++i)
            for (int j = 0 ; j < 3 ; ++j)
                test.A.at(i).at(j).set(10*i+j);

        for (int i=0 ; i < 5 ; ++i)
        {
            test.S1.at(i).insert(0, "abc" + i);
            test.S2.at(i).set("x" + i);
        }
    }


    // Measured rate = 100k transactions per second on Huawei KII-L22 Octa-core 1.5GHz
    static void emptyTransactionPerformance(PSpace pspace)
    {
        long startTime = System.nanoTime();

        //System.out.println("Begin empty transaction performance test");
        for (int i=0 ; i < 100000 ; ++i) {
            try (Transaction txn = pspace.OpenTxn()) {
            }
        }
        //System.out.println("End empty transaction performance test");
        long endTime = System.nanoTime();

        double durationSecs = (endTime - startTime) / 1E9;
        System.out.println("Empty transaction performance test : " + durationSecs);
    }

    static void assignmentOnIntFieldOfPersistentObjectPerformance(PSpace pspace, TPizzaDeliveryDatabase pdd)
    {
        long startTime;

        startTime = System.nanoTime();
        try (Transaction txn = pspace.OpenTxn()) {
            for (int i=0 ; i < 100000 ; ++i) {
                // 100k per 5.7 sec
                pdd.Car.get().Test.i32.set(i);
            }
        }
        System.out.println("Assignment pdd.Car.get().Test.i32.set(i) : " + (System.nanoTime() - startTime) / 1E9);

        startTime = System.nanoTime();
        try (Transaction txn = pspace.OpenTxn()) {
            TCar car = pdd.Car.get();
            TCar.typeof_Test.typeof_i32 x = car.Test.i32;
            for (int i=0 ; i < 100000 ; ++i) {
                // 100k per 1.3 sec
                x.set(i);
            }
        }
        System.out.println("Assignment x.set(i) : " + (System.nanoTime() - startTime) / 1E9);

        startTime = System.nanoTime();
        try (Transaction txn = pspace.OpenTxn()) {
            TCar car = pdd.Car.get();
            for (int i=0 ; i < 100000 ; ++i) {
                // 100k per 1.3 sec
                car.Test.i32.set(i);
            }
        }
        System.out.println("Assignment car.Test.i32.set(i) : " + (System.nanoTime() - startTime) / 1E9);

        startTime = System.nanoTime();
        try (Transaction txn = pspace.OpenTxn()) {
            TCar car = pdd.Car.get();
            for (int i=0 ; i < 100000 ; ++i) {
                // 100k per 2.8 sec
                car.M3.at(i).set(i);
            }
        }
        System.out.println("Assignment car.M3.at(i).set(i) : " + (System.nanoTime() - startTime) / 1E9);

        startTime = System.nanoTime();
        long sum = 0;
        try (Transaction txn = pspace.OpenTxn()) {
            TCar car = pdd.Car.get();
            for (int i=0 ; i < 100000 ; ++i) {
                // 100k per 1 sec
                sum += car.M3.at(i).get();
            }
        }
        System.out.println("sum += car.M3.at(i).get() = " + sum + "  time : " + (System.nanoTime() - startTime) / 1E9);

        startTime = System.nanoTime();
        sum = 0;
        try (Transaction txn = pspace.OpenTxn()) {
            TCar car = pdd.Car.get();
            for (TCar.typeof_M3.Element1 e : car.M3) {
                // 100k per 1.3 sec
                sum += e.get();
            }
        }
        System.out.println("sum += e.get() = " + sum + "  time : " + (System.nanoTime() - startTime) / 1E9);
    }
    */

    static void transactionOnPizzaDatabase(PSpace pspace, TPizzaDeliveryDatabase pdd)
    {
        try (Transaction txn = pspace.OpenTxn()) {
            /*
            TCar car = pdd.Car.get();
            if (car == null)
            {
                System.out.println("No car defined");
                car = TCar.create();
                setTest(car.Test);
                car.Test.f32.set(10.0f);
                car.M1.at("x").insert(0,"world");
                car.M1.at("x").insert(0,"hello ");
                car.M1.at("x").erase(0,1);
                car.M1.at("y").insert(0,"hi");

                car.M2.at(0).at("1").set("one");
                car.M2.at(1).at("2").set("two");
                car.M2.at(1).at("3").set("three");
                pdd.Car.set(car);
            }
            */

            //System.out.println("pdd.Car.M2.at(1).get() = " + pdd.Car.get().M2.at("1").get());

            // TPizzaDeliveryDatabase.typeof_Test test = pdd.Test;


            //pdd.setbOfTest(true);
            //pdd.seti8OfTest((byte) -10);
            //pdd.seti16OfTest((short) -1234);
            //pdd.seti32OfTest(-123456);
            //pdd.seti64OfTest(-1234567890L);
            //pdd.setui8OfTest((byte) 10);
            //pdd.setui16OfTest((short) 1234);
            //pdd.setui32OfTest(123456);
            //pdd.setui64OfTest(1234567890L);
            //pdd.setf32OfTest(3.14f);
            //pdd.setf64OfTest(2.71828);
            //pdd.setElementInLOfTest(0, 100);
            //pdd.setElementInLOfTest(3, 200);
            //pdd.sets8OfTest("hi");

            /*
            TTest t = TTest.make();
            t.setb(true);
            t.setElementInL(0, 25);
            t.setf64(1.2);
            t.sets8("hello world");
            //pdd.setTest2(t);
            pdd.Test2.set(t);
            */

            // Add a vehicle type
            int vehicleTypeId1 = 10;
            //pdd.insertInDescriptionOfElementInVehicleTypes(vehicleTypeId1, 0, "Toyota Camry");
            pdd.VehicleTypes.at(vehicleTypeId1).Description.insert(0, "Toyota Camry");

            // Add a vehicle type
            int vehicleTypeId2 = 11;
            pdd.VehicleTypes.at(vehicleTypeId2).Description.insert(0, "Holden Gemini");
            //pdd.insertInDescriptionOfElementInVehicleTypes(vehicleTypeId2, 0, "Holden Gemini");

            // Define toppings
            int hamToppingId = 1;
            pdd.Toppings.at(hamToppingId).Price.set(50.0);
            //pdd.setPriceOfElementInToppings(hamToppingId, 50.0);
            //pdd.insertInDescriptionOfElementInToppings(hamToppingId, 0, "Ham");
            pdd.Toppings.at(hamToppingId).Description.insert(0, "Ham");

            int pineappleToppingId = 2;
            pdd.setPriceOfElementInToppings(pineappleToppingId, 25.0);
            pdd.insertInDescriptionOfElementInToppings(pineappleToppingId, 0, "Pineapple");

            // Add a customer
            long customerId = 100;
            pdd.insertInFirstNameOfElementInCustomers(customerId, 0, "Peter");
            pdd.insertInLastNameOfElementInCustomers(customerId, 0, "Rabbit");
            pdd.setNumberOfAddressOfElementInCustomers(customerId, 2);
            pdd.insertInStreetOfAddressOfElementInCustomers(customerId, 0, "Oxyzford St");
            pdd.eraseRangeInStreetOfAddressOfElementInCustomers(customerId, 2, 4);

            // Add a pizza order
            TPizzaOrder pizzaOrder = TPizzaOrder.create();
            long orderId = 10 + pdd.sizeOfOrders();
            pizzaOrder.setId(orderId);
            pdd.insertInOrders(0, pizzaOrder);
            pizzaOrder.setCustomerId(customerId);
            pizzaOrder.setDayOfDateOfDateTimeOrderDelivered(24);
            pizzaOrder.setMonthOfDateOfDateTimeOrderDelivered(TMonth.Aug);
            pizzaOrder.setYearOfDateOfDateTimeOrderDelivered(2018);
            pizzaOrder.setHourOfTimeOfDateTimeOrderDelivered(10);
            pizzaOrder.setMinuteOfTimeOfDateTimeOrderDelivered(30);
            pizzaOrder.setDeliveryStatus(EDeliveryStatus.Delivering);

            TShape shape = TShape.make();
            shape.setAsRectangle(makeRectangle(120.0f, 180.0f).self());
            TOrderedPizza pizza = TOrderedPizza.make();
            pizza.setShape(shape);
            pizza.setBaseType(EBaseType.DeepPan);
            pizza.insertInToppingIds(0, hamToppingId);
            pizza.insertInToppingIds(1, pineappleToppingId);

            pizzaOrder.insertInPizzas(0, pizza);

            pdd.printInstance();
            System.out.println("Number of customers = " + pdd.sizeOfCustomers());
            System.out.println("Customer 100 exists = " + pdd.hasElementInCustomers(customerId));
            System.out.println("Customer 101 exists = " + pdd.hasElementInCustomers(101));
            System.out.println("Customer 100's first name = " + pdd.getFirstNameOfElementInCustomers(customerId));

            for (TPizzaDeliveryDatabase.typeof_Toppings.Element1 topping : pdd.Toppings)
            {
                topping.Price.set(topping.Price.get() + 1.0);
            }





            // Set an image with 10000000 bytes
            int size = 10000000;
            byte[] b = new byte[size];
            for (int i=0 ; i < size ; ++i) {
                b[i] = (byte) 100;
            }

            /*
            MImage image = MImage.make();
            image.Format.set(EImageFormat.Jpeg);
            image.Height.set(100);
            image.Width.set(100);
            */
            /*
            for (int i=0 ; i < size ; ++i) {
                image.Bytes.insert(i,(byte)100);
            }
            */

            /*
            image.Bytes.insert(0,b);

            pdd.Voter.FingerPrints.at(0).set(image);
            */








        }
    }

    static void PizzaTest()
    {
        PizzaServer server = new PizzaServer();
        transactionOnPizzaDatabase(server.getPSpace(), server.getPizzaDeliveryDatabase());

        //emptyTransactionPerformance(server.getPSpace());
        //assignmentOnIntFieldOfPersistentObjectPerformance(server.getPSpace(), server.getPizzaDeliveryDatabase());

        PizzaClient client = new PizzaClient();
        showPizzaOrders(client.getPSpace(), client.getPizzaDeliveryDatabase());
        showPizzaToppings(client.getPSpace(), client.getPizzaDeliveryDatabase());
        //showCar(client.getPSpace(), client.getPizzaDeliveryDatabase());

        client.close();
        server.close();
    }

    static void PizzaTest2()
    {
        for (int i=0 ; i < 2 ; ++i) {
            PizzaServer server = new PizzaServer();
            PizzaClient client = new PizzaClient();
            client.close();
            server.close();
        }
    }

    static void AddressTest()
    {
        TAddress address = TAddress.make();
        address.setNumber(10);
        address.setZipPostCode(6000);
        address.insertInStreet(0, "Old Kent Road");
        System.out.println("Street = " + address.getStreet());
        address.clearStreet();
        System.out.println("Street = " + address.getStreet());

        System.out.println("Number = " + address.getNumber());
        System.out.println("ZipPostCode = " + address.getZipPostCode());
        address.printInstance();
    }

    static void AddressTest2()
    {
        TAddress address = TAddress.make();
        address.Number.set(10);
        address.ZipPostCode.set(6000);
        address.Street.insert(0, "Old Kent Road");
        System.out.println("Street = " + address.Street.get());
        address.Street.clear();
        System.out.println("Street = " + address.Street.get());

        System.out.println("Number = " + address.Number.get());
        System.out.println("ZipPostCode = " + address.ZipPostCode.get());
        address.printInstance();
    }


    static void ImageTest()
    {
        /*
        System.out.println("Create array");
        int n = 100000;
        byte[] b = new byte[n];
        for (int i=0 ; i < n ; ++i) {
            b[i] = (byte) i;
        }

        System.out.println("Begin");
        MImage image = MImage.make();
        image.Format.set(EImageFormat.Jpeg);
        image.Height.set(100);
        image.Width.set(100);
        */

        /*
        for (int i=0 ; i < n ; ++i) {
            image.Bytes.insert(i,(byte)100);
        }
        */

        /*
        image.Bytes.insert(0,b);

        System.out.println("Size = " + image.Bytes.size());

        //byte[] r1 = image.Bytes.get();
        byte[] r1 = image.Bytes.get(10,16);

        for (int i=0 ; i < 5 ; ++i) {
            System.out.println("image[" + i + "] = " + image.Bytes.at(i));
        }

        for (int i=0 ; i < 5 ; ++i) {
            System.out.println("image[" + i + "] = " + r1[i]);
        }

        byte[] r2 = new byte[n];
        for (int i=0 ; i < n ; ++i) {
            r2[i] = image.Bytes.at(i);
        }

        System.out.println("End");

        byte[] bytes1 = image.Bytes.get();

        int i1 = 0;
        int i2 = 100;
        byte[] bytes2 = image.Bytes.get(i1, i2);  // get bytes in range [i1,i2)
        */
    }

 
    // Test Driver
    public static void main(String[] args) 
    {
        //CreatePersistStoreTest1();
        //CreatePersistStoreTest2();
      
        //AddressTest();
        //AddressTest2();

        //CreatePersistStoreTest();
        //OpenPersistStoreTest();
        //PizzaTest2();
        //ImageTest();
        PizzaTest();

        //new Test().sayHello();  // Create an instance and invoke the native method
    }
}