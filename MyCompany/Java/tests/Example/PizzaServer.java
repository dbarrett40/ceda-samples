import com.MyCompany.pizza.*;
import com.cedanet.ceda.*;

public class PizzaServer {

    public PizzaServer()
    {
        // Path to the local ceda database used by this server
        String path = "pizzaserver";

        // Open the local ceda database (a PersistStore) used by the server.
        // OM_OPEN_ALWAYS means if it doesn't already exist it is created.
        _pstore = PersistStore.openPersistStore(path, EOpenMode.OM_CREATE_ALWAYS);
        //_pstore = PersistStore.openPersistStore(path, EOpenMode.OM_OPEN_ALWAYS);

        // Open a single PSpace in the PersistStore with the given name
        // If it doesn't already exist it is created
        _pspace = _pstore.openPSpace("PizzaPSpace");

        // Open a single WorkingSet in the PSpace with the given name
        // If it doesn't already exist it is created
        // dataSetMaster is TRUE meaning that the server takes on responsibility for allocating a
        // data set identifier when the working set is created for the first time.
        _workingSet = _pspace.openWorkingSet("PizzaWorkingSet", true);

        // Associate a socket end point with the working set which listens on port 3000 for
        // connections from clients.
        // The protocol name and version must be the same as specified on the client.
        _server = _workingSet.openServer("PizzaProtocol", 1, 3000 );

        // Bootstrap a root object identified by the name "PizzaDeliveryDatabase" of type pizza::TPizzaDeliveryDatabase
        _pizzaDeliveryDatabase = new TPizzaDeliveryDatabase(_workingSet.bootstrapRootObject("PizzaDeliveryDatabase", "pizza::TPizzaDeliveryDatabase"), false);
    }

    public void close() {
        _server.close();
        _workingSet.close();
        _pspace.close();
        _pstore.close();
    }

    public PSpace getPSpace() { return _pspace; }
    public TPizzaDeliveryDatabase getPizzaDeliveryDatabase() { return _pizzaDeliveryDatabase; }

    private PersistStore _pstore;
    private PSpace _pspace;
    private WorkingSet _workingSet;
    private Server _server;
    private TPizzaDeliveryDatabase _pizzaDeliveryDatabase;
}

/*
namespace pizza
{
    $typedef+ int32 TToppingId;
    $typedef+ int32 TEmployeeId;
    $typedef+ int32 TVehicleTypeId;
    $typedef+ int32 TVehicleId;
    $typedef+ int64 TCustomerId;
    $typedef+ int64 TOrderId;
    $typedef+ float64 TCurrency;
    $typedef+ string8 TEmail;
    $typedef+ string8 TPhoneNumber;

    $model+ TAddress <<multiline>>
    {
        int32 Number;
        string8 Street;
        string8 City;
        int32 ZipPostCode;
        string8 StateProvinceCounty;
        string8 Country;
    };

    $model+ TEmployee <<multiline>>
    {
        TAddress Address;
        string8 FirstName;
        string8 LastName;
        TPhoneNumber PhoneNumber;
    };

    $model+ TVehicleType
    {
        string8 Description;
    };

    $model+ TVehicle
    {
        TVehicleTypeId VehicleTypeId;
        string8 LicensePlateNumber;
    };

    $enum+ class EPaymentMethod
    {
        Cash,
        EftPos,
        CreditCard
    };

    $model+ TCustomer <<multiline>>
    {
        TAddress Address;
        string8 FirstName;
        string8 LastName;
        TPhoneNumber PhoneNumber;
        TEmail Email;
        dt::TDateTime DateOfFirstOrder;
        EPaymentMethod PaymentMethod;
    };

    $enum+ class EDeliveryStatus
    {
        Cooking,
        Delivering,
        Completed,
        Returned
    };

    $enum+ class EBaseType
    {
        Thin,
        DeepPan
    };
    $model+ TCircle
    {
        float32 Radius;
    };

    $model+ TRectangle
    {
        float32 Width;
        float32 Height;
    };

    $variant+ TShape
    {
        default TCircle(200);
        TCircle Circle;
        TRectangle Rectangle;
    };

    $model+ TTopping <<multiline>>
    {
        TCurrency Price;
        string8 Description;
    };

    $model+ TOrderedPizza <<multiline>>
    {
        TShape Shape;
        EBaseType BaseType;
        xvector<TToppingId> ToppingIds;
    };

    $struct+ TPizzaOrder <<multiline>> isa ceda::IPersistable :
        model
        {
            TOrderId Id;
            TCustomerId CustomerId;
            TEmployeeId TakenByEmployeeId;
            TEmployeeId DeliveredByEmployeeId;
            EDeliveryStatus DeliveryStatus;
            TVehicleId VehicleId;
            dt::TDateTime DateTimeOrderTaken;
            dt::TDateTime DateTimeOrderDelivered;
            TCurrency TotalOrderPrice;
            xvector<TOrderedPizza> Pizzas;
        }
    {
    };

    $struct+ TPizzaDeliveryDatabase <<multiline>> isa ceda::IPersistable :
        model
        {
            xmap<TVehicleId, TVehicle> Vehicles;
            xmap<TVehicleTypeId, TVehicleType> VehicleTypes;
            xmap<TEmployeeId, TEmployee> Employees;
            xmap<TToppingId, TTopping> Toppings;
            xmap<TCustomerId, TCustomer> Customers;
            xvector< movable< cref<TPizzaOrder> > > Orders;
        }
    {
    };
} // namespace pizza
*/
