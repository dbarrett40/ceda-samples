cmake_minimum_required (VERSION 3.5)

find_package(Java REQUIRED)
include(UseJava)

set(CMAKE_JAVA_COMPILE_FLAGS "-source" "1.8" "-target" "1.8")

if (TARGET PizzaJni)
    set(cedaJniJarFile "${Ceda_BIN}/CedaJni.jar")
    message("cedaJniJarFile = ${cedaJniJarFile}")

    add_jar(MyTestJar SOURCES MyTest.java PizzaClient.java PizzaServer.java INCLUDE_JARS ${cedaJniJarFile} PizzaJni)

    get_target_property(MyTestJarFile MyTestJar JAR_FILE)
    get_target_property(PizzaJniJarFile PizzaJni JAR_FILE)

    if(CMAKE_HOST_WIN32)
        message("Adding test: ${Java_JAVA_EXECUTABLE} -cp ${MyTestJarFile};${PizzaJniJarFile};${cedaJniJarFile} MyTest")
        message("with path = ${Ceda_BIN}")
        
        add_test(
            NAME Test_MyTest 
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin
            COMMAND ${Java_JAVA_EXECUTABLE} -cp "${MyTestJarFile};${PizzaJniJarFile};${cedaJniJarFile}" MyTest)

        SET_TESTS_PROPERTIES(Test_MyTest PROPERTIES ENVIRONMENT "PATH=${Ceda_BIN}")
    else()
        message("Adding test: ${Java_JAVA_EXECUTABLE} -Djava.library.path=${Ceda_LIB}:${CMAKE_BINARY_DIR}/lib -cp ${MyTestJarFile}:${PizzaJniJarFile}:${cedaJniJarFile} MyTest")

        add_test(
            NAME Test_MyTest 
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin
            COMMAND ${Java_JAVA_EXECUTABLE} -Djava.library.path=${Ceda_LIB}:${CMAKE_BINARY_DIR}/lib -cp "${MyTestJarFile}:${PizzaJniJarFile}:${cedaJniJarFile}" MyTest)
    endif()
endif()
