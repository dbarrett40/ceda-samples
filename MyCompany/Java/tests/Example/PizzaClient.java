import com.MyCompany.pizza.*;
import com.cedanet.ceda.*;

public class PizzaClient
{
    public PizzaClient()
    {
        // Path to the local ceda database used by this client
        String path = "pizzaclient";

        // Open the local ceda database (a PersistStore) used by the client.
        // OM_OPEN_ALWAYS means if it doesn't already exist it is created.
        _pstore = PersistStore.openPersistStore(path, EOpenMode.OM_CREATE_ALWAYS);
        //_pstore = PersistStore.openPersistStore(path, EOpenMode.OM_OPEN_ALWAYS);

        // Open a single PSpace in the PersistStore with the given name
        // If it doesn't already exist it is created
        _pspace = _pstore.openPSpace("PizzaPSpace");

        // Open a single WorkingSet in the PSpace with the given name
        // If it doesn't already exist it is created
        // dataSetMaster is FALSE meaning that the client receives the data set identifier from the
        // server the first time it connects to the server.
        _workingSet = _pspace.openWorkingSet("PizzaWorkingSet", false);

        _client = _workingSet.openClient("PizzaProtocol", 1, "127.0.0.1",3000 );

        // Wait for at least one operation to be received from the server.
        // Since the first operation generated on the server was to create the PizzaDeliveryDatabase object
        // named "PizzaDeliveryDatabase", it will then be possible for this client to access the
        // PizzaDeliveryDatabase object
        while(true)
        {
            try
            {
                Thread.sleep(100);
            }
            catch(Exception e)
            {
            }

            int size = _workingSet.historyBufferSize();
            System.out.println("History buffer size = " + size);
            if (size > 0) break;
        }

        _pizzaDeliveryDatabase = new TPizzaDeliveryDatabase(_workingSet.getRootObject("PizzaDeliveryDatabase"), false);
    }

    public void close()
    {
        _client.close();
        _workingSet.close();
        _pspace.close();
        _pstore.close();
    }

    public PSpace getPSpace() { return _pspace; }
    public TPizzaDeliveryDatabase getPizzaDeliveryDatabase() { return _pizzaDeliveryDatabase; }

    private PersistStore _pstore;
    private PSpace _pspace;
    private WorkingSet _workingSet;
    private Client _client;
    private TPizzaDeliveryDatabase _pizzaDeliveryDatabase;
}
