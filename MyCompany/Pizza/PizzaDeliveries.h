// PizzaDeliveries.h

@import "Pizza.h"
@import "MyCompany/DateTime/DateTime.h"
@import "MyCompany/Shapes/Shapes.h"

namespace pizza
{
    $typedef+ int32 TToppingId;
    $typedef+ int32 TEmployeeId;
    $typedef+ int32 TVehicleTypeId;
    $typedef+ int32 TVehicleId;
    $typedef+ int64 TCustomerId;
    $typedef+ int64 TOrderId;
    $typedef+ float64 TCurrency;
    $typedef+ string8 TEmail;
    $typedef+ string8 TPhoneNumber;
    
    $model+ TAddress <<multiline>>
    {
        int32 Number;
        string8 Street;
        string8 City;
        int32 ZipPostCode;
        string8 StateProvinceCounty;
        string8 Country;
    };
    
    $model+ TEmployee <<multiline>>
    {
        TAddress Address;
        string8 FirstName;
        string8 LastName;
        TPhoneNumber PhoneNumber;
    };
    
    $model+ TVehicleType
    {
        string8 Description;
    };
    
    $model+ TVehicle
    {
        TVehicleTypeId VehicleTypeId;
        string8 LicensePlateNumber;
    };
    
    $enum+ class EPaymentMethod
    {
        Cash,
        EftPos,
        CreditCard
    };
    
    $model+ TCustomer <<multiline>>
    {
        TAddress Address;
        string8 FirstName;
        string8 LastName;
        TPhoneNumber PhoneNumber;
        TEmail Email;
        dt::TDateTime DateOfFirstOrder;
        EPaymentMethod PaymentMethod;
    };
    
    $enum+ class EDeliveryStatus
    {
        Cooking,
        Delivering,
        Completed,
        Returned
    };
    
    $enum+ class EBaseType
    {
        Thin,
        DeepPan
    };
    $model+ TCircle
    {
        float32 Radius;    
    };

    $model+ TRectangle
    {
        float32 Width;
        float32 Height;
    };

    $variant+ TShape
    {
        default TCircle(200);
        TCircle Circle;
        TRectangle Rectangle;
    };
    
    $model+ TTopping <<multiline>>
    {
        TCurrency Price;
        string8 Description;
    };
    
    $model+ TOrderedPizza <<multiline>>
    {
        TShape Shape;
        EBaseType BaseType;
        xvector<TToppingId> ToppingIds;
    };
    
    $struct+ TPizzaOrder <<multiline>> isa ceda::IPersistable :
        model
        {
            TOrderId Id;
            TCustomerId CustomerId;
            TEmployeeId TakenByEmployeeId;
            TEmployeeId DeliveredByEmployeeId;
            EDeliveryStatus DeliveryStatus;
            TVehicleId VehicleId;
            dt::TDateTime DateTimeOrderTaken;
            dt::TDateTime DateTimeOrderDelivered;
            TCurrency TotalOrderPrice;
            xvector<TOrderedPizza> Pizzas;
        }
    {
    };
    
    $struct+ TPizzaDeliveryDatabase <<multiline>> isa ceda::IPersistable :
        model
        {
            xmap<TVehicleId, TVehicle> Vehicles;
            xmap<TVehicleTypeId, TVehicleType> VehicleTypes;
            xmap<TEmployeeId, TEmployee> Employees;
            xmap<TToppingId, TTopping> Toppings;
            xmap<TCustomerId, TCustomer> Customers;
            xvector< movable< cref<TPizzaOrder> > > Orders;
        }
    {
    };
} // namespace pizza
