// Pizza.cpp

@import "Pizza.h"
@import "PizzaDeliveries.h"
@import "Ceda/cxObject/TargetRegistry.h"

mRegisterLeafTarget(
    // Releases
    {
        // version         year mon day
        { "Release 0.1",   {2017,6,20}  },      // Release #0
    })
