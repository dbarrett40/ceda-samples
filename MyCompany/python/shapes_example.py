# shapes_example.py

import pyceda
import pypizza

geom = pyceda.rootnamespace.geom

p = geom.TPolygon( [ geom.TPoint(X=1,Y=2), geom.TPoint(X=7,Y=-2) ] )
p.V.append( geom.TPoint(X=5,Y=1) )
p.V[0].X = 1000
p.V.append( p.V[1] )
print 'p = ' + `p`

c = geom.TCircle( Radius = 100 )
print 'c = ' + `c`

r = geom.TRectangle( Width=200, Height=300 )
print 'r = ' + `r`

s = geom.TShape()
print 's = ' + `s`

s.Polygon = p
print 's = ' + `s`