import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

# Tell setuptools this isn't a pure distribution
try:
    from wheel.bdist_wheel import bdist_wheel as _bdist_wheel
    class bdist_wheel(_bdist_wheel):
        def finalize_options(self):
            _bdist_wheel.finalize_options(self)
            self.root_is_pure = False
except ImportError:
    bdist_wheel = None

setuptools.setup(
    cmdclass={'bdist_wheel': bdist_wheel},
    name='pypizza',
    version='${PROJECT_VERSION}',
    author='My name',
    author_email='my.name@mycompany.com',
    description='Python extension module for the Pizza Delivery System',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='http://www.mycompany.com',
    platforms=['windows'],
    packages=setuptools.find_packages(),
    package_data = {
        'pypizza' : ['lib/*']
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: Free for non-commercial use',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Operating System :: Microsoft :: Windows',
        "Programming Language :: C++",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
)

