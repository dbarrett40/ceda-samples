import os
import pyceda
import pypizza
from pyceda.generate_jni import *

prefix = '../CedaAndroidExample/app/src/main'

# Location for where to write the java files
javaFolderPath = prefix + '/java'

# Location for where to write the cpp files
cppFolderPath = prefix + '/cpp'

# includes paths which are inserted into every cpp file
projectIncludes = [
    '../Utils.h',
    'MyCompany/Pizza/PizzaDeliveries.h'
]

# Mapping from C++ namespace to Java package name
cppNamespaceToJavaPackageMap = {
    'dt' : 'com.MyCompany.dt',
    'geom' : 'com.MyCompany.geom',
    'pizza' : 'com.MyCompany.pizza' 
}

# The set of C++ namespaces to be processed
cppNamespacesToProcess = [
    'dt',
    'geom',
    'pizza'
]

def makeDir(dirPath):
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)

makeDir(javaFolderPath + '/com/MyCompany/dt')
makeDir(javaFolderPath + '/com/MyCompany/shapes')
makeDir(javaFolderPath + '/com/MyCompany/pizza')

makeDir(cppFolderPath + '/dt')
makeDir(cppFolderPath + '/shapes')
makeDir(cppFolderPath + '/pizza')

for cppNamespace in cppNamespacesToProcess:
    processNamespace(cppNamespaceToJavaPackageMap, cppNamespace, javaFolderPath, cppFolderPath, projectIncludes)




