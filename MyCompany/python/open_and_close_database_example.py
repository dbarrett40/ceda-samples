# open_and_close_database_example.py

import pyceda

ceda = pyceda.ceda

# Create a database named 'mydatabase'
pstore = ceda.OpenPersistStore(
    'mydatabase',
    ceda.EOpenMode.OM_CREATE_ALWAYS,
    ceda.PersistStoreSettings())

# Close the database
pstore.Close()