# datetime_example.py

import pyceda
import pypizza

dt = pyceda.rootnamespace.dt

date = dt.TDate(2017,dt.TMonth.Aug,24)
print('date = ' + `date`)
print('date.Year = ' + `date.Year`)
print('date.Month = ' + `date.Month`)
print('date.Day = ' + `date.Day`)

time = dt.TTime(20,30,00)
print('time = ' + `time`)

datetime = dt.TDateTime(date,time)
print('datetime = ' + `datetime`)

print('datetime.Date = ' + `datetime.Date`)
print('datetime.Time = ' + `datetime.Time`)